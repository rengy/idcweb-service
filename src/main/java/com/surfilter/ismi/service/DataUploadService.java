package com.surfilter.ismi.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.surfilter.ismi.util.ExceptionPrintUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DataUploadService {

//    @Value("${sftp.host}")
//    private String sftpHost;
//
//    @Value("${sftp.port}")
//    private String sftpPort;
//
//    @Value("${sftp.username}")
//    private String sftpUsername;
//
//    @Value("${sftp.password}")
//    private String sftpPassword;
//
//    @Value("${sftp.root}")
//    private String sftpRoot = "/home/lg/isms/";
//
//    @Value("${sftp.rltdir}")
//    private String sftpRltDir = "999";

    @Value("${ftp.host}")
    private String ftpHost;

    @Value("${ftp.port}")
    private String ftpPort;

    @Value("${ftp.username}")
    private String ftpUsername;

    @Value("${ftp.password}")
    private String ftpPassword;

    @Value("${ftp.root}")
    private String ftpRoot;

    @Value("${ftp.rltdir}")
    private String ftpRltDir = "999";

    private XmlMapper xmlMapper=new XmlMapper();

    


    /**
     * 数据上报
     * 上报数据文件的存放路径规则为：/idc_home/上报数据类型代码/上报日期/
     * 文件以生成时间命名且应带有.xml后缀名，生成时间用1970年1月1日到文件生成时的秒数表示（如，“生成时间.xml”形式）
     *
     * @param bizXmlStr
     * @param dataTypeCode  1	基础数据记录
     *                      2	基础数据监测异常记录
     *                      3	访问日志查询记录
     *                      4	违法信息监测记录
     *                      5	违法信息过滤记录
     *                      6	（保留）
     *                      7	ISMS活动状态
     *                      8	活跃资源监测记录
     *                      9	违法违规网站监测记录
     * @param uploadDateStr yyyy-MM-dd
     */
//    public void doUpload(String uploadXml, String dataTypeCode, String uploadDateStr) {
//        SftpChannel sftpChannel = null;
//        OutputStream os = null;
//        try {
//            sftpChannel = new SftpChannel();
//            ChannelSftp sftp = sftpChannel.getChannel(genConnMap(), 3000);
//            log.info("判断目标文件夹是否存在:{}", sftpRoot);
//            try {
//                sftp.stat(sftpRoot);
//            } catch (Exception e) {
//                log.error("sftp根目录不存在，无法继续！");
//                return;
//            }
//
//            sftp.cd(sftpRoot);
//            log.info("当前文件夹:{}", sftp.pwd());
//
//            log.info("判断目标文件夹是否存在:{}", dataTypeCode);
//            try {
//                sftp.stat(dataTypeCode);
//            } catch (Exception e) {
//                sftp.mkdir(dataTypeCode);
//            }
//            sftp.cd(dataTypeCode);
//            log.info("当前文件夹:{}", sftp.pwd());
//
//            log.info("判断目标文件夹是否存在:{}", uploadDateStr);
//            try {
//                sftp.stat(uploadDateStr);
//            } catch (Exception e) {
//                sftp.mkdir(uploadDateStr);
//            }
//            sftp.cd(uploadDateStr);
//            log.info("当前文件夹:{}", sftp.pwd());
//
//            String fileName = (System.currentTimeMillis() / 1000) + ".xml";
//
//            os = sftp.put(fileName);
//            //String uploadXml = genFileUploadXml(bizXmlStr);
//            if (StringUtils.isNotBlank(uploadXml)) {
//                os.write(uploadXml.getBytes(Charset.forName("UTF-8")));
//                os.flush();
//            } else {
//                log.error("上传数据异常");
//            }
//        } catch (Exception e) {
//            log.error("上传异常：" + e.getMessage(), e);
//        } finally {
//            if (os != null) {
//                try {
//                    os.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            if (sftpChannel != null) {
//                try {
//                    sftpChannel.closeChannel();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }

    /**
     * 轮训检查文件处理结果
     *
     * @return List<FileUploadResult>
     * <p>
     * 0    处理完成
     * 1	文件解密失败
     * 2	文件校验失败
     * 3	文件解压缩失败
     * 4	文件格式异常
     * 5	文件内容异常（版本错误）
     * 51	文件内容异常——上报类型错误
     * 52	文件内容异常——节点/子节点长度错误
     * 53	文件内容异常——节点/子节点类型错误
     * 54	文件内容异常——节点/子节点内容错误
     * 55	文件内容异常——节点/子节点缺漏
     * 900	其他异常（存在其他错误，需重新上报）
     * 999	其他异常（处理中）
     */
//    public List<FileUploadResult> getFileUploadRlt() {
//        SftpChannel sftpChannel = null;
//        try {
//            sftpChannel = new SftpChannel();
//            ChannelSftp sftp = sftpChannel.getChannel(genConnMap(), 3000);
//
//            log.info("判断目标文件夹是否存在:{}", sftpRoot);
//            try {
//                sftp.stat(sftpRoot);
//            } catch (Exception e) {
//                log.error("sftp根目录不存在，无法继续！");
//                return null;
//            }
//            sftp.cd(sftpRoot);
//            log.info("当前文件夹:{}", sftp.pwd());
//
//            log.info("判断目标文件夹是否存在:{}", sftpRltDir);
//            try {
//                sftp.stat(sftpRltDir);
//            } catch (Exception e) {
//                log.error("sftp结果目录不存在，无法继续！");
//                return null;
//            }
//
//            sftp.cd(sftpRltDir);
//            log.info("当前文件夹:{}", sftp.pwd());
//
//            // TODO  对应处理代码 为 0（处理完成的） 直接删除，对于其他看情况后期如何处理
//            Vector fileList = sftp.ls(".");
//            fileList.forEach(
//                    it -> {
//                        log.info("文件处理结果中的文件：{}", it);
//                    }
//            );
//
//            return null;
//        } catch (Exception e) {
//            log.error("上传异常：" + e.getMessage(), e);
//            return null;
//        } finally {
//            if (sftpChannel != null) {
//                try {
//                    sftpChannel.closeChannel();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }



    public ExecutorService executorService=Executors.newSingleThreadExecutor();

    public void doUploadFtp(String uploadXml, String dataTypeCode, String uploadDateStr) {
        FTPClient ftpClient = new FTPClient();
        OutputStream os = null;
        try {
            //ftp初始化的一些参数
            ftpClient.connect(ftpHost, Integer.parseInt(ftpPort));
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            ftpClient.setControlEncoding("UTF-8");
            if (ftpClient.login(ftpUsername, ftpPassword)) {
                log.info("连接ftp成功");
            } else {
                log.error("连接ftp失败，可能用户名或密码错误");
                try {
                    if (ftpClient.isConnected()) {
                        ftpClient.disconnect();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return;
            }
            String pwd = ftpClient.printWorkingDirectory();
            log.info("WorkingDirectory:{}", pwd);
            //ftpClient.changeWorkingDirectory(ftpRoot);
            //if (ftpClient.getReplyCode() != 250) {
            ////    log.info("进入根目标文件夹失败:{}", ftpRoot);
            //    return;
            //}


            ftpClient.changeWorkingDirectory(dataTypeCode);
            if (ftpClient.getReplyCode() != 250) {
                ftpClient.makeDirectory(dataTypeCode);
                ftpClient.changeWorkingDirectory(dataTypeCode);
            }

            ftpClient.changeWorkingDirectory(uploadDateStr);
            if (ftpClient.getReplyCode() != 250) {
                ftpClient.makeDirectory(uploadDateStr);
                log.info("创建目标文件夹:{}", ftpClient.getReplyString());
                ftpClient.changeWorkingDirectory(uploadDateStr);
            }
            log.info("当前目录:{}", ftpClient.printWorkingDirectory());
            String filename0=""+(System.currentTimeMillis() / 1000);
            String fileName = filename0 + ".xml";

            os = ftpClient.storeFileStream(fileName);
            //String uploadXml = genFileUploadXml(bizXmlStr);
            //log.info("上传数据：{}",uploadXml);
                os.write(uploadXml.getBytes(Charset.forName("UTF-8")));
                os.flush();
                log.info("{}上传完成",fileName);
            //定时查询上传结果文件    
            timer1(dataTypeCode,filename0);
        } catch (Exception e) {
            log.error("上传异常：" + e.getMessage(), e);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (ftpClient != null && ftpClient.isConnected()) {
                try {
                    ftpClient.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    /**
     * 从ftp服务器下载文件
     * @param type
     * @param filename
     */
    private void getUploadResult(String type,String filename) {
        FTPClient ftpClient = new FTPClient();
        try {
            //ftp初始化的一些参数
            ftpClient.connect(ftpHost, Integer.parseInt(ftpPort));
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            ftpClient.setControlEncoding("UTF-8");
            if (ftpClient.login(ftpUsername, ftpPassword)) {
                log.info("连接ftp成功");
            } else {
                log.error("连接ftp失败，可能用户名或密码错误");
                try {
                    if (ftpClient.isConnected()) {
                        ftpClient.disconnect();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return;
            }
            log.info("进入目标文件夹:{}", ftpRoot);
            //ftpClient.changeWorkingDirectory(ftpRoot);
            //if (ftpClient.getReplyCode() != 250) {
            ////    log.info("进入根目标文件夹失败:{}", ftpRoot);
            //    return;
            //}

            String dataTypeCode="999";
            ftpClient.changeWorkingDirectory(dataTypeCode);
            if (ftpClient.getReplyCode() != 250) {
                ftpClient.makeDirectory(dataTypeCode);
                ftpClient.changeWorkingDirectory(dataTypeCode);
            }

            FTPFile[] list=ftpClient.listFiles();
            String s=type+"-"+filename;
            boolean isFinsh=false;
            for (int i = 0; i < list.length; i++) {
            	if (list[i].isFile()) {
            		String name=list[i].getName();
            		if(name.startsWith(s)) {
            			isFinsh=true;
            			//下载
            			ApplicationHome home=new ApplicationHome();
            			
            			File file=home.getSource().getParentFile();
            			File p=new File(file,"ftpResult");
            			p.mkdir();
            			File localFile=new File(p,name);
            			ftpClient.retrieveFile(name, new FileOutputStream(localFile));
            			break;
            		}
            	}
            }
            if(!isFinsh) {
            	timer1(type,filename);
            }
            
        } catch (Exception e) {
            log.error("上传异常：" + e.getMessage(), e);
        } finally {
            if (ftpClient != null && ftpClient.isConnected()) {
                try {
                    ftpClient.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void timer1(String type,String filename) {
        Timer timer = new Timer();  
        timer.schedule(new TimerTask() {  
            public void run() {
            	try {
            		getUploadResult(type,filename);
            	}catch(Exception e) {
            		log.error(ExceptionPrintUtils.getTrace(e));
            	}
            	
            }  
        }, 1000*60);//毫秒  
    }   

}
