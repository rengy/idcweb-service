package com.surfilter.ismi.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.jcraft.jsch.Logger;
import com.surfilter.ismi.client.WSClientService;
import com.surfilter.ismi.dto.ReturnNode;
import com.surfilter.ismi.dto.type0.IdcInfoManage;
import com.surfilter.ismi.util.EncryptAndDecryptUtil;
import com.surfilter.ismi.util.ExceptionPrintUtils;
import com.surfilter.ismi.util.ZipStringUtil;
import com.surfilter.ismi.util._Constant;
import com.surfilter.ismi.xmlbeans.FileUploadModel;
import com.surfilter.ismi.xmlbeans.UploadData;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 * 业务数据处理service
 */
@Slf4j
@Service
public class BizService {

	public ExecutorService executorService=Executors.newSingleThreadExecutor();
	@Autowired
	private XmlMapper xmlMapper;
	@Autowired
	private WSClientService wSClientService;
	
    public String genBasicInfo(String type, String idcId, Long commandId, Map commandInfo) {
        UploadData uploadData = new UploadData();
        if ("1".equals(type)) {
            // 新增
            UploadData.NewInfo newInfo = new UploadData.NewInfo();
            // TODO 根据 idcId 查询
            newInfo.setIdcID(idcId);
        }
        return null;
    }
//    private Marshaller marshaller;
//
//    @PostConstruct
//    public void init() {
//        try {
//            JAXBContext context = JAXBContext.newInstance(FileUploadModel.class);
//            marshaller = context.createMarshaller();
//            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//            marshaller.setProperty(Marshaller.JAXB_ENCODING, "utf-8");
//            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, false);
//            //            marshaller.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION,"http://www.w3.org/2001/XMLSchema");
//        } catch (JAXBException e) {
//            e.printStackTrace();
//        }
//    }
    /**
     * 生成文件上传的xml格式
     *
     * @param bizXmlStr
     * @return
     */
    public String genFileUploadXml(String idcId,String bizXmlStr) {
        log.info("上传的原始业务节点：{}", bizXmlStr);

        try {
        	/**
        	 * 使用参数compressionFormat指定的压缩格式对需要上报的数据进行压缩；
        	 * 对压缩后的信息使用参数encryptAlgorithm指定的加密算法加密，
        	 * 并对加密结果进行base64编码运算后得到的结果。
        	 */
            byte[] bizXmlZip = ZipStringUtil.compress(bizXmlStr);
            //log.info("zip压缩完成");
            String bizXmlBase64 = new String(Base64.encodeBase64(EncryptAndDecryptUtil.encrypt(bizXmlZip, _Constant.aeskey)),StandardCharsets.UTF_8);
            //log.info("Base64压缩完成:{}", bizXmlBase64);
            /**
             * 将上报的数据按照compressionFormat要求进行压缩后串接消息认证密钥，
             * 再根据hashAlgorithm参数进行哈希运算，并进行base64编码后的数据结果。
             */
            MessageDigest md5=DigestUtils.getMd5Digest();
            md5.update(bizXmlZip);
            md5.update(_Constant.authkey.getBytes(StandardCharsets.UTF_8));
            byte md5data[]=md5.digest();
            String hashHex=Hex.encodeHexString(md5data);
            String datahash= new String(Base64.encodeBase64(hashHex.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8);
//            FileUploadModel model = FileUploadModel.builder()
//            		.commandVersion("v2.0")
//            		.dataHash(datahash)
//            		.hashAlgorithm(1)
//            		.compressionFormat(1)
//            		.encryptAlgorithm(1)
//            		.dataUpload(bizXmlBase64)
//                    .idcId("11234")
//                    .build();
            //因为对方有顺序有要求，工具类生成的xml无法调整顺序，选择字符串拼接
            //String idcId="11234";
            StringBuilder sb=new StringBuilder("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            sb.append("<fileLoad>");
            sb.append("<idcId>").append(idcId).append("</idcId>");;
            sb.append("<dataUpload>").append(bizXmlBase64).append("</dataUpload>");;
            sb.append("<encryptAlgorithm>1</encryptAlgorithm>");
            sb.append("<compressionFormat>1</compressionFormat>");
            sb.append("<hashAlgorithm>1</hashAlgorithm>");
            sb.append("<dataHash>").append(datahash).append("</dataHash>");
            sb.append("<commandVersion>v2.0</commandVersion>");
            sb.append("</fileLoad>");
            

            //StringWriter writer = new StringWriter();
           // marshaller.marshal(model, writer);
            return sb.toString();
        } catch (Exception e) {
        	log.error(ExceptionPrintUtils.getTrace(e));
            return "";
        }

    }
    
    public String idcCommand(String idcId, String randVal, String pwdHash, String command, String commandHash, int commandType, long commandSequence, int encryptAlgorithm, int hashAlgorithm, int compressionFormat, String commandVersion) {
    	log.info("<===============================================");
		log.info("idcId={}",idcId);
		log.info("randVal={}",randVal);
		log.info("pwdHash={}",pwdHash);
		log.info("command={}",command);
		log.info("commandHash={}",commandHash);
		log.info("commandType={}",commandType);
		log.info("commandSequence={}",commandSequence);
		log.info("encryptAlgorithm={}",encryptAlgorithm);
		log.info("hashAlgorithm={}",hashAlgorithm);
		log.info("compressionFormat={}",compressionFormat);
		log.info("commandVersion={}",commandVersion);
		log.info("===============================================>");
		
		
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				try {
					idc_command0(  idcId,   randVal,   pwdHash,   command,   commandHash,
							 commandType,   commandSequence,   encryptAlgorithm,   hashAlgorithm,
							  compressionFormat,  commandVersion);
				}catch(Exception e) {
					log.error(ExceptionPrintUtils.getTrace(e));
				}
			}
		});
		try {
			return xmlMapper.writeValueAsString(new ReturnNode());
		} catch (JsonProcessingException e) {
			log.error(ExceptionPrintUtils.getTrace(e));
		}
		return "fail";
    	
    }
    
    private void idc_command0( String idcId,  String randVal,  String pwdHash,  String command,  String commandHash,
    		Integer commandType,  Long commandSequence,  Integer encryptAlgorithm,  Integer hashAlgorithm,
    		 Integer compressionFormat, String commandVersion) throws Exception {
    	/**
    	 * 1.ISMS收到指令文件后，首先对command进行base64反解码，
    	 * 2.然后采用参数encryptAlgorithm指定的加密算法对解码后的数据进行解密处理，得到data；
    	 * 3.针对data串接消息认证密钥后，使用hashAlgorithm指定的哈希算法计算哈希值，
    	 * 将得到的哈希值与收到的commandHash进行比较，如果一致，
    	 * 则对指令文件的完整性校验通过。===========暂时不做校验
    	 * 4.进一步按照compressionFormat指定的压缩格式对data进行解压后即得到指令信息。
    	 */
    	byte[] base64_command=Base64.decodeBase64(command);

    	byte[] aes_command=EncryptAndDecryptUtil.decrypt(base64_command, _Constant.aeskey);

    	String data=ZipStringUtil.decompress(aes_command);

    	log.info("原始报文：{}",data);
    	//回调
    	/**
    	 * 指令类型如下。
    	0：基础数据管理指令（基础数据核验处理指令）；
    	1：访问日志查询指令；
    	2：信息安全管理指令（违法网站列表管理指令、免过滤网站列表管理指令、违法信息监测和处置指令管理指令）；
    	3：保留
    	4：代码表发布指令
    	5：基础数据查询指令（基础数据记录查询指令、基础数据监测异常记录查询指令）
    	6：信息安全查询指令（活跃资源监测记录查询指令、违法违规网站监测记录查询指令、违法信息监测和处置记录查询指令）
    	 */
    	Long commandId=0L;
    	if(commandType==0) {
    		log.info("0：基础数据管理指令（基础数据核验处理指令）");
    		//
    	}else if(commandType==1 ) {
    		log.info("1：访问日志查询指令；");
    	}else if(commandType==2 ) {
    		log.info("2：信息安全管理指令（违法网站列表管理指令、免过滤网站列表管理指令、违法信息监测和处置指令管理指令）；");
    	}else if(commandType==3 ) {
    		log.info("3：保留");
    	}else if(commandType==4 ) {
    		log.info("4：代码表发布指令");
    	}else if(commandType==5 ) {
    		log.info("5：基础数据查询指令（基础数据记录查询指令、基础数据监测异常记录查询指令）");
    		IdcInfoManage idcInfoManage=xmlMapper.readValue(data, IdcInfoManage.class);
    		commandId=idcInfoManage.getCommandId();
    	}else if(commandType==6 ) {
    		log.info("6：信息安全查询指令（活跃资源监测记录查询指令、违法违规网站监测记录查询指令、违法信息监测和处置记录查询指令）");
    	}
    	//ClassPathResource classPathResource = new ClassPathResource("returndata/commandType0.xml");
    	//File file=classPathResource.getFile();
    	//log.info(file.getAbsolutePath());
    	//String content=FileUtils.readFileToString(file,StandardCharsets.UTF_8);
    	//log.info(content);

    	wSClientService.idc_commandack(commandId,randVal);
	}



	//每间隔10分钟执行
	@Scheduled(cron="0 */10 * * * ?") 
	public void schedule10(){
		
	}
}
