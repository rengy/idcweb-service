package com.surfilter.ismi;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class IDCWebServiceApplication {
    public static void main(String[] args) {
    	ApplicationContext ctx=
    	SpringApplication.run(IDCWebServiceApplication.class, args);
    }
}