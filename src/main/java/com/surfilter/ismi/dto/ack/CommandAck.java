package com.surfilter.ismi.dto.ack;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommandAck {

	private Long houseId;
	
	private Long commandId;
	
	private Integer type;
	/**指令惟一ID，该ID由SMMS产生，包括：
1——违法信息监测指令；
2——违法信息过滤指令；
3——保留
4——代码发布指令
5——免过滤网站列表指令
6——违法网站列表指令
7——活跃资源访问量查询指令
8——违法信息管理查询指令**/
	private Integer resultCode;
	
	private Long view;
	
	private Long monitorCount;
	
	private Long filterCount;
	
	private String msgInfo;
}
