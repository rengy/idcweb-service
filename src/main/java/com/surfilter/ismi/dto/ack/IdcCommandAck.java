package com.surfilter.ismi.dto.ack;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdcCommandAck {

	private String idcId;
	
	private CommandAck commandAck;
	
	private String timeStamp;
}
