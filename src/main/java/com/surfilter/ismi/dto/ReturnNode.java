package com.surfilter.ismi.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JacksonXmlRootElement(localName = "return")
public class ReturnNode {

	private int resultCode;
	private String msg;
}
