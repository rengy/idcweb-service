package com.surfilter.ismi.dto.type0;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JacksonXmlRootElement(localName = "returnInfo")
public class Type0ReturnInfo {
	private String idcId;
	
	private String returnData;
	
	private String returnCode;
	
	
	private String returnMsg;
	
	private String timeStamp;
	
}
