package com.surfilter.ismi.dto.type0;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Type0ReturnData {

	private String house;
	
	private String user;
}
