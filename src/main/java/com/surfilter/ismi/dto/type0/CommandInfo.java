package com.surfilter.ismi.dto.type0;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommandInfo {
	private String queryMonitorDayFrom;
	private String queryMonitorDayTo;
	private String idcId;
	private Long id;
	private Long houseId;
}
