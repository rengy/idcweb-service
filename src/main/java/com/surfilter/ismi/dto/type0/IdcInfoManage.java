package com.surfilter.ismi.dto.type0;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdcInfoManage {

	private Long commandId;
	private Integer type;
	private CommandInfo commandInfo;
	private String timestamp;
}
