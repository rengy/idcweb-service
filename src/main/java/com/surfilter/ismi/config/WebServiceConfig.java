package com.surfilter.ismi.config;
import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.surfilter.ismi.webservice.IdcWebService;
 
@Configuration
public class WebServiceConfig {
 
    @Autowired
    private IdcWebService idcWebService;
 
    
 
 
    @Bean(name = Bus.DEFAULT_BUS_ID)
    public SpringBus springBus() {
        return new SpringBus();
    }
 
    /**
     * 注册 接口到webservice服务
     * @return
     */
    @Bean
    public Endpoint sweptPayEndpoint() {
        EndpointImpl endpoint = new EndpointImpl(springBus(), idcWebService);
        endpoint.publish("/idcCommand");
        return endpoint;
    }
 
}