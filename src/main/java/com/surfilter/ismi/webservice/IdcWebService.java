package com.surfilter.ismi.webservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.14
 * 2021-08-20T11:53:18.601+08:00
 * Generated source version: 3.2.14
 *
 */
@WebService(targetNamespace = "http://webservice.ismi.surfilter.com/", name = "IdcWebService")
//@XmlSeeAlso({ObjectFactory.class})
public interface IdcWebService {


	@WebMethod
    public void sendAck(
        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.Long arg1,
        @WebParam(name = "arg2", targetNamespace = "")
        java.lang.Long arg2
    );
	
	@WebMethod(operationName = "idc_commandack", action = "http://webservice.ismi.surfilter.com/idc_commandack")
    //@RequestWrapper(localName = "idc_commandack", className = "com.surfilter.ismi.webservice.IdcCommandack")
    //@ResponseWrapper(localName = "idc_commandackResponse", className = "com.surfilter.ismi.webservice.IdcCommandackResponse")
    //@WebResult(name = "return", targetNamespace = "")
    public java.lang.String idcCommandack(
            @WebParam(name = "idcId", targetNamespace = "http://webservice.ismi.surfilter.com/")
            java.lang.String idcId,
            @WebParam(name = "randVal", targetNamespace = "http://webservice.ismi.surfilter.com/")
            java.lang.String randVal,
            @WebParam(name = "pwdHash", targetNamespace = "http://webservice.ismi.surfilter.com/")
            java.lang.String pwdHash,
            @WebParam(name = "result", targetNamespace = "http://webservice.ismi.surfilter.com/")
            java.lang.String result,
            @WebParam(name = "resultHash", targetNamespace = "http://webservice.ismi.surfilter.com/")
            java.lang.String resultHash,
            @WebParam(name = "encryptAlgorithm", targetNamespace = "http://webservice.ismi.surfilter.com/")
            int encryptAlgorithm,
            @WebParam(name = "hashAlgorithm", targetNamespace = "http://webservice.ismi.surfilter.com/")
            int hashAlgorithm,
            @WebParam(name = "compressionFormat", targetNamespace = "http://webservice.ismi.surfilter.com/")
            int compressionFormat,
            @WebParam(name = "commandVersion", targetNamespace = "http://webservice.ismi.surfilter.com/")
            java.lang.String commandVersion
        );
	
	
	
	@WebMethod(operationName = "idc_command", action = "http://webservice.ismi.surfilter.com/idc_command")
    public java.lang.String idcCommand(
        @WebParam(partName = "idcId", name = "idcId")
        java.lang.String idcId,
        @WebParam(partName = "randVal", name = "randVal")
        java.lang.String randVal,
        @WebParam(partName = "pwdHash", name = "pwdHash")
        java.lang.String pwdHash,
        @WebParam(partName = "command", name = "command")
        java.lang.String command,
        @WebParam(partName = "commandHash", name = "commandHash")
        java.lang.String commandHash,
        @WebParam(partName = "commandType", name = "commandType")
        int commandType,
        @WebParam(partName = "commandSequence", name = "commandSequence")
        long commandSequence,
        @WebParam(partName = "encryptAlgorithm", name = "encryptAlgorithm")
        int encryptAlgorithm,
        @WebParam(partName = "hashAlgorithm", name = "hashAlgorithm")
        int hashAlgorithm,
        @WebParam(partName = "compressionFormat", name = "compressionFormat")
        int compressionFormat,
        @WebParam(partName = "commandVersion", name = "commandVersion")
        java.lang.String commandVersion
    );
    		
}
