package com.surfilter.ismi.xmlbeans;

import lombok.*;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 文件上传
 */
@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "fileLoad")
public class FileUploadModel {

    private String idcId; //	IDC/ISP经营者ID	必填	字符串	128	电信管理部门颁发的IDC/ISP许可证号

    /*
    	数据上报内容	必填	字符串
    	使用参数compressionFormat指定的压缩格式对需要上报的数据进行压缩；
    	对压缩后的信息使用参数encryptAlgorithm指定的加密算法加密，并对加密结果进行base64编码运算后得到的结果。
    	上报的数据包括基础数据、基础数据监测数据、访问日志查询结果、监测日志、过滤日志、信息安全管理指令查询结果以及ISMS活动状态等（数据内容见第11章）。
    压缩前的上报数据应符合11章相应的数据上报内容要求，且小于12M
     */
    private String dataUpload;

    /*
    对称加密算法	必填	整型
    对称加密算法如下。
            0：不进行加密，明文传输；
            1：AES加密算法。
    加密密钥由SMMS与ISMS事先配置确定，长度至少为20字节，最多32字节。
    ISMS应根据SMMS的要求完成加密算法的具体实现。
    ISMS至少应支持采用CBC模式、PKCS7Padding补码方式实现AES加密算法，并可根据SMMS的要求设置AES密钥长度、加密偏移量等参数。
     */
    private Integer encryptAlgorithm;

    /*
    压缩格式	必填	整型
    压缩格式如下。
            0：无压缩；
            1：Zip压缩格式。
    ISMS应根据SMMS的要求完成压缩算法的具体实现
     */
    private Integer compressionFormat;

    /*
    哈希算法	必填	整型
    哈希算法如下。
            0：无hash；
            1：MD5；
            2：SHA-1。
    ISMS应根据SMMS的要求完成哈希算法的具体实现
     */
    private Integer hashAlgorithm;

    /*
    数据的哈希结果	选填	字符串	64
    将上报的数据按照compressionFormat要求进行压缩后串接消息认证密钥，
    再根据hashAlgorithm参数进行哈希运算，并进行base64编码后的数据结果。
    消息认证密钥由SMMS和ISMS通过配置确定，长度至少为20位，最多32位
     */
    private String dataHash;

    private String commandVersion; // 接口方法版本 必填 字符串	4符合本文件所规定要求的ISMI接口方法为“v2.0”

}
