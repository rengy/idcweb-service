//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.2.8-b130911.1802 生成的
// 请访问 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2021.08.18 时间 11:04:46 AM CST 
//


package com.surfilter.ismi.xmlbeans;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idcCommandID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="command" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="commandID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="rule" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="subtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="valueStart" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="valueEnd" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                             &lt;element name="keywordRange" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="action">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="block" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                             &lt;element name="log" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="report" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="time">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="effectTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="expiredTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="range" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="idcID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                             &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="privilege">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="owner" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="visible" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="operationType" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="timeStamp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idcCommandID",
    "command",
    "timeStamp"
})
@XmlRootElement(name = "idcCommand")
public class IdcCommand {

    @XmlElement(required = true)
    protected String idcCommandID;
    @XmlElement(required = true)
    protected List<Command> command;
    @XmlElement(required = true)
    protected String timeStamp;

    /**
     * 获取idcCommandID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdcCommandID() {
        return idcCommandID;
    }

    /**
     * 设置idcCommandID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdcCommandID(String value) {
        this.idcCommandID = value;
    }

    /**
     * Gets the value of the command property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the command property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommand().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Command }
     * 
     * 
     */
    public List<Command> getCommand() {
        if (command == null) {
            command = new ArrayList<Command>();
        }
        return this.command;
    }

    /**
     * 获取timeStamp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     * 设置timeStamp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeStamp(String value) {
        this.timeStamp = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="commandID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="rule" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="subtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="valueStart" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="valueEnd" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                   &lt;element name="keywordRange" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="action">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="block" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                   &lt;element name="log" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="report" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="time">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="effectTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="expiredTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="range" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="idcID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                   &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="privilege">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="owner" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="visible" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="operationType" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "commandID",
        "type",
        "rule",
        "action",
        "time",
        "range",
        "privilege",
        "operationType"
    })
    public static class Command {

        @XmlElement(required = true)
        protected Object commandID;
        @XmlElement(required = true)
        protected Object type;
        @XmlElement(required = true)
        protected List<Rule> rule;
        @XmlElement(required = true)
        protected Action action;
        @XmlElement(required = true)
        protected Time time;
        protected Range range;
        @XmlElement(required = true)
        protected Privilege privilege;
        @XmlElement(required = true)
        protected Object operationType;

        /**
         * 获取commandID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getCommandID() {
            return commandID;
        }

        /**
         * 设置commandID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setCommandID(Object value) {
            this.commandID = value;
        }

        /**
         * 获取type属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getType() {
            return type;
        }

        /**
         * 设置type属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setType(Object value) {
            this.type = value;
        }

        /**
         * Gets the value of the rule property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the rule property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRule().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Rule }
         * 
         * 
         */
        public List<Rule> getRule() {
            if (rule == null) {
                rule = new ArrayList<Rule>();
            }
            return this.rule;
        }

        /**
         * 获取action属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Action }
         *     
         */
        public Action getAction() {
            return action;
        }

        /**
         * 设置action属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Action }
         *     
         */
        public void setAction(Action value) {
            this.action = value;
        }

        /**
         * 获取time属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Time }
         *     
         */
        public Time getTime() {
            return time;
        }

        /**
         * 设置time属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Time }
         *     
         */
        public void setTime(Time value) {
            this.time = value;
        }

        /**
         * 获取range属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Range }
         *     
         */
        public Range getRange() {
            return range;
        }

        /**
         * 设置range属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Range }
         *     
         */
        public void setRange(Range value) {
            this.range = value;
        }

        /**
         * 获取privilege属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Privilege }
         *     
         */
        public Privilege getPrivilege() {
            return privilege;
        }

        /**
         * 设置privilege属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Privilege }
         *     
         */
        public void setPrivilege(Privilege value) {
            this.privilege = value;
        }

        /**
         * 获取operationType属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getOperationType() {
            return operationType;
        }

        /**
         * 设置operationType属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setOperationType(Object value) {
            this.operationType = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="block" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *         &lt;element name="log" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="report" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "block",
            "reason",
            "log",
            "report"
        })
        public static class Action {

            @XmlElement(required = true)
            protected Object block;
            protected Object reason;
            @XmlElement(required = true)
            protected Object log;
            @XmlElement(required = true)
            protected Object report;

            /**
             * 获取block属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getBlock() {
                return block;
            }

            /**
             * 设置block属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setBlock(Object value) {
                this.block = value;
            }

            /**
             * 获取reason属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getReason() {
                return reason;
            }

            /**
             * 设置reason属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setReason(Object value) {
                this.reason = value;
            }

            /**
             * 获取log属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getLog() {
                return log;
            }

            /**
             * 设置log属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setLog(Object value) {
                this.log = value;
            }

            /**
             * 获取report属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getReport() {
                return report;
            }

            /**
             * 设置report属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setReport(Object value) {
                this.report = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="owner" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="visible" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "owner",
            "visible"
        })
        public static class Privilege {

            @XmlElement(required = true)
            protected Object owner;
            @XmlElement(required = true)
            protected Object visible;

            /**
             * 获取owner属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getOwner() {
                return owner;
            }

            /**
             * 设置owner属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setOwner(Object value) {
                this.owner = value;
            }

            /**
             * 获取visible属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getVisible() {
                return visible;
            }

            /**
             * 设置visible属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setVisible(Object value) {
                this.visible = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="idcID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "idcID",
            "houseID"
        })
        public static class Range {

            protected Object idcID;
            protected List<Object> houseID;

            /**
             * 获取idcID属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getIdcID() {
                return idcID;
            }

            /**
             * 设置idcID属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setIdcID(Object value) {
                this.idcID = value;
            }

            /**
             * Gets the value of the houseID property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the houseID property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getHouseID().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Object }
             * 
             * 
             */
            public List<Object> getHouseID() {
                if (houseID == null) {
                    houseID = new ArrayList<Object>();
                }
                return this.houseID;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="subtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="valueStart" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="valueEnd" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *         &lt;element name="keywordRange" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "subtype",
            "valueStart",
            "valueEnd",
            "keywordRange"
        })
        public static class Rule {

            @XmlElement(required = true)
            protected Object subtype;
            @XmlElement(required = true)
            protected Object valueStart;
            protected Object valueEnd;
            protected List<Object> keywordRange;

            /**
             * 获取subtype属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getSubtype() {
                return subtype;
            }

            /**
             * 设置subtype属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setSubtype(Object value) {
                this.subtype = value;
            }

            /**
             * 获取valueStart属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getValueStart() {
                return valueStart;
            }

            /**
             * 设置valueStart属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setValueStart(Object value) {
                this.valueStart = value;
            }

            /**
             * 获取valueEnd属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getValueEnd() {
                return valueEnd;
            }

            /**
             * 设置valueEnd属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setValueEnd(Object value) {
                this.valueEnd = value;
            }

            /**
             * Gets the value of the keywordRange property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the keywordRange property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getKeywordRange().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Object }
             * 
             * 
             */
            public List<Object> getKeywordRange() {
                if (keywordRange == null) {
                    keywordRange = new ArrayList<Object>();
                }
                return this.keywordRange;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="effectTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="expiredTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "effectTime",
            "expiredTime"
        })
        public static class Time {

            @XmlElement(required = true)
            protected Object effectTime;
            @XmlElement(required = true)
            protected Object expiredTime;

            /**
             * 获取effectTime属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getEffectTime() {
                return effectTime;
            }

            /**
             * 设置effectTime属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setEffectTime(Object value) {
                this.effectTime = value;
            }

            /**
             * 获取expiredTime属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getExpiredTime() {
                return expiredTime;
            }

            /**
             * 设置expiredTime属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setExpiredTime(Object value) {
                this.expiredTime = value;
            }

        }

    }

}
