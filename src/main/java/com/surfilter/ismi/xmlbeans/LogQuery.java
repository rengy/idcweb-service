//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.2.8-b130911.1802 生成的
// 请访问 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2021.08.18 时间 11:04:46 AM CST 
//


package com.surfilter.ismi.xmlbeans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="commandID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="idcID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="startTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="endTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="srcIP" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="destIP" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="srcPort" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="dstPort" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="timeStamp" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "commandID",
    "idcID",
    "houseID",
    "startTime",
    "endTime",
    "srcIP",
    "destIP",
    "srcPort",
    "dstPort",
    "url",
    "timeStamp"
})
@XmlRootElement(name = "logQuery")
public class LogQuery {

    @XmlElement(required = true)
    protected Object commandID;
    @XmlElement(required = true)
    protected Object idcID;
    @XmlElement(required = true)
    protected Object houseID;
    @XmlElement(required = true)
    protected Object startTime;
    @XmlElement(required = true)
    protected Object endTime;
    protected SrcIP srcIP;
    protected DestIP destIP;
    protected Object srcPort;
    protected Object dstPort;
    protected Object url;
    @XmlElement(required = true)
    protected Object timeStamp;

    /**
     * 获取commandID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getCommandID() {
        return commandID;
    }

    /**
     * 设置commandID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setCommandID(Object value) {
        this.commandID = value;
    }

    /**
     * 获取idcID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getIdcID() {
        return idcID;
    }

    /**
     * 设置idcID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setIdcID(Object value) {
        this.idcID = value;
    }

    /**
     * 获取houseID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getHouseID() {
        return houseID;
    }

    /**
     * 设置houseID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setHouseID(Object value) {
        this.houseID = value;
    }

    /**
     * 获取startTime属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getStartTime() {
        return startTime;
    }

    /**
     * 设置startTime属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setStartTime(Object value) {
        this.startTime = value;
    }

    /**
     * 获取endTime属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getEndTime() {
        return endTime;
    }

    /**
     * 设置endTime属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setEndTime(Object value) {
        this.endTime = value;
    }

    /**
     * 获取srcIP属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SrcIP }
     *     
     */
    public SrcIP getSrcIP() {
        return srcIP;
    }

    /**
     * 设置srcIP属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SrcIP }
     *     
     */
    public void setSrcIP(SrcIP value) {
        this.srcIP = value;
    }

    /**
     * 获取destIP属性的值。
     * 
     * @return
     *     possible object is
     *     {@link DestIP }
     *     
     */
    public DestIP getDestIP() {
        return destIP;
    }

    /**
     * 设置destIP属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DestIP }
     *     
     */
    public void setDestIP(DestIP value) {
        this.destIP = value;
    }

    /**
     * 获取srcPort属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getSrcPort() {
        return srcPort;
    }

    /**
     * 设置srcPort属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setSrcPort(Object value) {
        this.srcPort = value;
    }

    /**
     * 获取dstPort属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getDstPort() {
        return dstPort;
    }

    /**
     * 设置dstPort属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setDstPort(Object value) {
        this.dstPort = value;
    }

    /**
     * 获取url属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getUrl() {
        return url;
    }

    /**
     * 设置url属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setUrl(Object value) {
        this.url = value;
    }

    /**
     * 获取timeStamp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getTimeStamp() {
        return timeStamp;
    }

    /**
     * 设置timeStamp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setTimeStamp(Object value) {
        this.timeStamp = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "startIP",
        "endIP"
    })
    public static class DestIP {

        @XmlElement(required = true)
        protected Object startIP;
        @XmlElement(required = true)
        protected Object endIP;

        /**
         * 获取startIP属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getStartIP() {
            return startIP;
        }

        /**
         * 设置startIP属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setStartIP(Object value) {
            this.startIP = value;
        }

        /**
         * 获取endIP属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getEndIP() {
            return endIP;
        }

        /**
         * 设置endIP属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setEndIP(Object value) {
            this.endIP = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "startIP",
        "endIP"
    })
    public static class SrcIP {

        @XmlElement(required = true)
        protected Object startIP;
        @XmlElement(required = true)
        protected Object endIP;

        /**
         * 获取startIP属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getStartIP() {
            return startIP;
        }

        /**
         * 设置startIP属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setStartIP(Object value) {
            this.startIP = value;
        }

        /**
         * 获取endIP属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getEndIP() {
            return endIP;
        }

        /**
         * 设置endIP属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setEndIP(Object value) {
            this.endIP = value;
        }

    }

}
