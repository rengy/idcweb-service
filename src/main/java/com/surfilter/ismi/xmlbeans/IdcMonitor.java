//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.2.8-b130911.1802 生成的
// 请访问 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2021.08.18 时间 11:04:46 AM CST 
//


package com.surfilter.ismi.xmlbeans;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="commandID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="idcID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="monitorState" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="houseMonitor" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="IP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="port" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="domain" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                   &lt;element name="serviceType" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded"/>
 *                   &lt;element name="firstFound" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="lastFound" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="illegalInfo">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="illegalType" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="currentState" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="IPinfo">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="error" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="regDomain" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="timeStamp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "commandID",
    "idcID",
    "monitorState",
    "houseMonitor",
    "timeStamp"
})
@XmlRootElement(name = "idcMonitor")
public class IdcMonitor {

    protected Object commandID;
    @XmlElement(required = true)
    protected Object idcID;
    @XmlElement(required = true)
    protected Object monitorState;
    @XmlElement(required = true)
    protected List<HouseMonitor> houseMonitor;
    @XmlElement(required = true)
    protected String timeStamp;

    /**
     * 获取commandID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getCommandID() {
        return commandID;
    }

    /**
     * 设置commandID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setCommandID(Object value) {
        this.commandID = value;
    }

    /**
     * 获取idcID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getIdcID() {
        return idcID;
    }

    /**
     * 设置idcID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setIdcID(Object value) {
        this.idcID = value;
    }

    /**
     * 获取monitorState属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getMonitorState() {
        return monitorState;
    }

    /**
     * 设置monitorState属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setMonitorState(Object value) {
        this.monitorState = value;
    }

    /**
     * Gets the value of the houseMonitor property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the houseMonitor property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHouseMonitor().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HouseMonitor }
     * 
     * 
     */
    public List<HouseMonitor> getHouseMonitor() {
        if (houseMonitor == null) {
            houseMonitor = new ArrayList<HouseMonitor>();
        }
        return this.houseMonitor;
    }

    /**
     * 获取timeStamp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     * 设置timeStamp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeStamp(String value) {
        this.timeStamp = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="IP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="port" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="domain" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *         &lt;element name="serviceType" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded"/>
     *         &lt;element name="firstFound" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="lastFound" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="illegalInfo">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="illegalType" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="currentState" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="IPinfo">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="error" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="regDomain" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "id",
        "ip",
        "port",
        "domain",
        "serviceType",
        "firstFound",
        "lastFound",
        "illegalInfo",
        "iPinfo"
    })
    public static class HouseMonitor {

        @XmlElement(name = "ID", required = true)
        protected Object id;
        @XmlElement(name = "IP", required = true)
        protected Object ip;
        @XmlElement(required = true)
        protected Object port;
        protected Object domain;
        @XmlElement(required = true)
        protected List<Object> serviceType;
        @XmlElement(required = true)
        protected Object firstFound;
        @XmlElement(required = true)
        protected Object lastFound;
        @XmlElement(required = true)
        protected IllegalInfo illegalInfo;
        @XmlElement(name = "IPinfo", required = true)
        protected IPinfo iPinfo;

        /**
         * 获取id属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getID() {
            return id;
        }

        /**
         * 设置id属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setID(Object value) {
            this.id = value;
        }

        /**
         * 获取ip属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getIP() {
            return ip;
        }

        /**
         * 设置ip属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setIP(Object value) {
            this.ip = value;
        }

        /**
         * 获取port属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getPort() {
            return port;
        }

        /**
         * 设置port属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setPort(Object value) {
            this.port = value;
        }

        /**
         * 获取domain属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getDomain() {
            return domain;
        }

        /**
         * 设置domain属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setDomain(Object value) {
            this.domain = value;
        }

        /**
         * Gets the value of the serviceType property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the serviceType property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getServiceType().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Object }
         * 
         * 
         */
        public List<Object> getServiceType() {
            if (serviceType == null) {
                serviceType = new ArrayList<Object>();
            }
            return this.serviceType;
        }

        /**
         * 获取firstFound属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getFirstFound() {
            return firstFound;
        }

        /**
         * 设置firstFound属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setFirstFound(Object value) {
            this.firstFound = value;
        }

        /**
         * 获取lastFound属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getLastFound() {
            return lastFound;
        }

        /**
         * 设置lastFound属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setLastFound(Object value) {
            this.lastFound = value;
        }

        /**
         * 获取illegalInfo属性的值。
         * 
         * @return
         *     possible object is
         *     {@link IllegalInfo }
         *     
         */
        public IllegalInfo getIllegalInfo() {
            return illegalInfo;
        }

        /**
         * 设置illegalInfo属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link IllegalInfo }
         *     
         */
        public void setIllegalInfo(IllegalInfo value) {
            this.illegalInfo = value;
        }

        /**
         * 获取iPinfo属性的值。
         * 
         * @return
         *     possible object is
         *     {@link IPinfo }
         *     
         */
        public IPinfo getIPinfo() {
            return iPinfo;
        }

        /**
         * 设置iPinfo属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link IPinfo }
         *     
         */
        public void setIPinfo(IPinfo value) {
            this.iPinfo = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="error" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="regDomain" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "error",
            "regDomain"
        })
        public static class IPinfo {

            @XmlElement(required = true)
            protected Object error;
            @XmlElement(required = true)
            protected Object regDomain;

            /**
             * 获取error属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getError() {
                return error;
            }

            /**
             * 设置error属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setError(Object value) {
                this.error = value;
            }

            /**
             * 获取regDomain属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getRegDomain() {
                return regDomain;
            }

            /**
             * 设置regDomain属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setRegDomain(Object value) {
                this.regDomain = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="illegalType" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="currentState" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "illegalType",
            "currentState",
            "user"
        })
        public static class IllegalInfo {

            @XmlElement(required = true)
            protected Object illegalType;
            @XmlElement(required = true)
            protected Object currentState;
            @XmlElement(required = true)
            protected Object user;

            /**
             * 获取illegalType属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getIllegalType() {
                return illegalType;
            }

            /**
             * 设置illegalType属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setIllegalType(Object value) {
                this.illegalType = value;
            }

            /**
             * 获取currentState属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getCurrentState() {
                return currentState;
            }

            /**
             * 设置currentState属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setCurrentState(Object value) {
                this.currentState = value;
            }

            /**
             * 获取user属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getUser() {
                return user;
            }

            /**
             * 设置user属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setUser(Object value) {
                this.user = value;
            }

        }

    }

}
