//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.2.8-b130911.1802 生成的
// 请访问 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2021.08.18 时间 11:04:46 AM CST 
//


package com.surfilter.ismi.xmlbeans;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="newInfo">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="idcID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="idcName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="idcAdd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="idcZip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="corp" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                     &lt;element name="idcOfficer" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="emergencyContact" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="houseInfo" maxOccurs="unbounded" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="houseName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="houseType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="houseProvince" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="houseCity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="houseCounty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="houseAdd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="houseZip" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="houseOfficer">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                               &lt;element name="gatewayInfo" maxOccurs="unbounded">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                         &lt;element name="gatewayIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                               &lt;element name="IPsegInfo" maxOccurs="unbounded">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                         &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                         &lt;element name="useTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="userInfo" maxOccurs="unbounded" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="nature" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="info">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="unitName" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="unitNature" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="officer">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                   &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                   &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                         &lt;element name="add" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="zipCode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;choice>
 *                                           &lt;element name="serviceInfo" maxOccurs="unbounded" minOccurs="0">
 *                                             &lt;complexType>
 *                                               &lt;complexContent>
 *                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                   &lt;sequence>
 *                                                     &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                     &lt;element name="serviceContent" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded"/>
 *                                                     &lt;element name="regType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                     &lt;element name="regID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                     &lt;element name="setMode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                     &lt;element name="domain" maxOccurs="unbounded" minOccurs="0">
 *                                                       &lt;complexType>
 *                                                         &lt;complexContent>
 *                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                             &lt;sequence>
 *                                                               &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                               &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                             &lt;/sequence>
 *                                                           &lt;/restriction>
 *                                                         &lt;/complexContent>
 *                                                       &lt;/complexType>
 *                                                     &lt;/element>
 *                                                     &lt;element name="housesHoldInfo" maxOccurs="unbounded">
 *                                                       &lt;complexType>
 *                                                         &lt;complexContent>
 *                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                             &lt;sequence>
 *                                                               &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                               &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                               &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                               &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                               &lt;element name="IPtrans" maxOccurs="unbounded">
 *                                                                 &lt;complexType>
 *                                                                   &lt;complexContent>
 *                                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                       &lt;sequence>
 *                                                                         &lt;element name="internetIP">
 *                                                                           &lt;complexType>
 *                                                                             &lt;complexContent>
 *                                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                                 &lt;sequence>
 *                                                                                   &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                                                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                                                 &lt;/sequence>
 *                                                                               &lt;/restriction>
 *                                                                             &lt;/complexContent>
 *                                                                           &lt;/complexType>
 *                                                                         &lt;/element>
 *                                                                         &lt;element name="netIP" minOccurs="0">
 *                                                                           &lt;complexType>
 *                                                                             &lt;complexContent>
 *                                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                                 &lt;sequence>
 *                                                                                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                                                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                                                 &lt;/sequence>
 *                                                                               &lt;/restriction>
 *                                                                             &lt;/complexContent>
 *                                                                           &lt;/complexType>
 *                                                                         &lt;/element>
 *                                                                       &lt;/sequence>
 *                                                                     &lt;/restriction>
 *                                                                   &lt;/complexContent>
 *                                                                 &lt;/complexType>
 *                                                               &lt;/element>
 *                                                             &lt;/sequence>
 *                                                           &lt;/restriction>
 *                                                         &lt;/complexContent>
 *                                                       &lt;/complexType>
 *                                                     &lt;/element>
 *                                                   &lt;/sequence>
 *                                                 &lt;/restriction>
 *                                               &lt;/complexContent>
 *                                             &lt;/complexType>
 *                                           &lt;/element>
 *                                           &lt;element name="houseHoldInfo" maxOccurs="unbounded" minOccurs="0">
 *                                             &lt;complexType>
 *                                               &lt;complexContent>
 *                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                   &lt;sequence>
 *                                                     &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                     &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                     &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                     &lt;element name="IPseg" maxOccurs="unbounded">
 *                                                       &lt;complexType>
 *                                                         &lt;complexContent>
 *                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                             &lt;sequence>
 *                                                               &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                               &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                             &lt;/sequence>
 *                                                           &lt;/restriction>
 *                                                         &lt;/complexContent>
 *                                                       &lt;/complexType>
 *                                                     &lt;/element>
 *                                                     &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                   &lt;/sequence>
 *                                                 &lt;/restriction>
 *                                               &lt;/complexContent>
 *                                             &lt;/complexType>
 *                                           &lt;/element>
 *                                         &lt;/choice>
 *                                         &lt;element name="registerTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="updateInfo">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="idcID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="idcName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="idcAdd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="idcZip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="corp" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                     &lt;element name="idcOfficer" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="emergencyContact" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="updateData">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="houseInfo" maxOccurs="unbounded" minOccurs="0">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                         &lt;element name="houseName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="houseType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="houseProvince" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="houseCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="houseCounty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="houseAdd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="houseZip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="houseOfficer" minOccurs="0">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                   &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                   &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                         &lt;element name="gatewayInfo" maxOccurs="unbounded" minOccurs="0">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                   &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                   &lt;element name="gatewayIP" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                         &lt;element name="IPsegInfo" maxOccurs="unbounded" minOccurs="0">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                   &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                   &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                   &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                   &lt;element name="useTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                               &lt;element name="userInfo" maxOccurs="unbounded" minOccurs="0">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                         &lt;element name="nature" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                         &lt;element name="info" minOccurs="0">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="unitName" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                   &lt;element name="unitNature" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                   &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                   &lt;element name="officer" minOccurs="0">
 *                                                     &lt;complexType>
 *                                                       &lt;complexContent>
 *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                           &lt;sequence>
 *                                                             &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                             &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                             &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                             &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                             &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                           &lt;/sequence>
 *                                                         &lt;/restriction>
 *                                                       &lt;/complexContent>
 *                                                     &lt;/complexType>
 *                                                   &lt;/element>
 *                                                   &lt;element name="add" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                   &lt;element name="zipCode" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                   &lt;choice>
 *                                                     &lt;element name="serviceInfo" maxOccurs="unbounded" minOccurs="0">
 *                                                       &lt;complexType>
 *                                                         &lt;complexContent>
 *                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                             &lt;sequence>
 *                                                               &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                               &lt;element name="serviceContent" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *                                                               &lt;element name="regType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                               &lt;element name="regID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                               &lt;element name="setMode" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                               &lt;element name="domain" maxOccurs="unbounded" minOccurs="0">
 *                                                                 &lt;complexType>
 *                                                                   &lt;complexContent>
 *                                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                       &lt;sequence>
 *                                                                         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                                         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                                       &lt;/sequence>
 *                                                                     &lt;/restriction>
 *                                                                   &lt;/complexContent>
 *                                                                 &lt;/complexType>
 *                                                               &lt;/element>
 *                                                               &lt;element name="housesHoldInfo" minOccurs="0">
 *                                                                 &lt;complexType>
 *                                                                   &lt;complexContent>
 *                                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                       &lt;sequence>
 *                                                                         &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                                         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                                         &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                                         &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                                         &lt;element name="IPtrans" maxOccurs="unbounded" minOccurs="0">
 *                                                                           &lt;complexType>
 *                                                                             &lt;complexContent>
 *                                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                                 &lt;sequence>
 *                                                                                   &lt;element name="internetIP">
 *                                                                                     &lt;complexType>
 *                                                                                       &lt;complexContent>
 *                                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                                           &lt;sequence>
 *                                                                                             &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                                                             &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                                                           &lt;/sequence>
 *                                                                                         &lt;/restriction>
 *                                                                                       &lt;/complexContent>
 *                                                                                     &lt;/complexType>
 *                                                                                   &lt;/element>
 *                                                                                   &lt;element name="netIP" minOccurs="0">
 *                                                                                     &lt;complexType>
 *                                                                                       &lt;complexContent>
 *                                                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                                           &lt;sequence>
 *                                                                                             &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                                                             &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                                                           &lt;/sequence>
 *                                                                                         &lt;/restriction>
 *                                                                                       &lt;/complexContent>
 *                                                                                     &lt;/complexType>
 *                                                                                   &lt;/element>
 *                                                                                 &lt;/sequence>
 *                                                                               &lt;/restriction>
 *                                                                             &lt;/complexContent>
 *                                                                           &lt;/complexType>
 *                                                                         &lt;/element>
 *                                                                       &lt;/sequence>
 *                                                                     &lt;/restriction>
 *                                                                   &lt;/complexContent>
 *                                                                 &lt;/complexType>
 *                                                               &lt;/element>
 *                                                             &lt;/sequence>
 *                                                           &lt;/restriction>
 *                                                         &lt;/complexContent>
 *                                                       &lt;/complexType>
 *                                                     &lt;/element>
 *                                                     &lt;element name="houseHoldInfo" minOccurs="0">
 *                                                       &lt;complexType>
 *                                                         &lt;complexContent>
 *                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                             &lt;sequence>
 *                                                               &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                               &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                               &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                               &lt;element name="IPseg" maxOccurs="unbounded" minOccurs="0">
 *                                                                 &lt;complexType>
 *                                                                   &lt;complexContent>
 *                                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                       &lt;sequence>
 *                                                                         &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                                         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                                       &lt;/sequence>
 *                                                                     &lt;/restriction>
 *                                                                   &lt;/complexContent>
 *                                                                 &lt;/complexType>
 *                                                               &lt;/element>
 *                                                               &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                             &lt;/sequence>
 *                                                           &lt;/restriction>
 *                                                         &lt;/complexContent>
 *                                                       &lt;/complexType>
 *                                                     &lt;/element>
 *                                                   &lt;/choice>
 *                                                   &lt;element name="registerTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="deleteInfo">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="idcID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="deleteData">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="house" maxOccurs="unbounded" minOccurs="0">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="gatewayID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                         &lt;element name="IPsegID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                               &lt;element name="user" maxOccurs="unbounded" minOccurs="0">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="userID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="service" minOccurs="0">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                   &lt;element name="domainID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                   &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                         &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="queryResult">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="commandID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                     &lt;element name="idcID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="idcName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="idcAdd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="idcZip" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="corp" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                     &lt;element name="idcOfficer">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="emergencyContact">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="houseInfo" maxOccurs="unbounded" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="houseName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="houseType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="houseProvince" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="houseCity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="houseCounty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="houseAdd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="houseZip" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="houseOfficer">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                               &lt;element name="gatewayInfo" maxOccurs="unbounded">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="gatewayIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                               &lt;element name="IPsegInfo" maxOccurs="unbounded">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                         &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                         &lt;element name="useTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="userInfo" maxOccurs="unbounded" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="nature" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                               &lt;element name="info">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="unitName" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="unitNature" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="officer">
 *                                           &lt;complexType>
 *                                             &lt;complexContent>
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                 &lt;sequence>
 *                                                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                   &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                   &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                 &lt;/sequence>
 *                                               &lt;/restriction>
 *                                             &lt;/complexContent>
 *                                           &lt;/complexType>
 *                                         &lt;/element>
 *                                         &lt;element name="add" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;element name="zipCode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                         &lt;choice>
 *                                           &lt;element name="serviceInfo" maxOccurs="unbounded" minOccurs="0">
 *                                             &lt;complexType>
 *                                               &lt;complexContent>
 *                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                   &lt;sequence>
 *                                                     &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                     &lt;element name="serviceContent" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded"/>
 *                                                     &lt;element name="regType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                     &lt;element name="regID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                     &lt;element name="setMode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                     &lt;element name="domain" maxOccurs="unbounded" minOccurs="0">
 *                                                       &lt;complexType>
 *                                                         &lt;complexContent>
 *                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                             &lt;sequence>
 *                                                               &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                               &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                             &lt;/sequence>
 *                                                           &lt;/restriction>
 *                                                         &lt;/complexContent>
 *                                                       &lt;/complexType>
 *                                                     &lt;/element>
 *                                                     &lt;element name="housesHoldInfo" maxOccurs="unbounded">
 *                                                       &lt;complexType>
 *                                                         &lt;complexContent>
 *                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                             &lt;sequence>
 *                                                               &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                               &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                               &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                               &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                               &lt;element name="IPtrans" maxOccurs="unbounded">
 *                                                                 &lt;complexType>
 *                                                                   &lt;complexContent>
 *                                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                       &lt;sequence>
 *                                                                         &lt;element name="internetIP">
 *                                                                           &lt;complexType>
 *                                                                             &lt;complexContent>
 *                                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                                 &lt;sequence>
 *                                                                                   &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                                                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                                                 &lt;/sequence>
 *                                                                               &lt;/restriction>
 *                                                                             &lt;/complexContent>
 *                                                                           &lt;/complexType>
 *                                                                         &lt;/element>
 *                                                                         &lt;element name="netIP" minOccurs="0">
 *                                                                           &lt;complexType>
 *                                                                             &lt;complexContent>
 *                                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                                 &lt;sequence>
 *                                                                                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                                                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                                                 &lt;/sequence>
 *                                                                               &lt;/restriction>
 *                                                                             &lt;/complexContent>
 *                                                                           &lt;/complexType>
 *                                                                         &lt;/element>
 *                                                                       &lt;/sequence>
 *                                                                     &lt;/restriction>
 *                                                                   &lt;/complexContent>
 *                                                                 &lt;/complexType>
 *                                                               &lt;/element>
 *                                                             &lt;/sequence>
 *                                                           &lt;/restriction>
 *                                                         &lt;/complexContent>
 *                                                       &lt;/complexType>
 *                                                     &lt;/element>
 *                                                   &lt;/sequence>
 *                                                 &lt;/restriction>
 *                                               &lt;/complexContent>
 *                                             &lt;/complexType>
 *                                           &lt;/element>
 *                                           &lt;element name="houseHoldInfo" maxOccurs="unbounded" minOccurs="0">
 *                                             &lt;complexType>
 *                                               &lt;complexContent>
 *                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                   &lt;sequence>
 *                                                     &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                     &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                     &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                     &lt;element name="IPseg" maxOccurs="unbounded">
 *                                                       &lt;complexType>
 *                                                         &lt;complexContent>
 *                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                             &lt;sequence>
 *                                                               &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                               &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                                             &lt;/sequence>
 *                                                           &lt;/restriction>
 *                                                         &lt;/complexContent>
 *                                                       &lt;/complexType>
 *                                                     &lt;/element>
 *                                                     &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                                                   &lt;/sequence>
 *                                                 &lt;/restriction>
 *                                               &lt;/complexContent>
 *                                             &lt;/complexType>
 *                                           &lt;/element>
 *                                         &lt;/choice>
 *                                         &lt;element name="registerTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *         &lt;element name="timeStamp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "newInfo",
    "updateInfo",
    "deleteInfo",
    "queryResult",
    "timeStamp"
})
@XmlRootElement(name = "uploadData")
public class UploadData {

    protected NewInfo newInfo;
    protected UpdateInfo updateInfo;
    protected DeleteInfo deleteInfo;
    protected QueryResult queryResult;
    @XmlElement(required = true)
    protected String timeStamp;

    /**
     * 获取newInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link NewInfo }
     *     
     */
    public NewInfo getNewInfo() {
        return newInfo;
    }

    /**
     * 设置newInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link NewInfo }
     *     
     */
    public void setNewInfo(NewInfo value) {
        this.newInfo = value;
    }

    /**
     * 获取updateInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link UpdateInfo }
     *     
     */
    public UpdateInfo getUpdateInfo() {
        return updateInfo;
    }

    /**
     * 设置updateInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateInfo }
     *     
     */
    public void setUpdateInfo(UpdateInfo value) {
        this.updateInfo = value;
    }

    /**
     * 获取deleteInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link DeleteInfo }
     *     
     */
    public DeleteInfo getDeleteInfo() {
        return deleteInfo;
    }

    /**
     * 设置deleteInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DeleteInfo }
     *     
     */
    public void setDeleteInfo(DeleteInfo value) {
        this.deleteInfo = value;
    }

    /**
     * 获取queryResult属性的值。
     * 
     * @return
     *     possible object is
     *     {@link QueryResult }
     *     
     */
    public QueryResult getQueryResult() {
        return queryResult;
    }

    /**
     * 设置queryResult属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link QueryResult }
     *     
     */
    public void setQueryResult(QueryResult value) {
        this.queryResult = value;
    }

    /**
     * 获取timeStamp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     * 设置timeStamp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeStamp(String value) {
        this.timeStamp = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="idcID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="deleteData">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="house" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="gatewayID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                             &lt;element name="IPsegID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="user" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="userID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="service" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                       &lt;element name="domainID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                       &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idcID",
        "deleteData"
    })
    public static class DeleteInfo {

        @XmlElement(required = true)
        protected String idcID;
        @XmlElement(required = true)
        protected DeleteData deleteData;

        /**
         * 获取idcID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdcID() {
            return idcID;
        }

        /**
         * 设置idcID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdcID(String value) {
            this.idcID = value;
        }

        /**
         * 获取deleteData属性的值。
         * 
         * @return
         *     possible object is
         *     {@link DeleteData }
         *     
         */
        public DeleteData getDeleteData() {
            return deleteData;
        }

        /**
         * 设置deleteData属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link DeleteData }
         *     
         */
        public void setDeleteData(DeleteData value) {
            this.deleteData = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="house" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="gatewayID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                   &lt;element name="IPsegID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="user" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="userID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="service" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                             &lt;element name="domainID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                             &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "house",
            "user"
        })
        public static class DeleteData {

            protected List<House> house;
            protected List<User> user;

            /**
             * Gets the value of the house property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the house property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getHouse().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link House }
             * 
             * 
             */
            public List<House> getHouse() {
                if (house == null) {
                    house = new ArrayList<House>();
                }
                return this.house;
            }

            /**
             * Gets the value of the user property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the user property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getUser().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link User }
             * 
             * 
             */
            public List<User> getUser() {
                if (user == null) {
                    user = new ArrayList<User>();
                }
                return this.user;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="gatewayID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *         &lt;element name="IPsegID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "houseID",
                "gatewayID",
                "iPsegID"
            })
            public static class House {

                @XmlElement(required = true)
                protected Object houseID;
                protected Object gatewayID;
                @XmlElement(name = "IPsegID")
                protected Object iPsegID;

                /**
                 * 获取houseID属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getHouseID() {
                    return houseID;
                }

                /**
                 * 设置houseID属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setHouseID(Object value) {
                    this.houseID = value;
                }

                /**
                 * 获取gatewayID属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getGatewayID() {
                    return gatewayID;
                }

                /**
                 * 设置gatewayID属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setGatewayID(Object value) {
                    this.gatewayID = value;
                }

                /**
                 * 获取iPsegID属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getIPsegID() {
                    return iPsegID;
                }

                /**
                 * 设置iPsegID属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setIPsegID(Object value) {
                    this.iPsegID = value;
                }

            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="userID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="service" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                   &lt;element name="domainID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                   &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "userID",
                "service",
                "hhID"
            })
            public static class User {

                @XmlElement(required = true)
                protected Object userID;
                protected Service service;
                protected Object hhID;

                /**
                 * 获取userID属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getUserID() {
                    return userID;
                }

                /**
                 * 设置userID属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setUserID(Object value) {
                    this.userID = value;
                }

                /**
                 * 获取service属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Service }
                 *     
                 */
                public Service getService() {
                    return service;
                }

                /**
                 * 设置service属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Service }
                 *     
                 */
                public void setService(Service value) {
                    this.service = value;
                }

                /**
                 * 获取hhID属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getHhID() {
                    return hhID;
                }

                /**
                 * 设置hhID属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setHhID(Object value) {
                    this.hhID = value;
                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="domainID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *         &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "serviceID",
                    "domainID",
                    "hhID"
                })
                public static class Service {

                    @XmlElement(required = true)
                    protected Object serviceID;
                    protected Object domainID;
                    protected Object hhID;

                    /**
                     * 获取serviceID属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getServiceID() {
                        return serviceID;
                    }

                    /**
                     * 设置serviceID属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setServiceID(Object value) {
                        this.serviceID = value;
                    }

                    /**
                     * 获取domainID属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getDomainID() {
                        return domainID;
                    }

                    /**
                     * 设置domainID属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setDomainID(Object value) {
                        this.domainID = value;
                    }

                    /**
                     * 获取hhID属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getHhID() {
                        return hhID;
                    }

                    /**
                     * 设置hhID属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setHhID(Object value) {
                        this.hhID = value;
                    }

                }

            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="idcID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="idcName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="idcAdd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="idcZip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="corp" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *         &lt;element name="idcOfficer" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="emergencyContact" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="houseInfo" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="houseName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="houseType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="houseProvince" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="houseCity" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="houseCounty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="houseAdd" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="houseZip" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="houseOfficer">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="gatewayInfo" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                             &lt;element name="gatewayIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="IPsegInfo" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                             &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                             &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                             &lt;element name="useTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="userInfo" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="nature" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="info">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="unitName" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="unitNature" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="officer">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                       &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                       &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                       &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                       &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                       &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="add" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="zipCode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;choice>
     *                               &lt;element name="serviceInfo" maxOccurs="unbounded" minOccurs="0">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;sequence>
     *                                         &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                         &lt;element name="serviceContent" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded"/>
     *                                         &lt;element name="regType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                         &lt;element name="regID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                         &lt;element name="setMode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                         &lt;element name="domain" maxOccurs="unbounded" minOccurs="0">
     *                                           &lt;complexType>
     *                                             &lt;complexContent>
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                 &lt;sequence>
     *                                                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                 &lt;/sequence>
     *                                               &lt;/restriction>
     *                                             &lt;/complexContent>
     *                                           &lt;/complexType>
     *                                         &lt;/element>
     *                                         &lt;element name="housesHoldInfo" maxOccurs="unbounded">
     *                                           &lt;complexType>
     *                                             &lt;complexContent>
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                 &lt;sequence>
     *                                                   &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                   &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                   &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                   &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                                   &lt;element name="IPtrans" maxOccurs="unbounded">
     *                                                     &lt;complexType>
     *                                                       &lt;complexContent>
     *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                           &lt;sequence>
     *                                                             &lt;element name="internetIP">
     *                                                               &lt;complexType>
     *                                                                 &lt;complexContent>
     *                                                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                     &lt;sequence>
     *                                                                       &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                                       &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                                     &lt;/sequence>
     *                                                                   &lt;/restriction>
     *                                                                 &lt;/complexContent>
     *                                                               &lt;/complexType>
     *                                                             &lt;/element>
     *                                                             &lt;element name="netIP" minOccurs="0">
     *                                                               &lt;complexType>
     *                                                                 &lt;complexContent>
     *                                                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                     &lt;sequence>
     *                                                                       &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                                       &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                                     &lt;/sequence>
     *                                                                   &lt;/restriction>
     *                                                                 &lt;/complexContent>
     *                                                               &lt;/complexType>
     *                                                             &lt;/element>
     *                                                           &lt;/sequence>
     *                                                         &lt;/restriction>
     *                                                       &lt;/complexContent>
     *                                                     &lt;/complexType>
     *                                                   &lt;/element>
     *                                                 &lt;/sequence>
     *                                               &lt;/restriction>
     *                                             &lt;/complexContent>
     *                                           &lt;/complexType>
     *                                         &lt;/element>
     *                                       &lt;/sequence>
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                               &lt;element name="houseHoldInfo" maxOccurs="unbounded" minOccurs="0">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;sequence>
     *                                         &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                         &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                         &lt;element name="IPseg" maxOccurs="unbounded">
     *                                           &lt;complexType>
     *                                             &lt;complexContent>
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                 &lt;sequence>
     *                                                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                 &lt;/sequence>
     *                                               &lt;/restriction>
     *                                             &lt;/complexContent>
     *                                           &lt;/complexType>
     *                                         &lt;/element>
     *                                         &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                       &lt;/sequence>
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                             &lt;/choice>
     *                             &lt;element name="registerTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idcID",
        "idcName",
        "idcAdd",
        "idcZip",
        "corp",
        "idcOfficer",
        "emergencyContact",
        "houseInfo",
        "userInfo"
    })
    public static class NewInfo {

        @XmlElement(required = true)
        protected String idcID;
        protected String idcName;
        protected String idcAdd;
        protected String idcZip;
        protected Object corp;
        protected IdcOfficer idcOfficer;
        protected EmergencyContact emergencyContact;
        protected List<HouseInfo> houseInfo;
        protected List<UserInfo> userInfo;

        /**
         * 获取idcID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdcID() {
            return idcID;
        }

        /**
         * 设置idcID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdcID(String value) {
            this.idcID = value;
        }

        /**
         * 获取idcName属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdcName() {
            return idcName;
        }

        /**
         * 设置idcName属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdcName(String value) {
            this.idcName = value;
        }

        /**
         * 获取idcAdd属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdcAdd() {
            return idcAdd;
        }

        /**
         * 设置idcAdd属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdcAdd(String value) {
            this.idcAdd = value;
        }

        /**
         * 获取idcZip属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdcZip() {
            return idcZip;
        }

        /**
         * 设置idcZip属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdcZip(String value) {
            this.idcZip = value;
        }

        /**
         * 获取corp属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getCorp() {
            return corp;
        }

        /**
         * 设置corp属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setCorp(Object value) {
            this.corp = value;
        }

        /**
         * 获取idcOfficer属性的值。
         * 
         * @return
         *     possible object is
         *     {@link IdcOfficer }
         *     
         */
        public IdcOfficer getIdcOfficer() {
            return idcOfficer;
        }

        /**
         * 设置idcOfficer属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link IdcOfficer }
         *     
         */
        public void setIdcOfficer(IdcOfficer value) {
            this.idcOfficer = value;
        }

        /**
         * 获取emergencyContact属性的值。
         * 
         * @return
         *     possible object is
         *     {@link EmergencyContact }
         *     
         */
        public EmergencyContact getEmergencyContact() {
            return emergencyContact;
        }

        /**
         * 设置emergencyContact属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link EmergencyContact }
         *     
         */
        public void setEmergencyContact(EmergencyContact value) {
            this.emergencyContact = value;
        }

        /**
         * Gets the value of the houseInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the houseInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getHouseInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link HouseInfo }
         * 
         * 
         */
        public List<HouseInfo> getHouseInfo() {
            if (houseInfo == null) {
                houseInfo = new ArrayList<HouseInfo>();
            }
            return this.houseInfo;
        }

        /**
         * Gets the value of the userInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the userInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getUserInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link UserInfo }
         * 
         * 
         */
        public List<UserInfo> getUserInfo() {
            if (userInfo == null) {
                userInfo = new ArrayList<UserInfo>();
            }
            return this.userInfo;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "name",
            "iDtype",
            "id",
            "tel",
            "mobile",
            "email"
        })
        public static class EmergencyContact {

            @XmlElement(required = true)
            protected Object name;
            @XmlElement(name = "IDtype", required = true)
            protected Object iDtype;
            @XmlElement(name = "ID", required = true)
            protected Object id;
            @XmlElement(required = true)
            protected Object tel;
            @XmlElement(required = true)
            protected Object mobile;
            @XmlElement(required = true)
            protected Object email;

            /**
             * 获取name属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getName() {
                return name;
            }

            /**
             * 设置name属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setName(Object value) {
                this.name = value;
            }

            /**
             * 获取iDtype属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getIDtype() {
                return iDtype;
            }

            /**
             * 设置iDtype属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setIDtype(Object value) {
                this.iDtype = value;
            }

            /**
             * 获取id属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getID() {
                return id;
            }

            /**
             * 设置id属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setID(Object value) {
                this.id = value;
            }

            /**
             * 获取tel属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getTel() {
                return tel;
            }

            /**
             * 设置tel属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setTel(Object value) {
                this.tel = value;
            }

            /**
             * 获取mobile属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMobile() {
                return mobile;
            }

            /**
             * 设置mobile属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMobile(Object value) {
                this.mobile = value;
            }

            /**
             * 获取email属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getEmail() {
                return email;
            }

            /**
             * 设置email属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setEmail(Object value) {
                this.email = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="houseName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="houseType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="houseProvince" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="houseCity" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="houseCounty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="houseAdd" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="houseZip" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="houseOfficer">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="gatewayInfo" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                   &lt;element name="gatewayIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="IPsegInfo" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                   &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                   &lt;element name="useTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "houseID",
            "houseName",
            "houseType",
            "houseProvince",
            "houseCity",
            "houseCounty",
            "houseAdd",
            "houseZip",
            "houseOfficer",
            "gatewayInfo",
            "iPsegInfo"
        })
        public static class HouseInfo {

            @XmlElement(required = true)
            protected String houseID;
            protected String houseName;
            @XmlElement(required = true)
            protected String houseType;
            @XmlElement(required = true)
            protected String houseProvince;
            @XmlElement(required = true)
            protected String houseCity;
            protected String houseCounty;
            @XmlElement(required = true)
            protected String houseAdd;
            @XmlElement(required = true)
            protected String houseZip;
            @XmlElement(required = true)
            protected HouseOfficer houseOfficer;
            @XmlElement(required = true)
            protected List<GatewayInfo> gatewayInfo;
            @XmlElement(name = "IPsegInfo", required = true)
            protected List<IPsegInfo> iPsegInfo;

            /**
             * 获取houseID属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseID() {
                return houseID;
            }

            /**
             * 设置houseID属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseID(String value) {
                this.houseID = value;
            }

            /**
             * 获取houseName属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseName() {
                return houseName;
            }

            /**
             * 设置houseName属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseName(String value) {
                this.houseName = value;
            }

            /**
             * 获取houseType属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseType() {
                return houseType;
            }

            /**
             * 设置houseType属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseType(String value) {
                this.houseType = value;
            }

            /**
             * 获取houseProvince属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseProvince() {
                return houseProvince;
            }

            /**
             * 设置houseProvince属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseProvince(String value) {
                this.houseProvince = value;
            }

            /**
             * 获取houseCity属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseCity() {
                return houseCity;
            }

            /**
             * 设置houseCity属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseCity(String value) {
                this.houseCity = value;
            }

            /**
             * 获取houseCounty属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseCounty() {
                return houseCounty;
            }

            /**
             * 设置houseCounty属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseCounty(String value) {
                this.houseCounty = value;
            }

            /**
             * 获取houseAdd属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseAdd() {
                return houseAdd;
            }

            /**
             * 设置houseAdd属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseAdd(String value) {
                this.houseAdd = value;
            }

            /**
             * 获取houseZip属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseZip() {
                return houseZip;
            }

            /**
             * 设置houseZip属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseZip(String value) {
                this.houseZip = value;
            }

            /**
             * 获取houseOfficer属性的值。
             * 
             * @return
             *     possible object is
             *     {@link HouseOfficer }
             *     
             */
            public HouseOfficer getHouseOfficer() {
                return houseOfficer;
            }

            /**
             * 设置houseOfficer属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link HouseOfficer }
             *     
             */
            public void setHouseOfficer(HouseOfficer value) {
                this.houseOfficer = value;
            }

            /**
             * Gets the value of the gatewayInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the gatewayInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getGatewayInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link GatewayInfo }
             * 
             * 
             */
            public List<GatewayInfo> getGatewayInfo() {
                if (gatewayInfo == null) {
                    gatewayInfo = new ArrayList<GatewayInfo>();
                }
                return this.gatewayInfo;
            }

            /**
             * Gets the value of the iPsegInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the iPsegInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getIPsegInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link IPsegInfo }
             * 
             * 
             */
            public List<IPsegInfo> getIPsegInfo() {
                if (iPsegInfo == null) {
                    iPsegInfo = new ArrayList<IPsegInfo>();
                }
                return this.iPsegInfo;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *         &lt;element name="gatewayIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "id",
                "bandWidth",
                "gatewayIP"
            })
            public static class GatewayInfo {

                @XmlElement(name = "ID", required = true)
                protected Object id;
                protected Object bandWidth;
                @XmlElement(required = true)
                protected Object gatewayIP;

                /**
                 * 获取id属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getID() {
                    return id;
                }

                /**
                 * 设置id属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setID(Object value) {
                    this.id = value;
                }

                /**
                 * 获取bandWidth属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getBandWidth() {
                    return bandWidth;
                }

                /**
                 * 设置bandWidth属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setBandWidth(Object value) {
                    this.bandWidth = value;
                }

                /**
                 * 获取gatewayIP属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getGatewayIP() {
                    return gatewayIP;
                }

                /**
                 * 设置gatewayIP属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setGatewayIP(Object value) {
                    this.gatewayIP = value;
                }

            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "name",
                "iDtype",
                "id",
                "tel",
                "mobile",
                "email"
            })
            public static class HouseOfficer {

                @XmlElement(required = true)
                protected Object name;
                @XmlElement(name = "IDtype", required = true)
                protected Object iDtype;
                @XmlElement(name = "ID", required = true)
                protected Object id;
                @XmlElement(required = true)
                protected Object tel;
                @XmlElement(required = true)
                protected Object mobile;
                @XmlElement(required = true)
                protected Object email;

                /**
                 * 获取name属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getName() {
                    return name;
                }

                /**
                 * 设置name属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setName(Object value) {
                    this.name = value;
                }

                /**
                 * 获取iDtype属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getIDtype() {
                    return iDtype;
                }

                /**
                 * 设置iDtype属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setIDtype(Object value) {
                    this.iDtype = value;
                }

                /**
                 * 获取id属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getID() {
                    return id;
                }

                /**
                 * 设置id属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setID(Object value) {
                    this.id = value;
                }

                /**
                 * 获取tel属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getTel() {
                    return tel;
                }

                /**
                 * 设置tel属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setTel(Object value) {
                    this.tel = value;
                }

                /**
                 * 获取mobile属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getMobile() {
                    return mobile;
                }

                /**
                 * 设置mobile属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setMobile(Object value) {
                    this.mobile = value;
                }

                /**
                 * 获取email属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getEmail() {
                    return email;
                }

                /**
                 * 设置email属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setEmail(Object value) {
                    this.email = value;
                }

            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *         &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *         &lt;element name="useTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "id",
                "startIP",
                "endIP",
                "type",
                "user",
                "iDtype",
                "iDnumber",
                "useTime"
            })
            public static class IPsegInfo {

                @XmlElement(name = "ID", required = true)
                protected Object id;
                @XmlElement(required = true)
                protected Object startIP;
                @XmlElement(required = true)
                protected Object endIP;
                @XmlElement(required = true)
                protected Object type;
                protected Object user;
                @XmlElement(name = "IDtype")
                protected Object iDtype;
                @XmlElement(name = "IDnumber")
                protected Object iDnumber;
                @XmlElement(required = true)
                protected Object useTime;

                /**
                 * 获取id属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getID() {
                    return id;
                }

                /**
                 * 设置id属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setID(Object value) {
                    this.id = value;
                }

                /**
                 * 获取startIP属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getStartIP() {
                    return startIP;
                }

                /**
                 * 设置startIP属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setStartIP(Object value) {
                    this.startIP = value;
                }

                /**
                 * 获取endIP属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getEndIP() {
                    return endIP;
                }

                /**
                 * 设置endIP属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setEndIP(Object value) {
                    this.endIP = value;
                }

                /**
                 * 获取type属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getType() {
                    return type;
                }

                /**
                 * 设置type属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setType(Object value) {
                    this.type = value;
                }

                /**
                 * 获取user属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getUser() {
                    return user;
                }

                /**
                 * 设置user属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setUser(Object value) {
                    this.user = value;
                }

                /**
                 * 获取iDtype属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getIDtype() {
                    return iDtype;
                }

                /**
                 * 设置iDtype属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setIDtype(Object value) {
                    this.iDtype = value;
                }

                /**
                 * 获取iDnumber属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getIDnumber() {
                    return iDnumber;
                }

                /**
                 * 设置iDnumber属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setIDnumber(Object value) {
                    this.iDnumber = value;
                }

                /**
                 * 获取useTime属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getUseTime() {
                    return useTime;
                }

                /**
                 * 设置useTime属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setUseTime(Object value) {
                    this.useTime = value;
                }

            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "name",
            "iDtype",
            "id",
            "tel",
            "mobile",
            "email"
        })
        public static class IdcOfficer {

            @XmlElement(required = true)
            protected Object name;
            @XmlElement(name = "IDtype", required = true)
            protected Object iDtype;
            @XmlElement(name = "ID", required = true)
            protected Object id;
            @XmlElement(required = true)
            protected Object tel;
            @XmlElement(required = true)
            protected Object mobile;
            @XmlElement(required = true)
            protected Object email;

            /**
             * 获取name属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getName() {
                return name;
            }

            /**
             * 设置name属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setName(Object value) {
                this.name = value;
            }

            /**
             * 获取iDtype属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getIDtype() {
                return iDtype;
            }

            /**
             * 设置iDtype属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setIDtype(Object value) {
                this.iDtype = value;
            }

            /**
             * 获取id属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getID() {
                return id;
            }

            /**
             * 设置id属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setID(Object value) {
                this.id = value;
            }

            /**
             * 获取tel属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getTel() {
                return tel;
            }

            /**
             * 设置tel属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setTel(Object value) {
                this.tel = value;
            }

            /**
             * 获取mobile属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMobile() {
                return mobile;
            }

            /**
             * 设置mobile属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMobile(Object value) {
                this.mobile = value;
            }

            /**
             * 获取email属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getEmail() {
                return email;
            }

            /**
             * 设置email属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setEmail(Object value) {
                this.email = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="nature" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="info">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="unitName" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="unitNature" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="officer">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                             &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                             &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                             &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                             &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="add" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="zipCode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;choice>
         *                     &lt;element name="serviceInfo" maxOccurs="unbounded" minOccurs="0">
         *                       &lt;complexType>
         *                         &lt;complexContent>
         *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                             &lt;sequence>
         *                               &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                               &lt;element name="serviceContent" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded"/>
         *                               &lt;element name="regType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                               &lt;element name="regID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                               &lt;element name="setMode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                               &lt;element name="domain" maxOccurs="unbounded" minOccurs="0">
         *                                 &lt;complexType>
         *                                   &lt;complexContent>
         *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                       &lt;sequence>
         *                                         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                       &lt;/sequence>
         *                                     &lt;/restriction>
         *                                   &lt;/complexContent>
         *                                 &lt;/complexType>
         *                               &lt;/element>
         *                               &lt;element name="housesHoldInfo" maxOccurs="unbounded">
         *                                 &lt;complexType>
         *                                   &lt;complexContent>
         *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                       &lt;sequence>
         *                                         &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                         &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                         &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                                         &lt;element name="IPtrans" maxOccurs="unbounded">
         *                                           &lt;complexType>
         *                                             &lt;complexContent>
         *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                 &lt;sequence>
         *                                                   &lt;element name="internetIP">
         *                                                     &lt;complexType>
         *                                                       &lt;complexContent>
         *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                           &lt;sequence>
         *                                                             &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                                             &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                                           &lt;/sequence>
         *                                                         &lt;/restriction>
         *                                                       &lt;/complexContent>
         *                                                     &lt;/complexType>
         *                                                   &lt;/element>
         *                                                   &lt;element name="netIP" minOccurs="0">
         *                                                     &lt;complexType>
         *                                                       &lt;complexContent>
         *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                           &lt;sequence>
         *                                                             &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                                             &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                                           &lt;/sequence>
         *                                                         &lt;/restriction>
         *                                                       &lt;/complexContent>
         *                                                     &lt;/complexType>
         *                                                   &lt;/element>
         *                                                 &lt;/sequence>
         *                                               &lt;/restriction>
         *                                             &lt;/complexContent>
         *                                           &lt;/complexType>
         *                                         &lt;/element>
         *                                       &lt;/sequence>
         *                                     &lt;/restriction>
         *                                   &lt;/complexContent>
         *                                 &lt;/complexType>
         *                               &lt;/element>
         *                             &lt;/sequence>
         *                           &lt;/restriction>
         *                         &lt;/complexContent>
         *                       &lt;/complexType>
         *                     &lt;/element>
         *                     &lt;element name="houseHoldInfo" maxOccurs="unbounded" minOccurs="0">
         *                       &lt;complexType>
         *                         &lt;complexContent>
         *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                             &lt;sequence>
         *                               &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                               &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                               &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                               &lt;element name="IPseg" maxOccurs="unbounded">
         *                                 &lt;complexType>
         *                                   &lt;complexContent>
         *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                       &lt;sequence>
         *                                         &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                       &lt;/sequence>
         *                                     &lt;/restriction>
         *                                   &lt;/complexContent>
         *                                 &lt;/complexType>
         *                               &lt;/element>
         *                               &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                             &lt;/sequence>
         *                           &lt;/restriction>
         *                         &lt;/complexContent>
         *                       &lt;/complexType>
         *                     &lt;/element>
         *                   &lt;/choice>
         *                   &lt;element name="registerTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "id",
            "nature",
            "info"
        })
        public static class UserInfo {

            @XmlElement(name = "ID", required = true)
            protected String id;
            @XmlElement(required = true)
            protected Object nature;
            @XmlElement(required = true)
            protected Info info;

            /**
             * 获取id属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getID() {
                return id;
            }

            /**
             * 设置id属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setID(String value) {
                this.id = value;
            }

            /**
             * 获取nature属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getNature() {
                return nature;
            }

            /**
             * 设置nature属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setNature(Object value) {
                this.nature = value;
            }

            /**
             * 获取info属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Info }
             *     
             */
            public Info getInfo() {
                return info;
            }

            /**
             * 设置info属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Info }
             *     
             */
            public void setInfo(Info value) {
                this.info = value;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="unitName" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="unitNature" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="officer">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                   &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                   &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="add" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="zipCode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;choice>
             *           &lt;element name="serviceInfo" maxOccurs="unbounded" minOccurs="0">
             *             &lt;complexType>
             *               &lt;complexContent>
             *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                   &lt;sequence>
             *                     &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                     &lt;element name="serviceContent" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded"/>
             *                     &lt;element name="regType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                     &lt;element name="regID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                     &lt;element name="setMode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                     &lt;element name="domain" maxOccurs="unbounded" minOccurs="0">
             *                       &lt;complexType>
             *                         &lt;complexContent>
             *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                             &lt;sequence>
             *                               &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                               &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                             &lt;/sequence>
             *                           &lt;/restriction>
             *                         &lt;/complexContent>
             *                       &lt;/complexType>
             *                     &lt;/element>
             *                     &lt;element name="housesHoldInfo" maxOccurs="unbounded">
             *                       &lt;complexType>
             *                         &lt;complexContent>
             *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                             &lt;sequence>
             *                               &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                               &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                               &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                               &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                               &lt;element name="IPtrans" maxOccurs="unbounded">
             *                                 &lt;complexType>
             *                                   &lt;complexContent>
             *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                       &lt;sequence>
             *                                         &lt;element name="internetIP">
             *                                           &lt;complexType>
             *                                             &lt;complexContent>
             *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                                 &lt;sequence>
             *                                                   &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                                                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                                                 &lt;/sequence>
             *                                               &lt;/restriction>
             *                                             &lt;/complexContent>
             *                                           &lt;/complexType>
             *                                         &lt;/element>
             *                                         &lt;element name="netIP" minOccurs="0">
             *                                           &lt;complexType>
             *                                             &lt;complexContent>
             *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                                 &lt;sequence>
             *                                                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                                                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                                                 &lt;/sequence>
             *                                               &lt;/restriction>
             *                                             &lt;/complexContent>
             *                                           &lt;/complexType>
             *                                         &lt;/element>
             *                                       &lt;/sequence>
             *                                     &lt;/restriction>
             *                                   &lt;/complexContent>
             *                                 &lt;/complexType>
             *                               &lt;/element>
             *                             &lt;/sequence>
             *                           &lt;/restriction>
             *                         &lt;/complexContent>
             *                       &lt;/complexType>
             *                     &lt;/element>
             *                   &lt;/sequence>
             *                 &lt;/restriction>
             *               &lt;/complexContent>
             *             &lt;/complexType>
             *           &lt;/element>
             *           &lt;element name="houseHoldInfo" maxOccurs="unbounded" minOccurs="0">
             *             &lt;complexType>
             *               &lt;complexContent>
             *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                   &lt;sequence>
             *                     &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                     &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                     &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                     &lt;element name="IPseg" maxOccurs="unbounded">
             *                       &lt;complexType>
             *                         &lt;complexContent>
             *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                             &lt;sequence>
             *                               &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                               &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                             &lt;/sequence>
             *                           &lt;/restriction>
             *                         &lt;/complexContent>
             *                       &lt;/complexType>
             *                     &lt;/element>
             *                     &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                   &lt;/sequence>
             *                 &lt;/restriction>
             *               &lt;/complexContent>
             *             &lt;/complexType>
             *           &lt;/element>
             *         &lt;/choice>
             *         &lt;element name="registerTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "unitName",
                "unitNature",
                "iDtype",
                "iDnumber",
                "officer",
                "add",
                "zipCode",
                "serviceInfo",
                "houseHoldInfo",
                "registerTime"
            })
            public static class Info {

                @XmlElement(required = true)
                protected Object unitName;
                @XmlElement(required = true)
                protected Object unitNature;
                @XmlElement(name = "IDtype", required = true)
                protected Object iDtype;
                @XmlElement(name = "IDnumber", required = true)
                protected Object iDnumber;
                @XmlElement(required = true)
                protected Officer officer;
                @XmlElement(required = true)
                protected Object add;
                @XmlElement(required = true)
                protected Object zipCode;
                protected List<ServiceInfo> serviceInfo;
                protected List<HouseHoldInfo> houseHoldInfo;
                @XmlElement(required = true)
                protected Object registerTime;

                /**
                 * 获取unitName属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getUnitName() {
                    return unitName;
                }

                /**
                 * 设置unitName属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setUnitName(Object value) {
                    this.unitName = value;
                }

                /**
                 * 获取unitNature属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getUnitNature() {
                    return unitNature;
                }

                /**
                 * 设置unitNature属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setUnitNature(Object value) {
                    this.unitNature = value;
                }

                /**
                 * 获取iDtype属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getIDtype() {
                    return iDtype;
                }

                /**
                 * 设置iDtype属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setIDtype(Object value) {
                    this.iDtype = value;
                }

                /**
                 * 获取iDnumber属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getIDnumber() {
                    return iDnumber;
                }

                /**
                 * 设置iDnumber属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setIDnumber(Object value) {
                    this.iDnumber = value;
                }

                /**
                 * 获取officer属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Officer }
                 *     
                 */
                public Officer getOfficer() {
                    return officer;
                }

                /**
                 * 设置officer属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Officer }
                 *     
                 */
                public void setOfficer(Officer value) {
                    this.officer = value;
                }

                /**
                 * 获取add属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getAdd() {
                    return add;
                }

                /**
                 * 设置add属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setAdd(Object value) {
                    this.add = value;
                }

                /**
                 * 获取zipCode属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getZipCode() {
                    return zipCode;
                }

                /**
                 * 设置zipCode属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setZipCode(Object value) {
                    this.zipCode = value;
                }

                /**
                 * Gets the value of the serviceInfo property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the serviceInfo property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getServiceInfo().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link ServiceInfo }
                 * 
                 * 
                 */
                public List<ServiceInfo> getServiceInfo() {
                    if (serviceInfo == null) {
                        serviceInfo = new ArrayList<ServiceInfo>();
                    }
                    return this.serviceInfo;
                }

                /**
                 * Gets the value of the houseHoldInfo property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the houseHoldInfo property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getHouseHoldInfo().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link HouseHoldInfo }
                 * 
                 * 
                 */
                public List<HouseHoldInfo> getHouseHoldInfo() {
                    if (houseHoldInfo == null) {
                        houseHoldInfo = new ArrayList<HouseHoldInfo>();
                    }
                    return this.houseHoldInfo;
                }

                /**
                 * 获取registerTime属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getRegisterTime() {
                    return registerTime;
                }

                /**
                 * 设置registerTime属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setRegisterTime(Object value) {
                    this.registerTime = value;
                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="IPseg" maxOccurs="unbounded">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "hhID",
                    "houseID",
                    "distributeTime",
                    "iPseg",
                    "bandWidth"
                })
                public static class HouseHoldInfo {

                    @XmlElement(required = true)
                    protected Object hhID;
                    @XmlElement(required = true)
                    protected Object houseID;
                    @XmlElement(required = true)
                    protected Object distributeTime;
                    @XmlElement(name = "IPseg", required = true)
                    protected List<IPseg> iPseg;
                    protected Object bandWidth;

                    /**
                     * 获取hhID属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getHhID() {
                        return hhID;
                    }

                    /**
                     * 设置hhID属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setHhID(Object value) {
                        this.hhID = value;
                    }

                    /**
                     * 获取houseID属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getHouseID() {
                        return houseID;
                    }

                    /**
                     * 设置houseID属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setHouseID(Object value) {
                        this.houseID = value;
                    }

                    /**
                     * 获取distributeTime属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getDistributeTime() {
                        return distributeTime;
                    }

                    /**
                     * 设置distributeTime属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setDistributeTime(Object value) {
                        this.distributeTime = value;
                    }

                    /**
                     * Gets the value of the iPseg property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the iPseg property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getIPseg().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link IPseg }
                     * 
                     * 
                     */
                    public List<IPseg> getIPseg() {
                        if (iPseg == null) {
                            iPseg = new ArrayList<IPseg>();
                        }
                        return this.iPseg;
                    }

                    /**
                     * 获取bandWidth属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getBandWidth() {
                        return bandWidth;
                    }

                    /**
                     * 设置bandWidth属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setBandWidth(Object value) {
                        this.bandWidth = value;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "startIP",
                        "endIP"
                    })
                    public static class IPseg {

                        @XmlElement(required = true)
                        protected Object startIP;
                        @XmlElement(required = true)
                        protected Object endIP;

                        /**
                         * 获取startIP属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getStartIP() {
                            return startIP;
                        }

                        /**
                         * 设置startIP属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setStartIP(Object value) {
                            this.startIP = value;
                        }

                        /**
                         * 获取endIP属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getEndIP() {
                            return endIP;
                        }

                        /**
                         * 设置endIP属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setEndIP(Object value) {
                            this.endIP = value;
                        }

                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "name",
                    "iDtype",
                    "id",
                    "tel",
                    "mobile",
                    "email"
                })
                public static class Officer {

                    @XmlElement(required = true)
                    protected Object name;
                    @XmlElement(name = "IDtype", required = true)
                    protected Object iDtype;
                    @XmlElement(name = "ID", required = true)
                    protected Object id;
                    @XmlElement(required = true)
                    protected Object tel;
                    @XmlElement(required = true)
                    protected Object mobile;
                    @XmlElement(required = true)
                    protected Object email;

                    /**
                     * 获取name属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getName() {
                        return name;
                    }

                    /**
                     * 设置name属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setName(Object value) {
                        this.name = value;
                    }

                    /**
                     * 获取iDtype属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getIDtype() {
                        return iDtype;
                    }

                    /**
                     * 设置iDtype属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setIDtype(Object value) {
                        this.iDtype = value;
                    }

                    /**
                     * 获取id属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getID() {
                        return id;
                    }

                    /**
                     * 设置id属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setID(Object value) {
                        this.id = value;
                    }

                    /**
                     * 获取tel属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getTel() {
                        return tel;
                    }

                    /**
                     * 设置tel属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setTel(Object value) {
                        this.tel = value;
                    }

                    /**
                     * 获取mobile属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getMobile() {
                        return mobile;
                    }

                    /**
                     * 设置mobile属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setMobile(Object value) {
                        this.mobile = value;
                    }

                    /**
                     * 获取email属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getEmail() {
                        return email;
                    }

                    /**
                     * 设置email属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setEmail(Object value) {
                        this.email = value;
                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="serviceContent" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded"/>
                 *         &lt;element name="regType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *         &lt;element name="regID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *         &lt;element name="setMode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="domain" maxOccurs="unbounded" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="housesHoldInfo" maxOccurs="unbounded">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                   &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                   &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                   &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *                   &lt;element name="IPtrans" maxOccurs="unbounded">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;sequence>
                 *                             &lt;element name="internetIP">
                 *                               &lt;complexType>
                 *                                 &lt;complexContent>
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                     &lt;sequence>
                 *                                       &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                                       &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                                     &lt;/sequence>
                 *                                   &lt;/restriction>
                 *                                 &lt;/complexContent>
                 *                               &lt;/complexType>
                 *                             &lt;/element>
                 *                             &lt;element name="netIP" minOccurs="0">
                 *                               &lt;complexType>
                 *                                 &lt;complexContent>
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                     &lt;sequence>
                 *                                       &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                                       &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                                     &lt;/sequence>
                 *                                   &lt;/restriction>
                 *                                 &lt;/complexContent>
                 *                               &lt;/complexType>
                 *                             &lt;/element>
                 *                           &lt;/sequence>
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "serviceID",
                    "serviceContent",
                    "regType",
                    "regID",
                    "setMode",
                    "domain",
                    "housesHoldInfo"
                })
                public static class ServiceInfo {

                    @XmlElement(required = true)
                    protected Object serviceID;
                    @XmlElement(required = true)
                    protected List<Object> serviceContent;
                    protected Object regType;
                    protected Object regID;
                    @XmlElement(required = true)
                    protected Object setMode;
                    protected List<Domain> domain;
                    @XmlElement(required = true)
                    protected List<HousesHoldInfo> housesHoldInfo;

                    /**
                     * 获取serviceID属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getServiceID() {
                        return serviceID;
                    }

                    /**
                     * 设置serviceID属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setServiceID(Object value) {
                        this.serviceID = value;
                    }

                    /**
                     * Gets the value of the serviceContent property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the serviceContent property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getServiceContent().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link Object }
                     * 
                     * 
                     */
                    public List<Object> getServiceContent() {
                        if (serviceContent == null) {
                            serviceContent = new ArrayList<Object>();
                        }
                        return this.serviceContent;
                    }

                    /**
                     * 获取regType属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getRegType() {
                        return regType;
                    }

                    /**
                     * 设置regType属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setRegType(Object value) {
                        this.regType = value;
                    }

                    /**
                     * 获取regID属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getRegID() {
                        return regID;
                    }

                    /**
                     * 设置regID属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setRegID(Object value) {
                        this.regID = value;
                    }

                    /**
                     * 获取setMode属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getSetMode() {
                        return setMode;
                    }

                    /**
                     * 设置setMode属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setSetMode(Object value) {
                        this.setMode = value;
                    }

                    /**
                     * Gets the value of the domain property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the domain property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getDomain().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link Domain }
                     * 
                     * 
                     */
                    public List<Domain> getDomain() {
                        if (domain == null) {
                            domain = new ArrayList<Domain>();
                        }
                        return this.domain;
                    }

                    /**
                     * Gets the value of the housesHoldInfo property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the housesHoldInfo property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getHousesHoldInfo().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link HousesHoldInfo }
                     * 
                     * 
                     */
                    public List<HousesHoldInfo> getHousesHoldInfo() {
                        if (housesHoldInfo == null) {
                            housesHoldInfo = new ArrayList<HousesHoldInfo>();
                        }
                        return this.housesHoldInfo;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "id",
                        "name"
                    })
                    public static class Domain {

                        @XmlElement(name = "ID", required = true)
                        protected Object id;
                        @XmlElement(required = true)
                        protected Object name;

                        /**
                         * 获取id属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getID() {
                            return id;
                        }

                        /**
                         * 设置id属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setID(Object value) {
                            this.id = value;
                        }

                        /**
                         * 获取name属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getName() {
                            return name;
                        }

                        /**
                         * 设置name属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setName(Object value) {
                            this.name = value;
                        }

                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *         &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *         &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                     *         &lt;element name="IPtrans" maxOccurs="unbounded">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="internetIP">
                     *                     &lt;complexType>
                     *                       &lt;complexContent>
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                           &lt;sequence>
                     *                             &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *                             &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *                           &lt;/sequence>
                     *                         &lt;/restriction>
                     *                       &lt;/complexContent>
                     *                     &lt;/complexType>
                     *                   &lt;/element>
                     *                   &lt;element name="netIP" minOccurs="0">
                     *                     &lt;complexType>
                     *                       &lt;complexContent>
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                           &lt;sequence>
                     *                             &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *                             &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *                           &lt;/sequence>
                     *                         &lt;/restriction>
                     *                       &lt;/complexContent>
                     *                     &lt;/complexType>
                     *                   &lt;/element>
                     *                 &lt;/sequence>
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "hhID",
                        "houseID",
                        "distributeTime",
                        "bandWidth",
                        "iPtrans"
                    })
                    public static class HousesHoldInfo {

                        @XmlElement(required = true)
                        protected Object hhID;
                        @XmlElement(required = true)
                        protected Object houseID;
                        @XmlElement(required = true)
                        protected Object distributeTime;
                        protected Object bandWidth;
                        @XmlElement(name = "IPtrans", required = true)
                        protected List<IPtrans> iPtrans;

                        /**
                         * 获取hhID属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getHhID() {
                            return hhID;
                        }

                        /**
                         * 设置hhID属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setHhID(Object value) {
                            this.hhID = value;
                        }

                        /**
                         * 获取houseID属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getHouseID() {
                            return houseID;
                        }

                        /**
                         * 设置houseID属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setHouseID(Object value) {
                            this.houseID = value;
                        }

                        /**
                         * 获取distributeTime属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getDistributeTime() {
                            return distributeTime;
                        }

                        /**
                         * 设置distributeTime属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setDistributeTime(Object value) {
                            this.distributeTime = value;
                        }

                        /**
                         * 获取bandWidth属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getBandWidth() {
                            return bandWidth;
                        }

                        /**
                         * 设置bandWidth属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setBandWidth(Object value) {
                            this.bandWidth = value;
                        }

                        /**
                         * Gets the value of the iPtrans property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the iPtrans property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getIPtrans().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link IPtrans }
                         * 
                         * 
                         */
                        public List<IPtrans> getIPtrans() {
                            if (iPtrans == null) {
                                iPtrans = new ArrayList<IPtrans>();
                            }
                            return this.iPtrans;
                        }


                        /**
                         * <p>anonymous complex type的 Java 类。
                         * 
                         * <p>以下模式片段指定包含在此类中的预期内容。
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="internetIP">
                         *           &lt;complexType>
                         *             &lt;complexContent>
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                 &lt;sequence>
                         *                   &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                         *                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                         *                 &lt;/sequence>
                         *               &lt;/restriction>
                         *             &lt;/complexContent>
                         *           &lt;/complexType>
                         *         &lt;/element>
                         *         &lt;element name="netIP" minOccurs="0">
                         *           &lt;complexType>
                         *             &lt;complexContent>
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                 &lt;sequence>
                         *                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                         *                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                         *                 &lt;/sequence>
                         *               &lt;/restriction>
                         *             &lt;/complexContent>
                         *           &lt;/complexType>
                         *         &lt;/element>
                         *       &lt;/sequence>
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "internetIP",
                            "netIP"
                        })
                        public static class IPtrans {

                            @XmlElement(required = true)
                            protected InternetIP internetIP;
                            protected NetIP netIP;

                            /**
                             * 获取internetIP属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link InternetIP }
                             *     
                             */
                            public InternetIP getInternetIP() {
                                return internetIP;
                            }

                            /**
                             * 设置internetIP属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link InternetIP }
                             *     
                             */
                            public void setInternetIP(InternetIP value) {
                                this.internetIP = value;
                            }

                            /**
                             * 获取netIP属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link NetIP }
                             *     
                             */
                            public NetIP getNetIP() {
                                return netIP;
                            }

                            /**
                             * 设置netIP属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link NetIP }
                             *     
                             */
                            public void setNetIP(NetIP value) {
                                this.netIP = value;
                            }


                            /**
                             * <p>anonymous complex type的 Java 类。
                             * 
                             * <p>以下模式片段指定包含在此类中的预期内容。
                             * 
                             * <pre>
                             * &lt;complexType>
                             *   &lt;complexContent>
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *       &lt;sequence>
                             *         &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                             *         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                             *       &lt;/sequence>
                             *     &lt;/restriction>
                             *   &lt;/complexContent>
                             * &lt;/complexType>
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "stratIP",
                                "endIP"
                            })
                            public static class InternetIP {

                                @XmlElement(required = true)
                                protected Object stratIP;
                                @XmlElement(required = true)
                                protected Object endIP;

                                /**
                                 * 获取stratIP属性的值。
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link Object }
                                 *     
                                 */
                                public Object getStratIP() {
                                    return stratIP;
                                }

                                /**
                                 * 设置stratIP属性的值。
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link Object }
                                 *     
                                 */
                                public void setStratIP(Object value) {
                                    this.stratIP = value;
                                }

                                /**
                                 * 获取endIP属性的值。
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link Object }
                                 *     
                                 */
                                public Object getEndIP() {
                                    return endIP;
                                }

                                /**
                                 * 设置endIP属性的值。
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link Object }
                                 *     
                                 */
                                public void setEndIP(Object value) {
                                    this.endIP = value;
                                }

                            }


                            /**
                             * <p>anonymous complex type的 Java 类。
                             * 
                             * <p>以下模式片段指定包含在此类中的预期内容。
                             * 
                             * <pre>
                             * &lt;complexType>
                             *   &lt;complexContent>
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *       &lt;sequence>
                             *         &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                             *         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                             *       &lt;/sequence>
                             *     &lt;/restriction>
                             *   &lt;/complexContent>
                             * &lt;/complexType>
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "startIP",
                                "endIP"
                            })
                            public static class NetIP {

                                @XmlElement(required = true)
                                protected Object startIP;
                                @XmlElement(required = true)
                                protected Object endIP;

                                /**
                                 * 获取startIP属性的值。
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link Object }
                                 *     
                                 */
                                public Object getStartIP() {
                                    return startIP;
                                }

                                /**
                                 * 设置startIP属性的值。
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link Object }
                                 *     
                                 */
                                public void setStartIP(Object value) {
                                    this.startIP = value;
                                }

                                /**
                                 * 获取endIP属性的值。
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link Object }
                                 *     
                                 */
                                public Object getEndIP() {
                                    return endIP;
                                }

                                /**
                                 * 设置endIP属性的值。
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link Object }
                                 *     
                                 */
                                public void setEndIP(Object value) {
                                    this.endIP = value;
                                }

                            }

                        }

                    }

                }

            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="commandID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="idcID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="idcName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="idcAdd" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="idcZip" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="corp" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="idcOfficer">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="emergencyContact">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="houseInfo" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="houseName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="houseType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="houseProvince" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="houseCity" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="houseCounty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="houseAdd" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="houseZip" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="houseOfficer">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="gatewayInfo" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="gatewayIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="IPsegInfo" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                             &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                             &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                             &lt;element name="useTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="userInfo" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="nature" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="info">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="unitName" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="unitNature" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="officer">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                       &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                       &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                       &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                       &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                       &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="add" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;element name="zipCode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                             &lt;choice>
     *                               &lt;element name="serviceInfo" maxOccurs="unbounded" minOccurs="0">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;sequence>
     *                                         &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                         &lt;element name="serviceContent" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded"/>
     *                                         &lt;element name="regType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                         &lt;element name="regID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                         &lt;element name="setMode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                         &lt;element name="domain" maxOccurs="unbounded" minOccurs="0">
     *                                           &lt;complexType>
     *                                             &lt;complexContent>
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                 &lt;sequence>
     *                                                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                 &lt;/sequence>
     *                                               &lt;/restriction>
     *                                             &lt;/complexContent>
     *                                           &lt;/complexType>
     *                                         &lt;/element>
     *                                         &lt;element name="housesHoldInfo" maxOccurs="unbounded">
     *                                           &lt;complexType>
     *                                             &lt;complexContent>
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                 &lt;sequence>
     *                                                   &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                   &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                   &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                   &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                                   &lt;element name="IPtrans" maxOccurs="unbounded">
     *                                                     &lt;complexType>
     *                                                       &lt;complexContent>
     *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                           &lt;sequence>
     *                                                             &lt;element name="internetIP">
     *                                                               &lt;complexType>
     *                                                                 &lt;complexContent>
     *                                                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                     &lt;sequence>
     *                                                                       &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                                       &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                                     &lt;/sequence>
     *                                                                   &lt;/restriction>
     *                                                                 &lt;/complexContent>
     *                                                               &lt;/complexType>
     *                                                             &lt;/element>
     *                                                             &lt;element name="netIP" minOccurs="0">
     *                                                               &lt;complexType>
     *                                                                 &lt;complexContent>
     *                                                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                     &lt;sequence>
     *                                                                       &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                                       &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                                     &lt;/sequence>
     *                                                                   &lt;/restriction>
     *                                                                 &lt;/complexContent>
     *                                                               &lt;/complexType>
     *                                                             &lt;/element>
     *                                                           &lt;/sequence>
     *                                                         &lt;/restriction>
     *                                                       &lt;/complexContent>
     *                                                     &lt;/complexType>
     *                                                   &lt;/element>
     *                                                 &lt;/sequence>
     *                                               &lt;/restriction>
     *                                             &lt;/complexContent>
     *                                           &lt;/complexType>
     *                                         &lt;/element>
     *                                       &lt;/sequence>
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                               &lt;element name="houseHoldInfo" maxOccurs="unbounded" minOccurs="0">
     *                                 &lt;complexType>
     *                                   &lt;complexContent>
     *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                       &lt;sequence>
     *                                         &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                         &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                         &lt;element name="IPseg" maxOccurs="unbounded">
     *                                           &lt;complexType>
     *                                             &lt;complexContent>
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                 &lt;sequence>
     *                                                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                 &lt;/sequence>
     *                                               &lt;/restriction>
     *                                             &lt;/complexContent>
     *                                           &lt;/complexType>
     *                                         &lt;/element>
     *                                         &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                       &lt;/sequence>
     *                                     &lt;/restriction>
     *                                   &lt;/complexContent>
     *                                 &lt;/complexType>
     *                               &lt;/element>
     *                             &lt;/choice>
     *                             &lt;element name="registerTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "commandID",
        "idcID",
        "idcName",
        "idcAdd",
        "idcZip",
        "corp",
        "idcOfficer",
        "emergencyContact",
        "houseInfo",
        "userInfo"
    })
    public static class QueryResult {

        @XmlElement(required = true)
        protected Object commandID;
        @XmlElement(required = true)
        protected String idcID;
        @XmlElement(required = true)
        protected String idcName;
        @XmlElement(required = true)
        protected String idcAdd;
        @XmlElement(required = true)
        protected String idcZip;
        @XmlElement(required = true)
        protected Object corp;
        @XmlElement(required = true)
        protected IdcOfficer idcOfficer;
        @XmlElement(required = true)
        protected EmergencyContact emergencyContact;
        protected List<HouseInfo> houseInfo;
        protected List<UserInfo> userInfo;

        /**
         * 获取commandID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getCommandID() {
            return commandID;
        }

        /**
         * 设置commandID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setCommandID(Object value) {
            this.commandID = value;
        }

        /**
         * 获取idcID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdcID() {
            return idcID;
        }

        /**
         * 设置idcID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdcID(String value) {
            this.idcID = value;
        }

        /**
         * 获取idcName属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdcName() {
            return idcName;
        }

        /**
         * 设置idcName属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdcName(String value) {
            this.idcName = value;
        }

        /**
         * 获取idcAdd属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdcAdd() {
            return idcAdd;
        }

        /**
         * 设置idcAdd属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdcAdd(String value) {
            this.idcAdd = value;
        }

        /**
         * 获取idcZip属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdcZip() {
            return idcZip;
        }

        /**
         * 设置idcZip属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdcZip(String value) {
            this.idcZip = value;
        }

        /**
         * 获取corp属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getCorp() {
            return corp;
        }

        /**
         * 设置corp属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setCorp(Object value) {
            this.corp = value;
        }

        /**
         * 获取idcOfficer属性的值。
         * 
         * @return
         *     possible object is
         *     {@link IdcOfficer }
         *     
         */
        public IdcOfficer getIdcOfficer() {
            return idcOfficer;
        }

        /**
         * 设置idcOfficer属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link IdcOfficer }
         *     
         */
        public void setIdcOfficer(IdcOfficer value) {
            this.idcOfficer = value;
        }

        /**
         * 获取emergencyContact属性的值。
         * 
         * @return
         *     possible object is
         *     {@link EmergencyContact }
         *     
         */
        public EmergencyContact getEmergencyContact() {
            return emergencyContact;
        }

        /**
         * 设置emergencyContact属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link EmergencyContact }
         *     
         */
        public void setEmergencyContact(EmergencyContact value) {
            this.emergencyContact = value;
        }

        /**
         * Gets the value of the houseInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the houseInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getHouseInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link HouseInfo }
         * 
         * 
         */
        public List<HouseInfo> getHouseInfo() {
            if (houseInfo == null) {
                houseInfo = new ArrayList<HouseInfo>();
            }
            return this.houseInfo;
        }

        /**
         * Gets the value of the userInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the userInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getUserInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link UserInfo }
         * 
         * 
         */
        public List<UserInfo> getUserInfo() {
            if (userInfo == null) {
                userInfo = new ArrayList<UserInfo>();
            }
            return this.userInfo;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "name",
            "iDtype",
            "id",
            "tel",
            "mobile",
            "email"
        })
        public static class EmergencyContact {

            @XmlElement(required = true)
            protected Object name;
            @XmlElement(name = "IDtype", required = true)
            protected Object iDtype;
            @XmlElement(name = "ID", required = true)
            protected Object id;
            @XmlElement(required = true)
            protected Object tel;
            @XmlElement(required = true)
            protected Object mobile;
            @XmlElement(required = true)
            protected Object email;

            /**
             * 获取name属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getName() {
                return name;
            }

            /**
             * 设置name属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setName(Object value) {
                this.name = value;
            }

            /**
             * 获取iDtype属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getIDtype() {
                return iDtype;
            }

            /**
             * 设置iDtype属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setIDtype(Object value) {
                this.iDtype = value;
            }

            /**
             * 获取id属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getID() {
                return id;
            }

            /**
             * 设置id属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setID(Object value) {
                this.id = value;
            }

            /**
             * 获取tel属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getTel() {
                return tel;
            }

            /**
             * 设置tel属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setTel(Object value) {
                this.tel = value;
            }

            /**
             * 获取mobile属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMobile() {
                return mobile;
            }

            /**
             * 设置mobile属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMobile(Object value) {
                this.mobile = value;
            }

            /**
             * 获取email属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getEmail() {
                return email;
            }

            /**
             * 设置email属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setEmail(Object value) {
                this.email = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="houseName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="houseType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="houseProvince" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="houseCity" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="houseCounty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="houseAdd" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="houseZip" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="houseOfficer">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="gatewayInfo" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="gatewayIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="IPsegInfo" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                   &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                   &lt;element name="useTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "houseID",
            "houseName",
            "houseType",
            "houseProvince",
            "houseCity",
            "houseCounty",
            "houseAdd",
            "houseZip",
            "houseOfficer",
            "gatewayInfo",
            "iPsegInfo"
        })
        public static class HouseInfo {

            @XmlElement(required = true)
            protected String houseID;
            protected String houseName;
            @XmlElement(required = true)
            protected String houseType;
            @XmlElement(required = true)
            protected String houseProvince;
            @XmlElement(required = true)
            protected String houseCity;
            protected String houseCounty;
            @XmlElement(required = true)
            protected String houseAdd;
            @XmlElement(required = true)
            protected String houseZip;
            @XmlElement(required = true)
            protected HouseOfficer houseOfficer;
            @XmlElement(required = true)
            protected List<GatewayInfo> gatewayInfo;
            @XmlElement(name = "IPsegInfo", required = true)
            protected List<IPsegInfo> iPsegInfo;

            /**
             * 获取houseID属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseID() {
                return houseID;
            }

            /**
             * 设置houseID属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseID(String value) {
                this.houseID = value;
            }

            /**
             * 获取houseName属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseName() {
                return houseName;
            }

            /**
             * 设置houseName属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseName(String value) {
                this.houseName = value;
            }

            /**
             * 获取houseType属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseType() {
                return houseType;
            }

            /**
             * 设置houseType属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseType(String value) {
                this.houseType = value;
            }

            /**
             * 获取houseProvince属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseProvince() {
                return houseProvince;
            }

            /**
             * 设置houseProvince属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseProvince(String value) {
                this.houseProvince = value;
            }

            /**
             * 获取houseCity属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseCity() {
                return houseCity;
            }

            /**
             * 设置houseCity属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseCity(String value) {
                this.houseCity = value;
            }

            /**
             * 获取houseCounty属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseCounty() {
                return houseCounty;
            }

            /**
             * 设置houseCounty属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseCounty(String value) {
                this.houseCounty = value;
            }

            /**
             * 获取houseAdd属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseAdd() {
                return houseAdd;
            }

            /**
             * 设置houseAdd属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseAdd(String value) {
                this.houseAdd = value;
            }

            /**
             * 获取houseZip属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseZip() {
                return houseZip;
            }

            /**
             * 设置houseZip属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseZip(String value) {
                this.houseZip = value;
            }

            /**
             * 获取houseOfficer属性的值。
             * 
             * @return
             *     possible object is
             *     {@link HouseOfficer }
             *     
             */
            public HouseOfficer getHouseOfficer() {
                return houseOfficer;
            }

            /**
             * 设置houseOfficer属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link HouseOfficer }
             *     
             */
            public void setHouseOfficer(HouseOfficer value) {
                this.houseOfficer = value;
            }

            /**
             * Gets the value of the gatewayInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the gatewayInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getGatewayInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link GatewayInfo }
             * 
             * 
             */
            public List<GatewayInfo> getGatewayInfo() {
                if (gatewayInfo == null) {
                    gatewayInfo = new ArrayList<GatewayInfo>();
                }
                return this.gatewayInfo;
            }

            /**
             * Gets the value of the iPsegInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the iPsegInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getIPsegInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link IPsegInfo }
             * 
             * 
             */
            public List<IPsegInfo> getIPsegInfo() {
                if (iPsegInfo == null) {
                    iPsegInfo = new ArrayList<IPsegInfo>();
                }
                return this.iPsegInfo;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="gatewayIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "id",
                "bandWidth",
                "gatewayIP"
            })
            public static class GatewayInfo {

                @XmlElement(name = "ID", required = true)
                protected Object id;
                @XmlElement(required = true)
                protected Object bandWidth;
                @XmlElement(required = true)
                protected Object gatewayIP;

                /**
                 * 获取id属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getID() {
                    return id;
                }

                /**
                 * 设置id属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setID(Object value) {
                    this.id = value;
                }

                /**
                 * 获取bandWidth属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getBandWidth() {
                    return bandWidth;
                }

                /**
                 * 设置bandWidth属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setBandWidth(Object value) {
                    this.bandWidth = value;
                }

                /**
                 * 获取gatewayIP属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getGatewayIP() {
                    return gatewayIP;
                }

                /**
                 * 设置gatewayIP属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setGatewayIP(Object value) {
                    this.gatewayIP = value;
                }

            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "name",
                "iDtype",
                "id",
                "tel",
                "mobile",
                "email"
            })
            public static class HouseOfficer {

                @XmlElement(required = true)
                protected Object name;
                @XmlElement(name = "IDtype", required = true)
                protected Object iDtype;
                @XmlElement(name = "ID", required = true)
                protected Object id;
                @XmlElement(required = true)
                protected Object tel;
                @XmlElement(required = true)
                protected Object mobile;
                @XmlElement(required = true)
                protected Object email;

                /**
                 * 获取name属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getName() {
                    return name;
                }

                /**
                 * 设置name属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setName(Object value) {
                    this.name = value;
                }

                /**
                 * 获取iDtype属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getIDtype() {
                    return iDtype;
                }

                /**
                 * 设置iDtype属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setIDtype(Object value) {
                    this.iDtype = value;
                }

                /**
                 * 获取id属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getID() {
                    return id;
                }

                /**
                 * 设置id属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setID(Object value) {
                    this.id = value;
                }

                /**
                 * 获取tel属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getTel() {
                    return tel;
                }

                /**
                 * 设置tel属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setTel(Object value) {
                    this.tel = value;
                }

                /**
                 * 获取mobile属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getMobile() {
                    return mobile;
                }

                /**
                 * 设置mobile属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setMobile(Object value) {
                    this.mobile = value;
                }

                /**
                 * 获取email属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getEmail() {
                    return email;
                }

                /**
                 * 设置email属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setEmail(Object value) {
                    this.email = value;
                }

            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *         &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *         &lt;element name="useTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "id",
                "startIP",
                "endIP",
                "type",
                "user",
                "iDtype",
                "iDnumber",
                "useTime"
            })
            public static class IPsegInfo {

                @XmlElement(name = "ID", required = true)
                protected Object id;
                @XmlElement(required = true)
                protected Object startIP;
                @XmlElement(required = true)
                protected Object endIP;
                @XmlElement(required = true)
                protected Object type;
                protected Object user;
                @XmlElement(name = "IDtype")
                protected Object iDtype;
                @XmlElement(name = "IDnumber")
                protected Object iDnumber;
                @XmlElement(required = true)
                protected Object useTime;

                /**
                 * 获取id属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getID() {
                    return id;
                }

                /**
                 * 设置id属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setID(Object value) {
                    this.id = value;
                }

                /**
                 * 获取startIP属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getStartIP() {
                    return startIP;
                }

                /**
                 * 设置startIP属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setStartIP(Object value) {
                    this.startIP = value;
                }

                /**
                 * 获取endIP属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getEndIP() {
                    return endIP;
                }

                /**
                 * 设置endIP属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setEndIP(Object value) {
                    this.endIP = value;
                }

                /**
                 * 获取type属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getType() {
                    return type;
                }

                /**
                 * 设置type属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setType(Object value) {
                    this.type = value;
                }

                /**
                 * 获取user属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getUser() {
                    return user;
                }

                /**
                 * 设置user属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setUser(Object value) {
                    this.user = value;
                }

                /**
                 * 获取iDtype属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getIDtype() {
                    return iDtype;
                }

                /**
                 * 设置iDtype属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setIDtype(Object value) {
                    this.iDtype = value;
                }

                /**
                 * 获取iDnumber属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getIDnumber() {
                    return iDnumber;
                }

                /**
                 * 设置iDnumber属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setIDnumber(Object value) {
                    this.iDnumber = value;
                }

                /**
                 * 获取useTime属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getUseTime() {
                    return useTime;
                }

                /**
                 * 设置useTime属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setUseTime(Object value) {
                    this.useTime = value;
                }

            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "name",
            "iDtype",
            "id",
            "tel",
            "mobile",
            "email"
        })
        public static class IdcOfficer {

            @XmlElement(required = true)
            protected Object name;
            @XmlElement(name = "IDtype", required = true)
            protected Object iDtype;
            @XmlElement(name = "ID", required = true)
            protected Object id;
            @XmlElement(required = true)
            protected Object tel;
            @XmlElement(required = true)
            protected Object mobile;
            @XmlElement(required = true)
            protected Object email;

            /**
             * 获取name属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getName() {
                return name;
            }

            /**
             * 设置name属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setName(Object value) {
                this.name = value;
            }

            /**
             * 获取iDtype属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getIDtype() {
                return iDtype;
            }

            /**
             * 设置iDtype属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setIDtype(Object value) {
                this.iDtype = value;
            }

            /**
             * 获取id属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getID() {
                return id;
            }

            /**
             * 设置id属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setID(Object value) {
                this.id = value;
            }

            /**
             * 获取tel属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getTel() {
                return tel;
            }

            /**
             * 设置tel属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setTel(Object value) {
                this.tel = value;
            }

            /**
             * 获取mobile属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMobile() {
                return mobile;
            }

            /**
             * 设置mobile属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMobile(Object value) {
                this.mobile = value;
            }

            /**
             * 获取email属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getEmail() {
                return email;
            }

            /**
             * 设置email属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setEmail(Object value) {
                this.email = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="nature" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="info">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="unitName" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="unitNature" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="officer">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                             &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                             &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                             &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                             &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="add" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;element name="zipCode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                   &lt;choice>
         *                     &lt;element name="serviceInfo" maxOccurs="unbounded" minOccurs="0">
         *                       &lt;complexType>
         *                         &lt;complexContent>
         *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                             &lt;sequence>
         *                               &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                               &lt;element name="serviceContent" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded"/>
         *                               &lt;element name="regType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                               &lt;element name="regID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                               &lt;element name="setMode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                               &lt;element name="domain" maxOccurs="unbounded" minOccurs="0">
         *                                 &lt;complexType>
         *                                   &lt;complexContent>
         *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                       &lt;sequence>
         *                                         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                       &lt;/sequence>
         *                                     &lt;/restriction>
         *                                   &lt;/complexContent>
         *                                 &lt;/complexType>
         *                               &lt;/element>
         *                               &lt;element name="housesHoldInfo" maxOccurs="unbounded">
         *                                 &lt;complexType>
         *                                   &lt;complexContent>
         *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                       &lt;sequence>
         *                                         &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                         &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                         &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                                         &lt;element name="IPtrans" maxOccurs="unbounded">
         *                                           &lt;complexType>
         *                                             &lt;complexContent>
         *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                 &lt;sequence>
         *                                                   &lt;element name="internetIP">
         *                                                     &lt;complexType>
         *                                                       &lt;complexContent>
         *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                           &lt;sequence>
         *                                                             &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                                             &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                                           &lt;/sequence>
         *                                                         &lt;/restriction>
         *                                                       &lt;/complexContent>
         *                                                     &lt;/complexType>
         *                                                   &lt;/element>
         *                                                   &lt;element name="netIP" minOccurs="0">
         *                                                     &lt;complexType>
         *                                                       &lt;complexContent>
         *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                           &lt;sequence>
         *                                                             &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                                             &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                                           &lt;/sequence>
         *                                                         &lt;/restriction>
         *                                                       &lt;/complexContent>
         *                                                     &lt;/complexType>
         *                                                   &lt;/element>
         *                                                 &lt;/sequence>
         *                                               &lt;/restriction>
         *                                             &lt;/complexContent>
         *                                           &lt;/complexType>
         *                                         &lt;/element>
         *                                       &lt;/sequence>
         *                                     &lt;/restriction>
         *                                   &lt;/complexContent>
         *                                 &lt;/complexType>
         *                               &lt;/element>
         *                             &lt;/sequence>
         *                           &lt;/restriction>
         *                         &lt;/complexContent>
         *                       &lt;/complexType>
         *                     &lt;/element>
         *                     &lt;element name="houseHoldInfo" maxOccurs="unbounded" minOccurs="0">
         *                       &lt;complexType>
         *                         &lt;complexContent>
         *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                             &lt;sequence>
         *                               &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                               &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                               &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                               &lt;element name="IPseg" maxOccurs="unbounded">
         *                                 &lt;complexType>
         *                                   &lt;complexContent>
         *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                       &lt;sequence>
         *                                         &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                       &lt;/sequence>
         *                                     &lt;/restriction>
         *                                   &lt;/complexContent>
         *                                 &lt;/complexType>
         *                               &lt;/element>
         *                               &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                             &lt;/sequence>
         *                           &lt;/restriction>
         *                         &lt;/complexContent>
         *                       &lt;/complexType>
         *                     &lt;/element>
         *                   &lt;/choice>
         *                   &lt;element name="registerTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "id",
            "nature",
            "info"
        })
        public static class UserInfo {

            @XmlElement(name = "ID", required = true)
            protected String id;
            @XmlElement(required = true)
            protected Object nature;
            @XmlElement(required = true)
            protected Info info;

            /**
             * 获取id属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getID() {
                return id;
            }

            /**
             * 设置id属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setID(String value) {
                this.id = value;
            }

            /**
             * 获取nature属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getNature() {
                return nature;
            }

            /**
             * 设置nature属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setNature(Object value) {
                this.nature = value;
            }

            /**
             * 获取info属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Info }
             *     
             */
            public Info getInfo() {
                return info;
            }

            /**
             * 设置info属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Info }
             *     
             */
            public void setInfo(Info value) {
                this.info = value;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="unitName" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="unitNature" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="officer">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                   &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                   &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="add" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;element name="zipCode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *         &lt;choice>
             *           &lt;element name="serviceInfo" maxOccurs="unbounded" minOccurs="0">
             *             &lt;complexType>
             *               &lt;complexContent>
             *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                   &lt;sequence>
             *                     &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                     &lt;element name="serviceContent" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded"/>
             *                     &lt;element name="regType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                     &lt;element name="regID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                     &lt;element name="setMode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                     &lt;element name="domain" maxOccurs="unbounded" minOccurs="0">
             *                       &lt;complexType>
             *                         &lt;complexContent>
             *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                             &lt;sequence>
             *                               &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                               &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                             &lt;/sequence>
             *                           &lt;/restriction>
             *                         &lt;/complexContent>
             *                       &lt;/complexType>
             *                     &lt;/element>
             *                     &lt;element name="housesHoldInfo" maxOccurs="unbounded">
             *                       &lt;complexType>
             *                         &lt;complexContent>
             *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                             &lt;sequence>
             *                               &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                               &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                               &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                               &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                               &lt;element name="IPtrans" maxOccurs="unbounded">
             *                                 &lt;complexType>
             *                                   &lt;complexContent>
             *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                       &lt;sequence>
             *                                         &lt;element name="internetIP">
             *                                           &lt;complexType>
             *                                             &lt;complexContent>
             *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                                 &lt;sequence>
             *                                                   &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                                                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                                                 &lt;/sequence>
             *                                               &lt;/restriction>
             *                                             &lt;/complexContent>
             *                                           &lt;/complexType>
             *                                         &lt;/element>
             *                                         &lt;element name="netIP" minOccurs="0">
             *                                           &lt;complexType>
             *                                             &lt;complexContent>
             *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                                 &lt;sequence>
             *                                                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                                                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                                                 &lt;/sequence>
             *                                               &lt;/restriction>
             *                                             &lt;/complexContent>
             *                                           &lt;/complexType>
             *                                         &lt;/element>
             *                                       &lt;/sequence>
             *                                     &lt;/restriction>
             *                                   &lt;/complexContent>
             *                                 &lt;/complexType>
             *                               &lt;/element>
             *                             &lt;/sequence>
             *                           &lt;/restriction>
             *                         &lt;/complexContent>
             *                       &lt;/complexType>
             *                     &lt;/element>
             *                   &lt;/sequence>
             *                 &lt;/restriction>
             *               &lt;/complexContent>
             *             &lt;/complexType>
             *           &lt;/element>
             *           &lt;element name="houseHoldInfo" maxOccurs="unbounded" minOccurs="0">
             *             &lt;complexType>
             *               &lt;complexContent>
             *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                   &lt;sequence>
             *                     &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                     &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                     &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                     &lt;element name="IPseg" maxOccurs="unbounded">
             *                       &lt;complexType>
             *                         &lt;complexContent>
             *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                             &lt;sequence>
             *                               &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                               &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                             &lt;/sequence>
             *                           &lt;/restriction>
             *                         &lt;/complexContent>
             *                       &lt;/complexType>
             *                     &lt;/element>
             *                     &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                   &lt;/sequence>
             *                 &lt;/restriction>
             *               &lt;/complexContent>
             *             &lt;/complexType>
             *           &lt;/element>
             *         &lt;/choice>
             *         &lt;element name="registerTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "unitName",
                "unitNature",
                "iDtype",
                "iDnumber",
                "officer",
                "add",
                "zipCode",
                "serviceInfo",
                "houseHoldInfo",
                "registerTime"
            })
            public static class Info {

                @XmlElement(required = true)
                protected Object unitName;
                @XmlElement(required = true)
                protected Object unitNature;
                @XmlElement(name = "IDtype", required = true)
                protected Object iDtype;
                @XmlElement(name = "IDnumber", required = true)
                protected Object iDnumber;
                @XmlElement(required = true)
                protected Officer officer;
                @XmlElement(required = true)
                protected Object add;
                @XmlElement(required = true)
                protected Object zipCode;
                protected List<ServiceInfo> serviceInfo;
                protected List<HouseHoldInfo> houseHoldInfo;
                @XmlElement(required = true)
                protected Object registerTime;

                /**
                 * 获取unitName属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getUnitName() {
                    return unitName;
                }

                /**
                 * 设置unitName属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setUnitName(Object value) {
                    this.unitName = value;
                }

                /**
                 * 获取unitNature属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getUnitNature() {
                    return unitNature;
                }

                /**
                 * 设置unitNature属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setUnitNature(Object value) {
                    this.unitNature = value;
                }

                /**
                 * 获取iDtype属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getIDtype() {
                    return iDtype;
                }

                /**
                 * 设置iDtype属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setIDtype(Object value) {
                    this.iDtype = value;
                }

                /**
                 * 获取iDnumber属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getIDnumber() {
                    return iDnumber;
                }

                /**
                 * 设置iDnumber属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setIDnumber(Object value) {
                    this.iDnumber = value;
                }

                /**
                 * 获取officer属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Officer }
                 *     
                 */
                public Officer getOfficer() {
                    return officer;
                }

                /**
                 * 设置officer属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Officer }
                 *     
                 */
                public void setOfficer(Officer value) {
                    this.officer = value;
                }

                /**
                 * 获取add属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getAdd() {
                    return add;
                }

                /**
                 * 设置add属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setAdd(Object value) {
                    this.add = value;
                }

                /**
                 * 获取zipCode属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getZipCode() {
                    return zipCode;
                }

                /**
                 * 设置zipCode属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setZipCode(Object value) {
                    this.zipCode = value;
                }

                /**
                 * Gets the value of the serviceInfo property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the serviceInfo property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getServiceInfo().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link ServiceInfo }
                 * 
                 * 
                 */
                public List<ServiceInfo> getServiceInfo() {
                    if (serviceInfo == null) {
                        serviceInfo = new ArrayList<ServiceInfo>();
                    }
                    return this.serviceInfo;
                }

                /**
                 * Gets the value of the houseHoldInfo property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the houseHoldInfo property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getHouseHoldInfo().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link HouseHoldInfo }
                 * 
                 * 
                 */
                public List<HouseHoldInfo> getHouseHoldInfo() {
                    if (houseHoldInfo == null) {
                        houseHoldInfo = new ArrayList<HouseHoldInfo>();
                    }
                    return this.houseHoldInfo;
                }

                /**
                 * 获取registerTime属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getRegisterTime() {
                    return registerTime;
                }

                /**
                 * 设置registerTime属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setRegisterTime(Object value) {
                    this.registerTime = value;
                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="IPseg" maxOccurs="unbounded">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "hhID",
                    "houseID",
                    "distributeTime",
                    "iPseg",
                    "bandWidth"
                })
                public static class HouseHoldInfo {

                    @XmlElement(required = true)
                    protected Object hhID;
                    @XmlElement(required = true)
                    protected Object houseID;
                    @XmlElement(required = true)
                    protected Object distributeTime;
                    @XmlElement(name = "IPseg", required = true)
                    protected List<IPseg> iPseg;
                    protected Object bandWidth;

                    /**
                     * 获取hhID属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getHhID() {
                        return hhID;
                    }

                    /**
                     * 设置hhID属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setHhID(Object value) {
                        this.hhID = value;
                    }

                    /**
                     * 获取houseID属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getHouseID() {
                        return houseID;
                    }

                    /**
                     * 设置houseID属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setHouseID(Object value) {
                        this.houseID = value;
                    }

                    /**
                     * 获取distributeTime属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getDistributeTime() {
                        return distributeTime;
                    }

                    /**
                     * 设置distributeTime属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setDistributeTime(Object value) {
                        this.distributeTime = value;
                    }

                    /**
                     * Gets the value of the iPseg property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the iPseg property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getIPseg().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link IPseg }
                     * 
                     * 
                     */
                    public List<IPseg> getIPseg() {
                        if (iPseg == null) {
                            iPseg = new ArrayList<IPseg>();
                        }
                        return this.iPseg;
                    }

                    /**
                     * 获取bandWidth属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getBandWidth() {
                        return bandWidth;
                    }

                    /**
                     * 设置bandWidth属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setBandWidth(Object value) {
                        this.bandWidth = value;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "startIP",
                        "endIP"
                    })
                    public static class IPseg {

                        @XmlElement(required = true)
                        protected Object startIP;
                        @XmlElement(required = true)
                        protected Object endIP;

                        /**
                         * 获取startIP属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getStartIP() {
                            return startIP;
                        }

                        /**
                         * 设置startIP属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setStartIP(Object value) {
                            this.startIP = value;
                        }

                        /**
                         * 获取endIP属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getEndIP() {
                            return endIP;
                        }

                        /**
                         * 设置endIP属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setEndIP(Object value) {
                            this.endIP = value;
                        }

                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "name",
                    "iDtype",
                    "id",
                    "tel",
                    "mobile",
                    "email"
                })
                public static class Officer {

                    @XmlElement(required = true)
                    protected Object name;
                    @XmlElement(name = "IDtype", required = true)
                    protected Object iDtype;
                    @XmlElement(name = "ID", required = true)
                    protected Object id;
                    @XmlElement(required = true)
                    protected Object tel;
                    @XmlElement(required = true)
                    protected Object mobile;
                    @XmlElement(required = true)
                    protected Object email;

                    /**
                     * 获取name属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getName() {
                        return name;
                    }

                    /**
                     * 设置name属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setName(Object value) {
                        this.name = value;
                    }

                    /**
                     * 获取iDtype属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getIDtype() {
                        return iDtype;
                    }

                    /**
                     * 设置iDtype属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setIDtype(Object value) {
                        this.iDtype = value;
                    }

                    /**
                     * 获取id属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getID() {
                        return id;
                    }

                    /**
                     * 设置id属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setID(Object value) {
                        this.id = value;
                    }

                    /**
                     * 获取tel属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getTel() {
                        return tel;
                    }

                    /**
                     * 设置tel属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setTel(Object value) {
                        this.tel = value;
                    }

                    /**
                     * 获取mobile属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getMobile() {
                        return mobile;
                    }

                    /**
                     * 设置mobile属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setMobile(Object value) {
                        this.mobile = value;
                    }

                    /**
                     * 获取email属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getEmail() {
                        return email;
                    }

                    /**
                     * 设置email属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setEmail(Object value) {
                        this.email = value;
                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="serviceContent" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded"/>
                 *         &lt;element name="regType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *         &lt;element name="regID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *         &lt;element name="setMode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="domain" maxOccurs="unbounded" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="housesHoldInfo" maxOccurs="unbounded">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                   &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                   &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                   &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *                   &lt;element name="IPtrans" maxOccurs="unbounded">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;sequence>
                 *                             &lt;element name="internetIP">
                 *                               &lt;complexType>
                 *                                 &lt;complexContent>
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                     &lt;sequence>
                 *                                       &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                                       &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                                     &lt;/sequence>
                 *                                   &lt;/restriction>
                 *                                 &lt;/complexContent>
                 *                               &lt;/complexType>
                 *                             &lt;/element>
                 *                             &lt;element name="netIP" minOccurs="0">
                 *                               &lt;complexType>
                 *                                 &lt;complexContent>
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                     &lt;sequence>
                 *                                       &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                                       &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                                     &lt;/sequence>
                 *                                   &lt;/restriction>
                 *                                 &lt;/complexContent>
                 *                               &lt;/complexType>
                 *                             &lt;/element>
                 *                           &lt;/sequence>
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "serviceID",
                    "serviceContent",
                    "regType",
                    "regID",
                    "setMode",
                    "domain",
                    "housesHoldInfo"
                })
                public static class ServiceInfo {

                    @XmlElement(required = true)
                    protected Object serviceID;
                    @XmlElement(required = true)
                    protected List<Object> serviceContent;
                    protected Object regType;
                    protected Object regID;
                    @XmlElement(required = true)
                    protected Object setMode;
                    protected List<Domain> domain;
                    @XmlElement(required = true)
                    protected List<HousesHoldInfo> housesHoldInfo;

                    /**
                     * 获取serviceID属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getServiceID() {
                        return serviceID;
                    }

                    /**
                     * 设置serviceID属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setServiceID(Object value) {
                        this.serviceID = value;
                    }

                    /**
                     * Gets the value of the serviceContent property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the serviceContent property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getServiceContent().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link Object }
                     * 
                     * 
                     */
                    public List<Object> getServiceContent() {
                        if (serviceContent == null) {
                            serviceContent = new ArrayList<Object>();
                        }
                        return this.serviceContent;
                    }

                    /**
                     * 获取regType属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getRegType() {
                        return regType;
                    }

                    /**
                     * 设置regType属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setRegType(Object value) {
                        this.regType = value;
                    }

                    /**
                     * 获取regID属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getRegID() {
                        return regID;
                    }

                    /**
                     * 设置regID属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setRegID(Object value) {
                        this.regID = value;
                    }

                    /**
                     * 获取setMode属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getSetMode() {
                        return setMode;
                    }

                    /**
                     * 设置setMode属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setSetMode(Object value) {
                        this.setMode = value;
                    }

                    /**
                     * Gets the value of the domain property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the domain property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getDomain().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link Domain }
                     * 
                     * 
                     */
                    public List<Domain> getDomain() {
                        if (domain == null) {
                            domain = new ArrayList<Domain>();
                        }
                        return this.domain;
                    }

                    /**
                     * Gets the value of the housesHoldInfo property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the housesHoldInfo property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getHousesHoldInfo().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link HousesHoldInfo }
                     * 
                     * 
                     */
                    public List<HousesHoldInfo> getHousesHoldInfo() {
                        if (housesHoldInfo == null) {
                            housesHoldInfo = new ArrayList<HousesHoldInfo>();
                        }
                        return this.housesHoldInfo;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "id",
                        "name"
                    })
                    public static class Domain {

                        @XmlElement(name = "ID", required = true)
                        protected Object id;
                        @XmlElement(required = true)
                        protected Object name;

                        /**
                         * 获取id属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getID() {
                            return id;
                        }

                        /**
                         * 设置id属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setID(Object value) {
                            this.id = value;
                        }

                        /**
                         * 获取name属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getName() {
                            return name;
                        }

                        /**
                         * 设置name属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setName(Object value) {
                            this.name = value;
                        }

                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *         &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *         &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                     *         &lt;element name="IPtrans" maxOccurs="unbounded">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="internetIP">
                     *                     &lt;complexType>
                     *                       &lt;complexContent>
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                           &lt;sequence>
                     *                             &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *                             &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *                           &lt;/sequence>
                     *                         &lt;/restriction>
                     *                       &lt;/complexContent>
                     *                     &lt;/complexType>
                     *                   &lt;/element>
                     *                   &lt;element name="netIP" minOccurs="0">
                     *                     &lt;complexType>
                     *                       &lt;complexContent>
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                           &lt;sequence>
                     *                             &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *                             &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *                           &lt;/sequence>
                     *                         &lt;/restriction>
                     *                       &lt;/complexContent>
                     *                     &lt;/complexType>
                     *                   &lt;/element>
                     *                 &lt;/sequence>
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "hhID",
                        "houseID",
                        "distributeTime",
                        "bandWidth",
                        "iPtrans"
                    })
                    public static class HousesHoldInfo {

                        @XmlElement(required = true)
                        protected Object hhID;
                        @XmlElement(required = true)
                        protected Object houseID;
                        @XmlElement(required = true)
                        protected Object distributeTime;
                        protected Object bandWidth;
                        @XmlElement(name = "IPtrans", required = true)
                        protected List<IPtrans> iPtrans;

                        /**
                         * 获取hhID属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getHhID() {
                            return hhID;
                        }

                        /**
                         * 设置hhID属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setHhID(Object value) {
                            this.hhID = value;
                        }

                        /**
                         * 获取houseID属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getHouseID() {
                            return houseID;
                        }

                        /**
                         * 设置houseID属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setHouseID(Object value) {
                            this.houseID = value;
                        }

                        /**
                         * 获取distributeTime属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getDistributeTime() {
                            return distributeTime;
                        }

                        /**
                         * 设置distributeTime属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setDistributeTime(Object value) {
                            this.distributeTime = value;
                        }

                        /**
                         * 获取bandWidth属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getBandWidth() {
                            return bandWidth;
                        }

                        /**
                         * 设置bandWidth属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setBandWidth(Object value) {
                            this.bandWidth = value;
                        }

                        /**
                         * Gets the value of the iPtrans property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the iPtrans property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getIPtrans().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link IPtrans }
                         * 
                         * 
                         */
                        public List<IPtrans> getIPtrans() {
                            if (iPtrans == null) {
                                iPtrans = new ArrayList<IPtrans>();
                            }
                            return this.iPtrans;
                        }


                        /**
                         * <p>anonymous complex type的 Java 类。
                         * 
                         * <p>以下模式片段指定包含在此类中的预期内容。
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="internetIP">
                         *           &lt;complexType>
                         *             &lt;complexContent>
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                 &lt;sequence>
                         *                   &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                         *                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                         *                 &lt;/sequence>
                         *               &lt;/restriction>
                         *             &lt;/complexContent>
                         *           &lt;/complexType>
                         *         &lt;/element>
                         *         &lt;element name="netIP" minOccurs="0">
                         *           &lt;complexType>
                         *             &lt;complexContent>
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                 &lt;sequence>
                         *                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                         *                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                         *                 &lt;/sequence>
                         *               &lt;/restriction>
                         *             &lt;/complexContent>
                         *           &lt;/complexType>
                         *         &lt;/element>
                         *       &lt;/sequence>
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "internetIP",
                            "netIP"
                        })
                        public static class IPtrans {

                            @XmlElement(required = true)
                            protected InternetIP internetIP;
                            protected NetIP netIP;

                            /**
                             * 获取internetIP属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link InternetIP }
                             *     
                             */
                            public InternetIP getInternetIP() {
                                return internetIP;
                            }

                            /**
                             * 设置internetIP属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link InternetIP }
                             *     
                             */
                            public void setInternetIP(InternetIP value) {
                                this.internetIP = value;
                            }

                            /**
                             * 获取netIP属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link NetIP }
                             *     
                             */
                            public NetIP getNetIP() {
                                return netIP;
                            }

                            /**
                             * 设置netIP属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link NetIP }
                             *     
                             */
                            public void setNetIP(NetIP value) {
                                this.netIP = value;
                            }


                            /**
                             * <p>anonymous complex type的 Java 类。
                             * 
                             * <p>以下模式片段指定包含在此类中的预期内容。
                             * 
                             * <pre>
                             * &lt;complexType>
                             *   &lt;complexContent>
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *       &lt;sequence>
                             *         &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                             *         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                             *       &lt;/sequence>
                             *     &lt;/restriction>
                             *   &lt;/complexContent>
                             * &lt;/complexType>
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "stratIP",
                                "endIP"
                            })
                            public static class InternetIP {

                                @XmlElement(required = true)
                                protected Object stratIP;
                                @XmlElement(required = true)
                                protected Object endIP;

                                /**
                                 * 获取stratIP属性的值。
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link Object }
                                 *     
                                 */
                                public Object getStratIP() {
                                    return stratIP;
                                }

                                /**
                                 * 设置stratIP属性的值。
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link Object }
                                 *     
                                 */
                                public void setStratIP(Object value) {
                                    this.stratIP = value;
                                }

                                /**
                                 * 获取endIP属性的值。
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link Object }
                                 *     
                                 */
                                public Object getEndIP() {
                                    return endIP;
                                }

                                /**
                                 * 设置endIP属性的值。
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link Object }
                                 *     
                                 */
                                public void setEndIP(Object value) {
                                    this.endIP = value;
                                }

                            }


                            /**
                             * <p>anonymous complex type的 Java 类。
                             * 
                             * <p>以下模式片段指定包含在此类中的预期内容。
                             * 
                             * <pre>
                             * &lt;complexType>
                             *   &lt;complexContent>
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *       &lt;sequence>
                             *         &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                             *         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                             *       &lt;/sequence>
                             *     &lt;/restriction>
                             *   &lt;/complexContent>
                             * &lt;/complexType>
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "startIP",
                                "endIP"
                            })
                            public static class NetIP {

                                @XmlElement(required = true)
                                protected Object startIP;
                                @XmlElement(required = true)
                                protected Object endIP;

                                /**
                                 * 获取startIP属性的值。
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link Object }
                                 *     
                                 */
                                public Object getStartIP() {
                                    return startIP;
                                }

                                /**
                                 * 设置startIP属性的值。
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link Object }
                                 *     
                                 */
                                public void setStartIP(Object value) {
                                    this.startIP = value;
                                }

                                /**
                                 * 获取endIP属性的值。
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link Object }
                                 *     
                                 */
                                public Object getEndIP() {
                                    return endIP;
                                }

                                /**
                                 * 设置endIP属性的值。
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link Object }
                                 *     
                                 */
                                public void setEndIP(Object value) {
                                    this.endIP = value;
                                }

                            }

                        }

                    }

                }

            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="idcID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="idcName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="idcAdd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="idcZip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="corp" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *         &lt;element name="idcOfficer" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="emergencyContact" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="updateData">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="houseInfo" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="houseName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="houseType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="houseProvince" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="houseCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="houseCounty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="houseAdd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="houseZip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="houseOfficer" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                       &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                       &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                       &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                       &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                       &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="gatewayInfo" maxOccurs="unbounded" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                       &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                       &lt;element name="gatewayIP" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="IPsegInfo" maxOccurs="unbounded" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                       &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                       &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                       &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                       &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                       &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                       &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                       &lt;element name="useTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="userInfo" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="nature" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                             &lt;element name="info" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="unitName" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                       &lt;element name="unitNature" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                       &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                       &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                       &lt;element name="officer" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                 &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                 &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                 &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                 &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                 &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="add" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                       &lt;element name="zipCode" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                       &lt;choice>
     *                                         &lt;element name="serviceInfo" maxOccurs="unbounded" minOccurs="0">
     *                                           &lt;complexType>
     *                                             &lt;complexContent>
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                 &lt;sequence>
     *                                                   &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                   &lt;element name="serviceContent" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
     *                                                   &lt;element name="regType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                                   &lt;element name="regID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                                   &lt;element name="setMode" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                                   &lt;element name="domain" maxOccurs="unbounded" minOccurs="0">
     *                                                     &lt;complexType>
     *                                                       &lt;complexContent>
     *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                           &lt;sequence>
     *                                                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                             &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                                           &lt;/sequence>
     *                                                         &lt;/restriction>
     *                                                       &lt;/complexContent>
     *                                                     &lt;/complexType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="housesHoldInfo" minOccurs="0">
     *                                                     &lt;complexType>
     *                                                       &lt;complexContent>
     *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                           &lt;sequence>
     *                                                             &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                             &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                                             &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                                             &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                                             &lt;element name="IPtrans" maxOccurs="unbounded" minOccurs="0">
     *                                                               &lt;complexType>
     *                                                                 &lt;complexContent>
     *                                                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                     &lt;sequence>
     *                                                                       &lt;element name="internetIP">
     *                                                                         &lt;complexType>
     *                                                                           &lt;complexContent>
     *                                                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                               &lt;sequence>
     *                                                                                 &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                                                 &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                                               &lt;/sequence>
     *                                                                             &lt;/restriction>
     *                                                                           &lt;/complexContent>
     *                                                                         &lt;/complexType>
     *                                                                       &lt;/element>
     *                                                                       &lt;element name="netIP" minOccurs="0">
     *                                                                         &lt;complexType>
     *                                                                           &lt;complexContent>
     *                                                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                               &lt;sequence>
     *                                                                                 &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                                                 &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                                               &lt;/sequence>
     *                                                                             &lt;/restriction>
     *                                                                           &lt;/complexContent>
     *                                                                         &lt;/complexType>
     *                                                                       &lt;/element>
     *                                                                     &lt;/sequence>
     *                                                                   &lt;/restriction>
     *                                                                 &lt;/complexContent>
     *                                                               &lt;/complexType>
     *                                                             &lt;/element>
     *                                                           &lt;/sequence>
     *                                                         &lt;/restriction>
     *                                                       &lt;/complexContent>
     *                                                     &lt;/complexType>
     *                                                   &lt;/element>
     *                                                 &lt;/sequence>
     *                                               &lt;/restriction>
     *                                             &lt;/complexContent>
     *                                           &lt;/complexType>
     *                                         &lt;/element>
     *                                         &lt;element name="houseHoldInfo" minOccurs="0">
     *                                           &lt;complexType>
     *                                             &lt;complexContent>
     *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                 &lt;sequence>
     *                                                   &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                   &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                                   &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                                   &lt;element name="IPseg" maxOccurs="unbounded" minOccurs="0">
     *                                                     &lt;complexType>
     *                                                       &lt;complexContent>
     *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                           &lt;sequence>
     *                                                             &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                             &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                                                           &lt;/sequence>
     *                                                         &lt;/restriction>
     *                                                       &lt;/complexContent>
     *                                                     &lt;/complexType>
     *                                                   &lt;/element>
     *                                                   &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                                 &lt;/sequence>
     *                                               &lt;/restriction>
     *                                             &lt;/complexContent>
     *                                           &lt;/complexType>
     *                                         &lt;/element>
     *                                       &lt;/choice>
     *                                       &lt;element name="registerTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idcID",
        "idcName",
        "idcAdd",
        "idcZip",
        "corp",
        "idcOfficer",
        "emergencyContact",
        "updateData"
    })
    public static class UpdateInfo {

        @XmlElement(required = true)
        protected String idcID;
        protected String idcName;
        protected String idcAdd;
        protected String idcZip;
        protected Object corp;
        protected IdcOfficer idcOfficer;
        protected EmergencyContact emergencyContact;
        @XmlElement(required = true)
        protected UpdateData updateData;

        /**
         * 获取idcID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdcID() {
            return idcID;
        }

        /**
         * 设置idcID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdcID(String value) {
            this.idcID = value;
        }

        /**
         * 获取idcName属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdcName() {
            return idcName;
        }

        /**
         * 设置idcName属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdcName(String value) {
            this.idcName = value;
        }

        /**
         * 获取idcAdd属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdcAdd() {
            return idcAdd;
        }

        /**
         * 设置idcAdd属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdcAdd(String value) {
            this.idcAdd = value;
        }

        /**
         * 获取idcZip属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdcZip() {
            return idcZip;
        }

        /**
         * 设置idcZip属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdcZip(String value) {
            this.idcZip = value;
        }

        /**
         * 获取corp属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getCorp() {
            return corp;
        }

        /**
         * 设置corp属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setCorp(Object value) {
            this.corp = value;
        }

        /**
         * 获取idcOfficer属性的值。
         * 
         * @return
         *     possible object is
         *     {@link IdcOfficer }
         *     
         */
        public IdcOfficer getIdcOfficer() {
            return idcOfficer;
        }

        /**
         * 设置idcOfficer属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link IdcOfficer }
         *     
         */
        public void setIdcOfficer(IdcOfficer value) {
            this.idcOfficer = value;
        }

        /**
         * 获取emergencyContact属性的值。
         * 
         * @return
         *     possible object is
         *     {@link EmergencyContact }
         *     
         */
        public EmergencyContact getEmergencyContact() {
            return emergencyContact;
        }

        /**
         * 设置emergencyContact属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link EmergencyContact }
         *     
         */
        public void setEmergencyContact(EmergencyContact value) {
            this.emergencyContact = value;
        }

        /**
         * 获取updateData属性的值。
         * 
         * @return
         *     possible object is
         *     {@link UpdateData }
         *     
         */
        public UpdateData getUpdateData() {
            return updateData;
        }

        /**
         * 设置updateData属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link UpdateData }
         *     
         */
        public void setUpdateData(UpdateData value) {
            this.updateData = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "name",
            "iDtype",
            "id",
            "tel",
            "mobile",
            "email"
        })
        public static class EmergencyContact {

            @XmlElement(required = true)
            protected Object name;
            @XmlElement(name = "IDtype", required = true)
            protected Object iDtype;
            @XmlElement(name = "ID", required = true)
            protected Object id;
            @XmlElement(required = true)
            protected Object tel;
            @XmlElement(required = true)
            protected Object mobile;
            @XmlElement(required = true)
            protected Object email;

            /**
             * 获取name属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getName() {
                return name;
            }

            /**
             * 设置name属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setName(Object value) {
                this.name = value;
            }

            /**
             * 获取iDtype属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getIDtype() {
                return iDtype;
            }

            /**
             * 设置iDtype属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setIDtype(Object value) {
                this.iDtype = value;
            }

            /**
             * 获取id属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getID() {
                return id;
            }

            /**
             * 设置id属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setID(Object value) {
                this.id = value;
            }

            /**
             * 获取tel属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getTel() {
                return tel;
            }

            /**
             * 设置tel属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setTel(Object value) {
                this.tel = value;
            }

            /**
             * 获取mobile属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMobile() {
                return mobile;
            }

            /**
             * 设置mobile属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMobile(Object value) {
                this.mobile = value;
            }

            /**
             * 获取email属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getEmail() {
                return email;
            }

            /**
             * 设置email属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setEmail(Object value) {
                this.email = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "name",
            "iDtype",
            "id",
            "tel",
            "mobile",
            "email"
        })
        public static class IdcOfficer {

            @XmlElement(required = true)
            protected Object name;
            @XmlElement(name = "IDtype", required = true)
            protected Object iDtype;
            @XmlElement(name = "ID", required = true)
            protected Object id;
            @XmlElement(required = true)
            protected Object tel;
            @XmlElement(required = true)
            protected Object mobile;
            @XmlElement(required = true)
            protected Object email;

            /**
             * 获取name属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getName() {
                return name;
            }

            /**
             * 设置name属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setName(Object value) {
                this.name = value;
            }

            /**
             * 获取iDtype属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getIDtype() {
                return iDtype;
            }

            /**
             * 设置iDtype属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setIDtype(Object value) {
                this.iDtype = value;
            }

            /**
             * 获取id属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getID() {
                return id;
            }

            /**
             * 设置id属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setID(Object value) {
                this.id = value;
            }

            /**
             * 获取tel属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getTel() {
                return tel;
            }

            /**
             * 设置tel属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setTel(Object value) {
                this.tel = value;
            }

            /**
             * 获取mobile属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getMobile() {
                return mobile;
            }

            /**
             * 设置mobile属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setMobile(Object value) {
                this.mobile = value;
            }

            /**
             * 获取email属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getEmail() {
                return email;
            }

            /**
             * 设置email属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setEmail(Object value) {
                this.email = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="houseInfo" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="houseName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="houseType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="houseProvince" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="houseCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="houseCounty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="houseAdd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="houseZip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="houseOfficer" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                             &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                             &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                             &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                             &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="gatewayInfo" maxOccurs="unbounded" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                             &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                             &lt;element name="gatewayIP" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="IPsegInfo" maxOccurs="unbounded" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                             &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                             &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                             &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                             &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                             &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                             &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                             &lt;element name="useTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="userInfo" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="nature" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                   &lt;element name="info" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="unitName" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                             &lt;element name="unitNature" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                             &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                             &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                             &lt;element name="officer" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                       &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                       &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                       &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                       &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                       &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="add" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                             &lt;element name="zipCode" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                             &lt;choice>
         *                               &lt;element name="serviceInfo" maxOccurs="unbounded" minOccurs="0">
         *                                 &lt;complexType>
         *                                   &lt;complexContent>
         *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                       &lt;sequence>
         *                                         &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                         &lt;element name="serviceContent" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
         *                                         &lt;element name="regType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                                         &lt;element name="regID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                                         &lt;element name="setMode" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                                         &lt;element name="domain" maxOccurs="unbounded" minOccurs="0">
         *                                           &lt;complexType>
         *                                             &lt;complexContent>
         *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                 &lt;sequence>
         *                                                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                                                 &lt;/sequence>
         *                                               &lt;/restriction>
         *                                             &lt;/complexContent>
         *                                           &lt;/complexType>
         *                                         &lt;/element>
         *                                         &lt;element name="housesHoldInfo" minOccurs="0">
         *                                           &lt;complexType>
         *                                             &lt;complexContent>
         *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                 &lt;sequence>
         *                                                   &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                                   &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                                                   &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                                                   &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                                                   &lt;element name="IPtrans" maxOccurs="unbounded" minOccurs="0">
         *                                                     &lt;complexType>
         *                                                       &lt;complexContent>
         *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                           &lt;sequence>
         *                                                             &lt;element name="internetIP">
         *                                                               &lt;complexType>
         *                                                                 &lt;complexContent>
         *                                                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                                     &lt;sequence>
         *                                                                       &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                                                       &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                                                     &lt;/sequence>
         *                                                                   &lt;/restriction>
         *                                                                 &lt;/complexContent>
         *                                                               &lt;/complexType>
         *                                                             &lt;/element>
         *                                                             &lt;element name="netIP" minOccurs="0">
         *                                                               &lt;complexType>
         *                                                                 &lt;complexContent>
         *                                                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                                     &lt;sequence>
         *                                                                       &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                                                       &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                                                     &lt;/sequence>
         *                                                                   &lt;/restriction>
         *                                                                 &lt;/complexContent>
         *                                                               &lt;/complexType>
         *                                                             &lt;/element>
         *                                                           &lt;/sequence>
         *                                                         &lt;/restriction>
         *                                                       &lt;/complexContent>
         *                                                     &lt;/complexType>
         *                                                   &lt;/element>
         *                                                 &lt;/sequence>
         *                                               &lt;/restriction>
         *                                             &lt;/complexContent>
         *                                           &lt;/complexType>
         *                                         &lt;/element>
         *                                       &lt;/sequence>
         *                                     &lt;/restriction>
         *                                   &lt;/complexContent>
         *                                 &lt;/complexType>
         *                               &lt;/element>
         *                               &lt;element name="houseHoldInfo" minOccurs="0">
         *                                 &lt;complexType>
         *                                   &lt;complexContent>
         *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                       &lt;sequence>
         *                                         &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                                         &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                                         &lt;element name="IPseg" maxOccurs="unbounded" minOccurs="0">
         *                                           &lt;complexType>
         *                                             &lt;complexContent>
         *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                 &lt;sequence>
         *                                                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *                                                 &lt;/sequence>
         *                                               &lt;/restriction>
         *                                             &lt;/complexContent>
         *                                           &lt;/complexType>
         *                                         &lt;/element>
         *                                         &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                                       &lt;/sequence>
         *                                     &lt;/restriction>
         *                                   &lt;/complexContent>
         *                                 &lt;/complexType>
         *                               &lt;/element>
         *                             &lt;/choice>
         *                             &lt;element name="registerTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "houseInfo",
            "userInfo"
        })
        public static class UpdateData {

            protected List<HouseInfo> houseInfo;
            protected List<UserInfo> userInfo;

            /**
             * Gets the value of the houseInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the houseInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getHouseInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link HouseInfo }
             * 
             * 
             */
            public List<HouseInfo> getHouseInfo() {
                if (houseInfo == null) {
                    houseInfo = new ArrayList<HouseInfo>();
                }
                return this.houseInfo;
            }

            /**
             * Gets the value of the userInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the userInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getUserInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link UserInfo }
             * 
             * 
             */
            public List<UserInfo> getUserInfo() {
                if (userInfo == null) {
                    userInfo = new ArrayList<UserInfo>();
                }
                return this.userInfo;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="houseName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="houseType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="houseProvince" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="houseCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="houseCounty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="houseAdd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="houseZip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="houseOfficer" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                   &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                   &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="gatewayInfo" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                   &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                   &lt;element name="gatewayIP" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="IPsegInfo" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                   &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                   &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                   &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                   &lt;element name="useTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "houseID",
                "houseName",
                "houseType",
                "houseProvince",
                "houseCity",
                "houseCounty",
                "houseAdd",
                "houseZip",
                "houseOfficer",
                "gatewayInfo",
                "iPsegInfo"
            })
            public static class HouseInfo {

                @XmlElement(required = true)
                protected String houseID;
                protected String houseName;
                protected String houseType;
                protected String houseProvince;
                protected String houseCity;
                protected String houseCounty;
                protected String houseAdd;
                protected String houseZip;
                protected HouseOfficer houseOfficer;
                protected List<GatewayInfo> gatewayInfo;
                @XmlElement(name = "IPsegInfo")
                protected List<IPsegInfo> iPsegInfo;

                /**
                 * 获取houseID属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHouseID() {
                    return houseID;
                }

                /**
                 * 设置houseID属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHouseID(String value) {
                    this.houseID = value;
                }

                /**
                 * 获取houseName属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHouseName() {
                    return houseName;
                }

                /**
                 * 设置houseName属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHouseName(String value) {
                    this.houseName = value;
                }

                /**
                 * 获取houseType属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHouseType() {
                    return houseType;
                }

                /**
                 * 设置houseType属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHouseType(String value) {
                    this.houseType = value;
                }

                /**
                 * 获取houseProvince属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHouseProvince() {
                    return houseProvince;
                }

                /**
                 * 设置houseProvince属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHouseProvince(String value) {
                    this.houseProvince = value;
                }

                /**
                 * 获取houseCity属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHouseCity() {
                    return houseCity;
                }

                /**
                 * 设置houseCity属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHouseCity(String value) {
                    this.houseCity = value;
                }

                /**
                 * 获取houseCounty属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHouseCounty() {
                    return houseCounty;
                }

                /**
                 * 设置houseCounty属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHouseCounty(String value) {
                    this.houseCounty = value;
                }

                /**
                 * 获取houseAdd属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHouseAdd() {
                    return houseAdd;
                }

                /**
                 * 设置houseAdd属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHouseAdd(String value) {
                    this.houseAdd = value;
                }

                /**
                 * 获取houseZip属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHouseZip() {
                    return houseZip;
                }

                /**
                 * 设置houseZip属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHouseZip(String value) {
                    this.houseZip = value;
                }

                /**
                 * 获取houseOfficer属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link HouseOfficer }
                 *     
                 */
                public HouseOfficer getHouseOfficer() {
                    return houseOfficer;
                }

                /**
                 * 设置houseOfficer属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link HouseOfficer }
                 *     
                 */
                public void setHouseOfficer(HouseOfficer value) {
                    this.houseOfficer = value;
                }

                /**
                 * Gets the value of the gatewayInfo property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the gatewayInfo property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getGatewayInfo().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link GatewayInfo }
                 * 
                 * 
                 */
                public List<GatewayInfo> getGatewayInfo() {
                    if (gatewayInfo == null) {
                        gatewayInfo = new ArrayList<GatewayInfo>();
                    }
                    return this.gatewayInfo;
                }

                /**
                 * Gets the value of the iPsegInfo property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the iPsegInfo property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getIPsegInfo().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link IPsegInfo }
                 * 
                 * 
                 */
                public List<IPsegInfo> getIPsegInfo() {
                    if (iPsegInfo == null) {
                        iPsegInfo = new ArrayList<IPsegInfo>();
                    }
                    return this.iPsegInfo;
                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *         &lt;element name="gatewayIP" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "id",
                    "bandWidth",
                    "gatewayIP"
                })
                public static class GatewayInfo {

                    @XmlElement(name = "ID", required = true)
                    protected Object id;
                    protected Object bandWidth;
                    protected Object gatewayIP;

                    /**
                     * 获取id属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getID() {
                        return id;
                    }

                    /**
                     * 设置id属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setID(Object value) {
                        this.id = value;
                    }

                    /**
                     * 获取bandWidth属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getBandWidth() {
                        return bandWidth;
                    }

                    /**
                     * 设置bandWidth属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setBandWidth(Object value) {
                        this.bandWidth = value;
                    }

                    /**
                     * 获取gatewayIP属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getGatewayIP() {
                        return gatewayIP;
                    }

                    /**
                     * 设置gatewayIP属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setGatewayIP(Object value) {
                        this.gatewayIP = value;
                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "name",
                    "iDtype",
                    "id",
                    "tel",
                    "mobile",
                    "email"
                })
                public static class HouseOfficer {

                    @XmlElement(required = true)
                    protected Object name;
                    @XmlElement(name = "IDtype", required = true)
                    protected Object iDtype;
                    @XmlElement(name = "ID", required = true)
                    protected Object id;
                    @XmlElement(required = true)
                    protected Object tel;
                    @XmlElement(required = true)
                    protected Object mobile;
                    @XmlElement(required = true)
                    protected Object email;

                    /**
                     * 获取name属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getName() {
                        return name;
                    }

                    /**
                     * 设置name属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setName(Object value) {
                        this.name = value;
                    }

                    /**
                     * 获取iDtype属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getIDtype() {
                        return iDtype;
                    }

                    /**
                     * 设置iDtype属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setIDtype(Object value) {
                        this.iDtype = value;
                    }

                    /**
                     * 获取id属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getID() {
                        return id;
                    }

                    /**
                     * 设置id属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setID(Object value) {
                        this.id = value;
                    }

                    /**
                     * 获取tel属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getTel() {
                        return tel;
                    }

                    /**
                     * 设置tel属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setTel(Object value) {
                        this.tel = value;
                    }

                    /**
                     * 获取mobile属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getMobile() {
                        return mobile;
                    }

                    /**
                     * 设置mobile属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setMobile(Object value) {
                        this.mobile = value;
                    }

                    /**
                     * 获取email属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getEmail() {
                        return email;
                    }

                    /**
                     * 设置email属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setEmail(Object value) {
                        this.email = value;
                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *         &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *         &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *         &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *         &lt;element name="useTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "id",
                    "startIP",
                    "endIP",
                    "type",
                    "user",
                    "iDtype",
                    "iDnumber",
                    "useTime"
                })
                public static class IPsegInfo {

                    @XmlElement(name = "ID", required = true)
                    protected Object id;
                    protected Object startIP;
                    protected Object endIP;
                    protected Object type;
                    protected Object user;
                    @XmlElement(name = "IDtype")
                    protected Object iDtype;
                    @XmlElement(name = "IDnumber")
                    protected Object iDnumber;
                    protected Object useTime;

                    /**
                     * 获取id属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getID() {
                        return id;
                    }

                    /**
                     * 设置id属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setID(Object value) {
                        this.id = value;
                    }

                    /**
                     * 获取startIP属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getStartIP() {
                        return startIP;
                    }

                    /**
                     * 设置startIP属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setStartIP(Object value) {
                        this.startIP = value;
                    }

                    /**
                     * 获取endIP属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getEndIP() {
                        return endIP;
                    }

                    /**
                     * 设置endIP属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setEndIP(Object value) {
                        this.endIP = value;
                    }

                    /**
                     * 获取type属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getType() {
                        return type;
                    }

                    /**
                     * 设置type属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setType(Object value) {
                        this.type = value;
                    }

                    /**
                     * 获取user属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getUser() {
                        return user;
                    }

                    /**
                     * 设置user属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setUser(Object value) {
                        this.user = value;
                    }

                    /**
                     * 获取iDtype属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getIDtype() {
                        return iDtype;
                    }

                    /**
                     * 设置iDtype属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setIDtype(Object value) {
                        this.iDtype = value;
                    }

                    /**
                     * 获取iDnumber属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getIDnumber() {
                        return iDnumber;
                    }

                    /**
                     * 设置iDnumber属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setIDnumber(Object value) {
                        this.iDnumber = value;
                    }

                    /**
                     * 获取useTime属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getUseTime() {
                        return useTime;
                    }

                    /**
                     * 设置useTime属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setUseTime(Object value) {
                        this.useTime = value;
                    }

                }

            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="nature" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *         &lt;element name="info" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="unitName" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                   &lt;element name="unitNature" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                   &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                   &lt;element name="officer" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                             &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                             &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                             &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                             &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="add" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                   &lt;element name="zipCode" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                   &lt;choice>
             *                     &lt;element name="serviceInfo" maxOccurs="unbounded" minOccurs="0">
             *                       &lt;complexType>
             *                         &lt;complexContent>
             *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                             &lt;sequence>
             *                               &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                               &lt;element name="serviceContent" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
             *                               &lt;element name="regType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                               &lt;element name="regID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                               &lt;element name="setMode" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                               &lt;element name="domain" maxOccurs="unbounded" minOccurs="0">
             *                                 &lt;complexType>
             *                                   &lt;complexContent>
             *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                       &lt;sequence>
             *                                         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                                         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                                       &lt;/sequence>
             *                                     &lt;/restriction>
             *                                   &lt;/complexContent>
             *                                 &lt;/complexType>
             *                               &lt;/element>
             *                               &lt;element name="housesHoldInfo" minOccurs="0">
             *                                 &lt;complexType>
             *                                   &lt;complexContent>
             *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                       &lt;sequence>
             *                                         &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                                         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                                         &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                                         &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                                         &lt;element name="IPtrans" maxOccurs="unbounded" minOccurs="0">
             *                                           &lt;complexType>
             *                                             &lt;complexContent>
             *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                                 &lt;sequence>
             *                                                   &lt;element name="internetIP">
             *                                                     &lt;complexType>
             *                                                       &lt;complexContent>
             *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                                           &lt;sequence>
             *                                                             &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                                                             &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                                                           &lt;/sequence>
             *                                                         &lt;/restriction>
             *                                                       &lt;/complexContent>
             *                                                     &lt;/complexType>
             *                                                   &lt;/element>
             *                                                   &lt;element name="netIP" minOccurs="0">
             *                                                     &lt;complexType>
             *                                                       &lt;complexContent>
             *                                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                                           &lt;sequence>
             *                                                             &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                                                             &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                                                           &lt;/sequence>
             *                                                         &lt;/restriction>
             *                                                       &lt;/complexContent>
             *                                                     &lt;/complexType>
             *                                                   &lt;/element>
             *                                                 &lt;/sequence>
             *                                               &lt;/restriction>
             *                                             &lt;/complexContent>
             *                                           &lt;/complexType>
             *                                         &lt;/element>
             *                                       &lt;/sequence>
             *                                     &lt;/restriction>
             *                                   &lt;/complexContent>
             *                                 &lt;/complexType>
             *                               &lt;/element>
             *                             &lt;/sequence>
             *                           &lt;/restriction>
             *                         &lt;/complexContent>
             *                       &lt;/complexType>
             *                     &lt;/element>
             *                     &lt;element name="houseHoldInfo" minOccurs="0">
             *                       &lt;complexType>
             *                         &lt;complexContent>
             *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                             &lt;sequence>
             *                               &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                               &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                               &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                               &lt;element name="IPseg" maxOccurs="unbounded" minOccurs="0">
             *                                 &lt;complexType>
             *                                   &lt;complexContent>
             *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                       &lt;sequence>
             *                                         &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                                         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
             *                                       &lt;/sequence>
             *                                     &lt;/restriction>
             *                                   &lt;/complexContent>
             *                                 &lt;/complexType>
             *                               &lt;/element>
             *                               &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                             &lt;/sequence>
             *                           &lt;/restriction>
             *                         &lt;/complexContent>
             *                       &lt;/complexType>
             *                     &lt;/element>
             *                   &lt;/choice>
             *                   &lt;element name="registerTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "id",
                "nature",
                "info"
            })
            public static class UserInfo {

                @XmlElement(name = "ID", required = true)
                protected String id;
                protected Object nature;
                protected Info info;

                /**
                 * 获取id属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getID() {
                    return id;
                }

                /**
                 * 设置id属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setID(String value) {
                    this.id = value;
                }

                /**
                 * 获取nature属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getNature() {
                    return nature;
                }

                /**
                 * 设置nature属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setNature(Object value) {
                    this.nature = value;
                }

                /**
                 * 获取info属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Info }
                 *     
                 */
                public Info getInfo() {
                    return info;
                }

                /**
                 * 设置info属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Info }
                 *     
                 */
                public void setInfo(Info value) {
                    this.info = value;
                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="unitName" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *         &lt;element name="unitNature" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *         &lt;element name="IDnumber" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *         &lt;element name="officer" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                   &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                   &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                   &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="add" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *         &lt;element name="zipCode" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *         &lt;choice>
                 *           &lt;element name="serviceInfo" maxOccurs="unbounded" minOccurs="0">
                 *             &lt;complexType>
                 *               &lt;complexContent>
                 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                   &lt;sequence>
                 *                     &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                     &lt;element name="serviceContent" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
                 *                     &lt;element name="regType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *                     &lt;element name="regID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *                     &lt;element name="setMode" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *                     &lt;element name="domain" maxOccurs="unbounded" minOccurs="0">
                 *                       &lt;complexType>
                 *                         &lt;complexContent>
                 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                             &lt;sequence>
                 *                               &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                               &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *                             &lt;/sequence>
                 *                           &lt;/restriction>
                 *                         &lt;/complexContent>
                 *                       &lt;/complexType>
                 *                     &lt;/element>
                 *                     &lt;element name="housesHoldInfo" minOccurs="0">
                 *                       &lt;complexType>
                 *                         &lt;complexContent>
                 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                             &lt;sequence>
                 *                               &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                               &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *                               &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *                               &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *                               &lt;element name="IPtrans" maxOccurs="unbounded" minOccurs="0">
                 *                                 &lt;complexType>
                 *                                   &lt;complexContent>
                 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                       &lt;sequence>
                 *                                         &lt;element name="internetIP">
                 *                                           &lt;complexType>
                 *                                             &lt;complexContent>
                 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                                 &lt;sequence>
                 *                                                   &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                                                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                                                 &lt;/sequence>
                 *                                               &lt;/restriction>
                 *                                             &lt;/complexContent>
                 *                                           &lt;/complexType>
                 *                                         &lt;/element>
                 *                                         &lt;element name="netIP" minOccurs="0">
                 *                                           &lt;complexType>
                 *                                             &lt;complexContent>
                 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                                 &lt;sequence>
                 *                                                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                                                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                                                 &lt;/sequence>
                 *                                               &lt;/restriction>
                 *                                             &lt;/complexContent>
                 *                                           &lt;/complexType>
                 *                                         &lt;/element>
                 *                                       &lt;/sequence>
                 *                                     &lt;/restriction>
                 *                                   &lt;/complexContent>
                 *                                 &lt;/complexType>
                 *                               &lt;/element>
                 *                             &lt;/sequence>
                 *                           &lt;/restriction>
                 *                         &lt;/complexContent>
                 *                       &lt;/complexType>
                 *                     &lt;/element>
                 *                   &lt;/sequence>
                 *                 &lt;/restriction>
                 *               &lt;/complexContent>
                 *             &lt;/complexType>
                 *           &lt;/element>
                 *           &lt;element name="houseHoldInfo" minOccurs="0">
                 *             &lt;complexType>
                 *               &lt;complexContent>
                 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                   &lt;sequence>
                 *                     &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                     &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *                     &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *                     &lt;element name="IPseg" maxOccurs="unbounded" minOccurs="0">
                 *                       &lt;complexType>
                 *                         &lt;complexContent>
                 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                             &lt;sequence>
                 *                               &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                               &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                 *                             &lt;/sequence>
                 *                           &lt;/restriction>
                 *                         &lt;/complexContent>
                 *                       &lt;/complexType>
                 *                     &lt;/element>
                 *                     &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *                   &lt;/sequence>
                 *                 &lt;/restriction>
                 *               &lt;/complexContent>
                 *             &lt;/complexType>
                 *           &lt;/element>
                 *         &lt;/choice>
                 *         &lt;element name="registerTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "unitName",
                    "unitNature",
                    "iDtype",
                    "iDnumber",
                    "officer",
                    "add",
                    "zipCode",
                    "serviceInfo",
                    "houseHoldInfo",
                    "registerTime"
                })
                public static class Info {

                    protected Object unitName;
                    protected Object unitNature;
                    @XmlElement(name = "IDtype")
                    protected Object iDtype;
                    @XmlElement(name = "IDnumber")
                    protected Object iDnumber;
                    protected Officer officer;
                    protected Object add;
                    protected Object zipCode;
                    protected List<ServiceInfo> serviceInfo;
                    protected HouseHoldInfo houseHoldInfo;
                    protected Object registerTime;

                    /**
                     * 获取unitName属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getUnitName() {
                        return unitName;
                    }

                    /**
                     * 设置unitName属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setUnitName(Object value) {
                        this.unitName = value;
                    }

                    /**
                     * 获取unitNature属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getUnitNature() {
                        return unitNature;
                    }

                    /**
                     * 设置unitNature属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setUnitNature(Object value) {
                        this.unitNature = value;
                    }

                    /**
                     * 获取iDtype属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getIDtype() {
                        return iDtype;
                    }

                    /**
                     * 设置iDtype属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setIDtype(Object value) {
                        this.iDtype = value;
                    }

                    /**
                     * 获取iDnumber属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getIDnumber() {
                        return iDnumber;
                    }

                    /**
                     * 设置iDnumber属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setIDnumber(Object value) {
                        this.iDnumber = value;
                    }

                    /**
                     * 获取officer属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Officer }
                     *     
                     */
                    public Officer getOfficer() {
                        return officer;
                    }

                    /**
                     * 设置officer属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Officer }
                     *     
                     */
                    public void setOfficer(Officer value) {
                        this.officer = value;
                    }

                    /**
                     * 获取add属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getAdd() {
                        return add;
                    }

                    /**
                     * 设置add属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setAdd(Object value) {
                        this.add = value;
                    }

                    /**
                     * 获取zipCode属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getZipCode() {
                        return zipCode;
                    }

                    /**
                     * 设置zipCode属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setZipCode(Object value) {
                        this.zipCode = value;
                    }

                    /**
                     * Gets the value of the serviceInfo property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the serviceInfo property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getServiceInfo().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link ServiceInfo }
                     * 
                     * 
                     */
                    public List<ServiceInfo> getServiceInfo() {
                        if (serviceInfo == null) {
                            serviceInfo = new ArrayList<ServiceInfo>();
                        }
                        return this.serviceInfo;
                    }

                    /**
                     * 获取houseHoldInfo属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link HouseHoldInfo }
                     *     
                     */
                    public HouseHoldInfo getHouseHoldInfo() {
                        return houseHoldInfo;
                    }

                    /**
                     * 设置houseHoldInfo属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link HouseHoldInfo }
                     *     
                     */
                    public void setHouseHoldInfo(HouseHoldInfo value) {
                        this.houseHoldInfo = value;
                    }

                    /**
                     * 获取registerTime属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Object }
                     *     
                     */
                    public Object getRegisterTime() {
                        return registerTime;
                    }

                    /**
                     * 设置registerTime属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Object }
                     *     
                     */
                    public void setRegisterTime(Object value) {
                        this.registerTime = value;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                     *         &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                     *         &lt;element name="IPseg" maxOccurs="unbounded" minOccurs="0">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *                 &lt;/sequence>
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *         &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "hhID",
                        "houseID",
                        "distributeTime",
                        "iPseg",
                        "bandWidth"
                    })
                    public static class HouseHoldInfo {

                        @XmlElement(required = true)
                        protected Object hhID;
                        protected Object houseID;
                        protected Object distributeTime;
                        @XmlElement(name = "IPseg")
                        protected List<IPseg> iPseg;
                        protected Object bandWidth;

                        /**
                         * 获取hhID属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getHhID() {
                            return hhID;
                        }

                        /**
                         * 设置hhID属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setHhID(Object value) {
                            this.hhID = value;
                        }

                        /**
                         * 获取houseID属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getHouseID() {
                            return houseID;
                        }

                        /**
                         * 设置houseID属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setHouseID(Object value) {
                            this.houseID = value;
                        }

                        /**
                         * 获取distributeTime属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getDistributeTime() {
                            return distributeTime;
                        }

                        /**
                         * 设置distributeTime属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setDistributeTime(Object value) {
                            this.distributeTime = value;
                        }

                        /**
                         * Gets the value of the iPseg property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the iPseg property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getIPseg().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link IPseg }
                         * 
                         * 
                         */
                        public List<IPseg> getIPseg() {
                            if (iPseg == null) {
                                iPseg = new ArrayList<IPseg>();
                            }
                            return this.iPseg;
                        }

                        /**
                         * 获取bandWidth属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getBandWidth() {
                            return bandWidth;
                        }

                        /**
                         * 设置bandWidth属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setBandWidth(Object value) {
                            this.bandWidth = value;
                        }


                        /**
                         * <p>anonymous complex type的 Java 类。
                         * 
                         * <p>以下模式片段指定包含在此类中的预期内容。
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                         *         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                         *       &lt;/sequence>
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "startIP",
                            "endIP"
                        })
                        public static class IPseg {

                            @XmlElement(required = true)
                            protected Object startIP;
                            @XmlElement(required = true)
                            protected Object endIP;

                            /**
                             * 获取startIP属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link Object }
                             *     
                             */
                            public Object getStartIP() {
                                return startIP;
                            }

                            /**
                             * 设置startIP属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Object }
                             *     
                             */
                            public void setStartIP(Object value) {
                                this.startIP = value;
                            }

                            /**
                             * 获取endIP属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link Object }
                             *     
                             */
                            public Object getEndIP() {
                                return endIP;
                            }

                            /**
                             * 设置endIP属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Object }
                             *     
                             */
                            public void setEndIP(Object value) {
                                this.endIP = value;
                            }

                        }

                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *         &lt;element name="IDtype" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *         &lt;element name="tel" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "name",
                        "iDtype",
                        "id",
                        "tel",
                        "mobile",
                        "email"
                    })
                    public static class Officer {

                        @XmlElement(required = true)
                        protected Object name;
                        @XmlElement(name = "IDtype", required = true)
                        protected Object iDtype;
                        @XmlElement(name = "ID", required = true)
                        protected Object id;
                        @XmlElement(required = true)
                        protected Object tel;
                        @XmlElement(required = true)
                        protected Object mobile;
                        @XmlElement(required = true)
                        protected Object email;

                        /**
                         * 获取name属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getName() {
                            return name;
                        }

                        /**
                         * 设置name属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setName(Object value) {
                            this.name = value;
                        }

                        /**
                         * 获取iDtype属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getIDtype() {
                            return iDtype;
                        }

                        /**
                         * 设置iDtype属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setIDtype(Object value) {
                            this.iDtype = value;
                        }

                        /**
                         * 获取id属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getID() {
                            return id;
                        }

                        /**
                         * 设置id属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setID(Object value) {
                            this.id = value;
                        }

                        /**
                         * 获取tel属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getTel() {
                            return tel;
                        }

                        /**
                         * 设置tel属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setTel(Object value) {
                            this.tel = value;
                        }

                        /**
                         * 获取mobile属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getMobile() {
                            return mobile;
                        }

                        /**
                         * 设置mobile属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setMobile(Object value) {
                            this.mobile = value;
                        }

                        /**
                         * 获取email属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getEmail() {
                            return email;
                        }

                        /**
                         * 设置email属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setEmail(Object value) {
                            this.email = value;
                        }

                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *         &lt;element name="serviceContent" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
                     *         &lt;element name="regType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                     *         &lt;element name="regID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                     *         &lt;element name="setMode" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                     *         &lt;element name="domain" maxOccurs="unbounded" minOccurs="0">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                     *                 &lt;/sequence>
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *         &lt;element name="housesHoldInfo" minOccurs="0">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *                   &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                     *                   &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                     *                   &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                     *                   &lt;element name="IPtrans" maxOccurs="unbounded" minOccurs="0">
                     *                     &lt;complexType>
                     *                       &lt;complexContent>
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                           &lt;sequence>
                     *                             &lt;element name="internetIP">
                     *                               &lt;complexType>
                     *                                 &lt;complexContent>
                     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                                     &lt;sequence>
                     *                                       &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *                                       &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *                                     &lt;/sequence>
                     *                                   &lt;/restriction>
                     *                                 &lt;/complexContent>
                     *                               &lt;/complexType>
                     *                             &lt;/element>
                     *                             &lt;element name="netIP" minOccurs="0">
                     *                               &lt;complexType>
                     *                                 &lt;complexContent>
                     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                                     &lt;sequence>
                     *                                       &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *                                       &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                     *                                     &lt;/sequence>
                     *                                   &lt;/restriction>
                     *                                 &lt;/complexContent>
                     *                               &lt;/complexType>
                     *                             &lt;/element>
                     *                           &lt;/sequence>
                     *                         &lt;/restriction>
                     *                       &lt;/complexContent>
                     *                     &lt;/complexType>
                     *                   &lt;/element>
                     *                 &lt;/sequence>
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "serviceID",
                        "serviceContent",
                        "regType",
                        "regID",
                        "setMode",
                        "domain",
                        "housesHoldInfo"
                    })
                    public static class ServiceInfo {

                        @XmlElement(required = true)
                        protected Object serviceID;
                        protected List<Object> serviceContent;
                        protected Object regType;
                        protected Object regID;
                        protected Object setMode;
                        protected List<Domain> domain;
                        protected HousesHoldInfo housesHoldInfo;

                        /**
                         * 获取serviceID属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getServiceID() {
                            return serviceID;
                        }

                        /**
                         * 设置serviceID属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setServiceID(Object value) {
                            this.serviceID = value;
                        }

                        /**
                         * Gets the value of the serviceContent property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the serviceContent property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getServiceContent().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link Object }
                         * 
                         * 
                         */
                        public List<Object> getServiceContent() {
                            if (serviceContent == null) {
                                serviceContent = new ArrayList<Object>();
                            }
                            return this.serviceContent;
                        }

                        /**
                         * 获取regType属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getRegType() {
                            return regType;
                        }

                        /**
                         * 设置regType属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setRegType(Object value) {
                            this.regType = value;
                        }

                        /**
                         * 获取regID属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getRegID() {
                            return regID;
                        }

                        /**
                         * 设置regID属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setRegID(Object value) {
                            this.regID = value;
                        }

                        /**
                         * 获取setMode属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getSetMode() {
                            return setMode;
                        }

                        /**
                         * 设置setMode属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setSetMode(Object value) {
                            this.setMode = value;
                        }

                        /**
                         * Gets the value of the domain property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the domain property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getDomain().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link Domain }
                         * 
                         * 
                         */
                        public List<Domain> getDomain() {
                            if (domain == null) {
                                domain = new ArrayList<Domain>();
                            }
                            return this.domain;
                        }

                        /**
                         * 获取housesHoldInfo属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link HousesHoldInfo }
                         *     
                         */
                        public HousesHoldInfo getHousesHoldInfo() {
                            return housesHoldInfo;
                        }

                        /**
                         * 设置housesHoldInfo属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link HousesHoldInfo }
                         *     
                         */
                        public void setHousesHoldInfo(HousesHoldInfo value) {
                            this.housesHoldInfo = value;
                        }


                        /**
                         * <p>anonymous complex type的 Java 类。
                         * 
                         * <p>以下模式片段指定包含在此类中的预期内容。
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                         *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                         *       &lt;/sequence>
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "id",
                            "name"
                        })
                        public static class Domain {

                            @XmlElement(name = "ID", required = true)
                            protected Object id;
                            protected Object name;

                            /**
                             * 获取id属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link Object }
                             *     
                             */
                            public Object getID() {
                                return id;
                            }

                            /**
                             * 设置id属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Object }
                             *     
                             */
                            public void setID(Object value) {
                                this.id = value;
                            }

                            /**
                             * 获取name属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link Object }
                             *     
                             */
                            public Object getName() {
                                return name;
                            }

                            /**
                             * 设置name属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Object }
                             *     
                             */
                            public void setName(Object value) {
                                this.name = value;
                            }

                        }


                        /**
                         * <p>anonymous complex type的 Java 类。
                         * 
                         * <p>以下模式片段指定包含在此类中的预期内容。
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="hhID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                         *         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                         *         &lt;element name="distributeTime" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                         *         &lt;element name="bandWidth" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
                         *         &lt;element name="IPtrans" maxOccurs="unbounded" minOccurs="0">
                         *           &lt;complexType>
                         *             &lt;complexContent>
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                 &lt;sequence>
                         *                   &lt;element name="internetIP">
                         *                     &lt;complexType>
                         *                       &lt;complexContent>
                         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                           &lt;sequence>
                         *                             &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                         *                             &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                         *                           &lt;/sequence>
                         *                         &lt;/restriction>
                         *                       &lt;/complexContent>
                         *                     &lt;/complexType>
                         *                   &lt;/element>
                         *                   &lt;element name="netIP" minOccurs="0">
                         *                     &lt;complexType>
                         *                       &lt;complexContent>
                         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                           &lt;sequence>
                         *                             &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                         *                             &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                         *                           &lt;/sequence>
                         *                         &lt;/restriction>
                         *                       &lt;/complexContent>
                         *                     &lt;/complexType>
                         *                   &lt;/element>
                         *                 &lt;/sequence>
                         *               &lt;/restriction>
                         *             &lt;/complexContent>
                         *           &lt;/complexType>
                         *         &lt;/element>
                         *       &lt;/sequence>
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "hhID",
                            "houseID",
                            "distributeTime",
                            "bandWidth",
                            "iPtrans"
                        })
                        public static class HousesHoldInfo {

                            @XmlElement(required = true)
                            protected Object hhID;
                            protected Object houseID;
                            protected Object distributeTime;
                            protected Object bandWidth;
                            @XmlElement(name = "IPtrans")
                            protected List<IPtrans> iPtrans;

                            /**
                             * 获取hhID属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link Object }
                             *     
                             */
                            public Object getHhID() {
                                return hhID;
                            }

                            /**
                             * 设置hhID属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Object }
                             *     
                             */
                            public void setHhID(Object value) {
                                this.hhID = value;
                            }

                            /**
                             * 获取houseID属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link Object }
                             *     
                             */
                            public Object getHouseID() {
                                return houseID;
                            }

                            /**
                             * 设置houseID属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Object }
                             *     
                             */
                            public void setHouseID(Object value) {
                                this.houseID = value;
                            }

                            /**
                             * 获取distributeTime属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link Object }
                             *     
                             */
                            public Object getDistributeTime() {
                                return distributeTime;
                            }

                            /**
                             * 设置distributeTime属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Object }
                             *     
                             */
                            public void setDistributeTime(Object value) {
                                this.distributeTime = value;
                            }

                            /**
                             * 获取bandWidth属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link Object }
                             *     
                             */
                            public Object getBandWidth() {
                                return bandWidth;
                            }

                            /**
                             * 设置bandWidth属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Object }
                             *     
                             */
                            public void setBandWidth(Object value) {
                                this.bandWidth = value;
                            }

                            /**
                             * Gets the value of the iPtrans property.
                             * 
                             * <p>
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a <CODE>set</CODE> method for the iPtrans property.
                             * 
                             * <p>
                             * For example, to add a new item, do as follows:
                             * <pre>
                             *    getIPtrans().add(newItem);
                             * </pre>
                             * 
                             * 
                             * <p>
                             * Objects of the following type(s) are allowed in the list
                             * {@link IPtrans }
                             * 
                             * 
                             */
                            public List<IPtrans> getIPtrans() {
                                if (iPtrans == null) {
                                    iPtrans = new ArrayList<IPtrans>();
                                }
                                return this.iPtrans;
                            }


                            /**
                             * <p>anonymous complex type的 Java 类。
                             * 
                             * <p>以下模式片段指定包含在此类中的预期内容。
                             * 
                             * <pre>
                             * &lt;complexType>
                             *   &lt;complexContent>
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *       &lt;sequence>
                             *         &lt;element name="internetIP">
                             *           &lt;complexType>
                             *             &lt;complexContent>
                             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *                 &lt;sequence>
                             *                   &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                             *                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                             *                 &lt;/sequence>
                             *               &lt;/restriction>
                             *             &lt;/complexContent>
                             *           &lt;/complexType>
                             *         &lt;/element>
                             *         &lt;element name="netIP" minOccurs="0">
                             *           &lt;complexType>
                             *             &lt;complexContent>
                             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *                 &lt;sequence>
                             *                   &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                             *                   &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                             *                 &lt;/sequence>
                             *               &lt;/restriction>
                             *             &lt;/complexContent>
                             *           &lt;/complexType>
                             *         &lt;/element>
                             *       &lt;/sequence>
                             *     &lt;/restriction>
                             *   &lt;/complexContent>
                             * &lt;/complexType>
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "internetIP",
                                "netIP"
                            })
                            public static class IPtrans {

                                @XmlElement(required = true)
                                protected InternetIP internetIP;
                                protected NetIP netIP;

                                /**
                                 * 获取internetIP属性的值。
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link InternetIP }
                                 *     
                                 */
                                public InternetIP getInternetIP() {
                                    return internetIP;
                                }

                                /**
                                 * 设置internetIP属性的值。
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link InternetIP }
                                 *     
                                 */
                                public void setInternetIP(InternetIP value) {
                                    this.internetIP = value;
                                }

                                /**
                                 * 获取netIP属性的值。
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link NetIP }
                                 *     
                                 */
                                public NetIP getNetIP() {
                                    return netIP;
                                }

                                /**
                                 * 设置netIP属性的值。
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link NetIP }
                                 *     
                                 */
                                public void setNetIP(NetIP value) {
                                    this.netIP = value;
                                }


                                /**
                                 * <p>anonymous complex type的 Java 类。
                                 * 
                                 * <p>以下模式片段指定包含在此类中的预期内容。
                                 * 
                                 * <pre>
                                 * &lt;complexType>
                                 *   &lt;complexContent>
                                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                                 *       &lt;sequence>
                                 *         &lt;element name="stratIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                                 *         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                                 *       &lt;/sequence>
                                 *     &lt;/restriction>
                                 *   &lt;/complexContent>
                                 * &lt;/complexType>
                                 * </pre>
                                 * 
                                 * 
                                 */
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @XmlType(name = "", propOrder = {
                                    "stratIP",
                                    "endIP"
                                })
                                public static class InternetIP {

                                    @XmlElement(required = true)
                                    protected Object stratIP;
                                    @XmlElement(required = true)
                                    protected Object endIP;

                                    /**
                                     * 获取stratIP属性的值。
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link Object }
                                     *     
                                     */
                                    public Object getStratIP() {
                                        return stratIP;
                                    }

                                    /**
                                     * 设置stratIP属性的值。
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link Object }
                                     *     
                                     */
                                    public void setStratIP(Object value) {
                                        this.stratIP = value;
                                    }

                                    /**
                                     * 获取endIP属性的值。
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link Object }
                                     *     
                                     */
                                    public Object getEndIP() {
                                        return endIP;
                                    }

                                    /**
                                     * 设置endIP属性的值。
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link Object }
                                     *     
                                     */
                                    public void setEndIP(Object value) {
                                        this.endIP = value;
                                    }

                                }


                                /**
                                 * <p>anonymous complex type的 Java 类。
                                 * 
                                 * <p>以下模式片段指定包含在此类中的预期内容。
                                 * 
                                 * <pre>
                                 * &lt;complexType>
                                 *   &lt;complexContent>
                                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                                 *       &lt;sequence>
                                 *         &lt;element name="startIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                                 *         &lt;element name="endIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
                                 *       &lt;/sequence>
                                 *     &lt;/restriction>
                                 *   &lt;/complexContent>
                                 * &lt;/complexType>
                                 * </pre>
                                 * 
                                 * 
                                 */
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @XmlType(name = "", propOrder = {
                                    "startIP",
                                    "endIP"
                                })
                                public static class NetIP {

                                    @XmlElement(required = true)
                                    protected Object startIP;
                                    @XmlElement(required = true)
                                    protected Object endIP;

                                    /**
                                     * 获取startIP属性的值。
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link Object }
                                     *     
                                     */
                                    public Object getStartIP() {
                                        return startIP;
                                    }

                                    /**
                                     * 设置startIP属性的值。
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link Object }
                                     *     
                                     */
                                    public void setStartIP(Object value) {
                                        this.startIP = value;
                                    }

                                    /**
                                     * 获取endIP属性的值。
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link Object }
                                     *     
                                     */
                                    public Object getEndIP() {
                                        return endIP;
                                    }

                                    /**
                                     * 设置endIP属性的值。
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link Object }
                                     *     
                                     */
                                    public void setEndIP(Object value) {
                                        this.endIP = value;
                                    }

                                }

                            }

                        }

                    }

                }

            }

        }

    }

}
