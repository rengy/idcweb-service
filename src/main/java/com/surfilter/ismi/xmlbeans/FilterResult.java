//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.2.8-b130911.1802 生成的
// 请访问 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2021.08.18 时间 11:04:46 AM CST 
//


package com.surfilter.ismi.xmlbeans;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idcID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="log" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="logID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="commandID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="srcIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="destIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="srcPort" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="destPort" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="domainName" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                   &lt;element name="proxyType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                   &lt;element name="proxyIP" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                   &lt;element name="proxyPort" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                   &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                   &lt;element name="content" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                   &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                   &lt;element name="attachment" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="file" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="gatherTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="timeStamp" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idcID",
    "log",
    "timeStamp"
})
@XmlRootElement(name = "filterResult")
public class FilterResult {

    @XmlElement(required = true)
    protected Object idcID;
    @XmlElement(required = true)
    protected List<Log> log;
    @XmlElement(required = true)
    protected Object timeStamp;

    /**
     * 获取idcID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getIdcID() {
        return idcID;
    }

    /**
     * 设置idcID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setIdcID(Object value) {
        this.idcID = value;
    }

    /**
     * Gets the value of the log property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the log property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLog().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Log }
     * 
     * 
     */
    public List<Log> getLog() {
        if (log == null) {
            log = new ArrayList<Log>();
        }
        return this.log;
    }

    /**
     * 获取timeStamp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getTimeStamp() {
        return timeStamp;
    }

    /**
     * 设置timeStamp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setTimeStamp(Object value) {
        this.timeStamp = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="logID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="commandID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="srcIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="destIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="srcPort" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="destPort" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="domainName" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *         &lt;element name="proxyType" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *         &lt;element name="proxyIP" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *         &lt;element name="proxyPort" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *         &lt;element name="content" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *         &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *         &lt;element name="attachment" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="file" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="gatherTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "logID",
        "commandID",
        "houseID",
        "srcIP",
        "destIP",
        "srcPort",
        "destPort",
        "domainName",
        "proxyType",
        "proxyIP",
        "proxyPort",
        "title",
        "content",
        "url",
        "attachment",
        "gatherTime"
    })
    public static class Log {

        @XmlElement(required = true)
        protected Object logID;
        @XmlElement(required = true)
        protected Object commandID;
        @XmlElement(required = true)
        protected Object houseID;
        @XmlElement(required = true)
        protected Object srcIP;
        @XmlElement(required = true)
        protected Object destIP;
        @XmlElement(required = true)
        protected Object srcPort;
        @XmlElement(required = true)
        protected Object destPort;
        protected Object domainName;
        protected Object proxyType;
        protected Object proxyIP;
        protected Object proxyPort;
        protected Object title;
        protected Object content;
        protected Object url;
        protected List<Attachment> attachment;
        @XmlElement(required = true)
        protected Object gatherTime;

        /**
         * 获取logID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getLogID() {
            return logID;
        }

        /**
         * 设置logID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setLogID(Object value) {
            this.logID = value;
        }

        /**
         * 获取commandID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getCommandID() {
            return commandID;
        }

        /**
         * 设置commandID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setCommandID(Object value) {
            this.commandID = value;
        }

        /**
         * 获取houseID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getHouseID() {
            return houseID;
        }

        /**
         * 设置houseID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setHouseID(Object value) {
            this.houseID = value;
        }

        /**
         * 获取srcIP属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getSrcIP() {
            return srcIP;
        }

        /**
         * 设置srcIP属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setSrcIP(Object value) {
            this.srcIP = value;
        }

        /**
         * 获取destIP属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getDestIP() {
            return destIP;
        }

        /**
         * 设置destIP属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setDestIP(Object value) {
            this.destIP = value;
        }

        /**
         * 获取srcPort属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getSrcPort() {
            return srcPort;
        }

        /**
         * 设置srcPort属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setSrcPort(Object value) {
            this.srcPort = value;
        }

        /**
         * 获取destPort属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getDestPort() {
            return destPort;
        }

        /**
         * 设置destPort属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setDestPort(Object value) {
            this.destPort = value;
        }

        /**
         * 获取domainName属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getDomainName() {
            return domainName;
        }

        /**
         * 设置domainName属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setDomainName(Object value) {
            this.domainName = value;
        }

        /**
         * 获取proxyType属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getProxyType() {
            return proxyType;
        }

        /**
         * 设置proxyType属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setProxyType(Object value) {
            this.proxyType = value;
        }

        /**
         * 获取proxyIP属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getProxyIP() {
            return proxyIP;
        }

        /**
         * 设置proxyIP属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setProxyIP(Object value) {
            this.proxyIP = value;
        }

        /**
         * 获取proxyPort属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getProxyPort() {
            return proxyPort;
        }

        /**
         * 设置proxyPort属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setProxyPort(Object value) {
            this.proxyPort = value;
        }

        /**
         * 获取title属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getTitle() {
            return title;
        }

        /**
         * 设置title属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setTitle(Object value) {
            this.title = value;
        }

        /**
         * 获取content属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getContent() {
            return content;
        }

        /**
         * 设置content属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setContent(Object value) {
            this.content = value;
        }

        /**
         * 获取url属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getUrl() {
            return url;
        }

        /**
         * 设置url属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setUrl(Object value) {
            this.url = value;
        }

        /**
         * Gets the value of the attachment property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the attachment property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAttachment().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Attachment }
         * 
         * 
         */
        public List<Attachment> getAttachment() {
            if (attachment == null) {
                attachment = new ArrayList<Attachment>();
            }
            return this.attachment;
        }

        /**
         * 获取gatherTime属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getGatherTime() {
            return gatherTime;
        }

        /**
         * 设置gatherTime属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setGatherTime(Object value) {
            this.gatherTime = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="file" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "title",
            "file"
        })
        public static class Attachment {

            @XmlElement(required = true)
            protected Object title;
            @XmlElement(required = true)
            protected Object file;

            /**
             * 获取title属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getTitle() {
                return title;
            }

            /**
             * 设置title属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setTitle(Object value) {
                this.title = value;
            }

            /**
             * 获取file属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getFile() {
                return file;
            }

            /**
             * 设置file属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setFile(Object value) {
                this.file = value;
            }

        }

    }

}
