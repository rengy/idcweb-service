//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.2.8-b130911.1802 生成的
// 请访问 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2021.08.18 时间 11:04:46 AM CST 
//


package com.surfilter.ismi.xmlbeans;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="commandID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="idcID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="result">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="logAmount" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="endFlag" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="msgInfo" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="log" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="logID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="srcIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="destIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="srcPort" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="destPort" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                   &lt;element name="accessTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="timeStamp" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "commandID",
    "idcID",
    "result",
    "log",
    "timeStamp"
})
@XmlRootElement(name = "logQueryResult")
public class LogQueryResult {

    @XmlElement(required = true)
    protected Object commandID;
    @XmlElement(required = true)
    protected Object idcID;
    @XmlElement(required = true)
    protected Result result;
    @XmlElement(required = true)
    protected List<Log> log;
    @XmlElement(required = true)
    protected Object timeStamp;

    /**
     * 获取commandID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getCommandID() {
        return commandID;
    }

    /**
     * 设置commandID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setCommandID(Object value) {
        this.commandID = value;
    }

    /**
     * 获取idcID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getIdcID() {
        return idcID;
    }

    /**
     * 设置idcID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setIdcID(Object value) {
        this.idcID = value;
    }

    /**
     * 获取result属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Result }
     *     
     */
    public Result getResult() {
        return result;
    }

    /**
     * 设置result属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Result }
     *     
     */
    public void setResult(Result value) {
        this.result = value;
    }

    /**
     * Gets the value of the log property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the log property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLog().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Log }
     * 
     * 
     */
    public List<Log> getLog() {
        if (log == null) {
            log = new ArrayList<Log>();
        }
        return this.log;
    }

    /**
     * 获取timeStamp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getTimeStamp() {
        return timeStamp;
    }

    /**
     * 设置timeStamp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setTimeStamp(Object value) {
        this.timeStamp = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="logID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="srcIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="destIP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="srcPort" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="destPort" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *         &lt;element name="accessTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "logID",
        "houseID",
        "srcIP",
        "destIP",
        "srcPort",
        "destPort",
        "url",
        "accessTime"
    })
    public static class Log {

        @XmlElement(required = true)
        protected Object logID;
        @XmlElement(required = true)
        protected Object houseID;
        @XmlElement(required = true)
        protected Object srcIP;
        @XmlElement(required = true)
        protected Object destIP;
        @XmlElement(required = true)
        protected Object srcPort;
        @XmlElement(required = true)
        protected Object destPort;
        protected Object url;
        @XmlElement(required = true)
        protected Object accessTime;

        /**
         * 获取logID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getLogID() {
            return logID;
        }

        /**
         * 设置logID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setLogID(Object value) {
            this.logID = value;
        }

        /**
         * 获取houseID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getHouseID() {
            return houseID;
        }

        /**
         * 设置houseID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setHouseID(Object value) {
            this.houseID = value;
        }

        /**
         * 获取srcIP属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getSrcIP() {
            return srcIP;
        }

        /**
         * 设置srcIP属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setSrcIP(Object value) {
            this.srcIP = value;
        }

        /**
         * 获取destIP属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getDestIP() {
            return destIP;
        }

        /**
         * 设置destIP属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setDestIP(Object value) {
            this.destIP = value;
        }

        /**
         * 获取srcPort属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getSrcPort() {
            return srcPort;
        }

        /**
         * 设置srcPort属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setSrcPort(Object value) {
            this.srcPort = value;
        }

        /**
         * 获取destPort属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getDestPort() {
            return destPort;
        }

        /**
         * 设置destPort属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setDestPort(Object value) {
            this.destPort = value;
        }

        /**
         * 获取url属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getUrl() {
            return url;
        }

        /**
         * 设置url属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setUrl(Object value) {
            this.url = value;
        }

        /**
         * 获取accessTime属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getAccessTime() {
            return accessTime;
        }

        /**
         * 设置accessTime属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setAccessTime(Object value) {
            this.accessTime = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="logAmount" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="endFlag" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="msgInfo" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "logAmount",
        "endFlag",
        "msgInfo"
    })
    public static class Result {

        @XmlElement(required = true)
        protected Object logAmount;
        @XmlElement(required = true)
        protected Object endFlag;
        protected Object msgInfo;

        /**
         * 获取logAmount属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getLogAmount() {
            return logAmount;
        }

        /**
         * 设置logAmount属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setLogAmount(Object value) {
            this.logAmount = value;
        }

        /**
         * 获取endFlag属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getEndFlag() {
            return endFlag;
        }

        /**
         * 设置endFlag属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setEndFlag(Object value) {
            this.endFlag = value;
        }

        /**
         * 获取msgInfo属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getMsgInfo() {
            return msgInfo;
        }

        /**
         * 设置msgInfo属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setMsgInfo(Object value) {
            this.msgInfo = value;
        }

    }

}
