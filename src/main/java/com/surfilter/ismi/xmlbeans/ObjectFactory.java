//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.2.8-b130911.1802 生成的
// 请访问 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2021.08.18 时间 11:04:46 AM CST 
//


package com.surfilter.ismi.xmlbeans;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.surfilter.ismi.xmlbeans package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.surfilter.ismi.xmlbeans
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IdcCommandAck }
     * 
     */
    public IdcCommandAck createIdcCommandAck() {
        return new IdcCommandAck();
    }

    /**
     * Create an instance of {@link LogQuery }
     * 
     */
    public LogQuery createLogQuery() {
        return new LogQuery();
    }

    /**
     * Create an instance of {@link MonitorResult }
     * 
     */
    public MonitorResult createMonitorResult() {
        return new MonitorResult();
    }

    /**
     * Create an instance of {@link IdcMonitor }
     * 
     */
    public IdcMonitor createIdcMonitor() {
        return new IdcMonitor();
    }

    /**
     * Create an instance of {@link CodeList }
     * 
     */
    public CodeList createCodeList() {
        return new CodeList();
    }

    /**
     * Create an instance of {@link IdcCommand }
     * 
     */
    public IdcCommand createIdcCommand() {
        return new IdcCommand();
    }

    /**
     * Create an instance of {@link UploadData }
     * 
     */
    public UploadData createUploadData() {
        return new UploadData();
    }

    /**
     * Create an instance of {@link CommandQueryResult }
     * 
     */
    public CommandQueryResult createCommandQueryResult() {
        return new CommandQueryResult();
    }

    /**
     * Create an instance of {@link LogQueryResult }
     * 
     */
    public LogQueryResult createLogQueryResult() {
        return new LogQueryResult();
    }

    /**
     * Create an instance of {@link FilterResult }
     * 
     */
    public FilterResult createFilterResult() {
        return new FilterResult();
    }

    /**
     * Create an instance of {@link FilterResult.Log }
     * 
     */
    public FilterResult.Log createFilterResultLog() {
        return new FilterResult.Log();
    }

    /**
     * Create an instance of {@link CommandQueryResult.Command }
     * 
     */
    public CommandQueryResult.Command createCommandQueryResultCommand() {
        return new CommandQueryResult.Command();
    }

    /**
     * Create an instance of {@link UploadData.QueryResult }
     * 
     */
    public UploadData.QueryResult createUploadDataQueryResult() {
        return new UploadData.QueryResult();
    }

    /**
     * Create an instance of {@link UploadData.QueryResult.UserInfo }
     * 
     */
    public UploadData.QueryResult.UserInfo createUploadDataQueryResultUserInfo() {
        return new UploadData.QueryResult.UserInfo();
    }

    /**
     * Create an instance of {@link UploadData.QueryResult.UserInfo.Info }
     * 
     */
    public UploadData.QueryResult.UserInfo.Info createUploadDataQueryResultUserInfoInfo() {
        return new UploadData.QueryResult.UserInfo.Info();
    }

    /**
     * Create an instance of {@link UploadData.QueryResult.UserInfo.Info.HouseHoldInfo }
     * 
     */
    public UploadData.QueryResult.UserInfo.Info.HouseHoldInfo createUploadDataQueryResultUserInfoInfoHouseHoldInfo() {
        return new UploadData.QueryResult.UserInfo.Info.HouseHoldInfo();
    }

    /**
     * Create an instance of {@link UploadData.QueryResult.UserInfo.Info.ServiceInfo }
     * 
     */
    public UploadData.QueryResult.UserInfo.Info.ServiceInfo createUploadDataQueryResultUserInfoInfoServiceInfo() {
        return new UploadData.QueryResult.UserInfo.Info.ServiceInfo();
    }

    /**
     * Create an instance of {@link UploadData.QueryResult.UserInfo.Info.ServiceInfo.HousesHoldInfo }
     * 
     */
    public UploadData.QueryResult.UserInfo.Info.ServiceInfo.HousesHoldInfo createUploadDataQueryResultUserInfoInfoServiceInfoHousesHoldInfo() {
        return new UploadData.QueryResult.UserInfo.Info.ServiceInfo.HousesHoldInfo();
    }

    /**
     * Create an instance of {@link UploadData.QueryResult.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans }
     * 
     */
    public UploadData.QueryResult.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans createUploadDataQueryResultUserInfoInfoServiceInfoHousesHoldInfoIPtrans() {
        return new UploadData.QueryResult.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans();
    }

    /**
     * Create an instance of {@link UploadData.QueryResult.HouseInfo }
     * 
     */
    public UploadData.QueryResult.HouseInfo createUploadDataQueryResultHouseInfo() {
        return new UploadData.QueryResult.HouseInfo();
    }

    /**
     * Create an instance of {@link UploadData.DeleteInfo }
     * 
     */
    public UploadData.DeleteInfo createUploadDataDeleteInfo() {
        return new UploadData.DeleteInfo();
    }

    /**
     * Create an instance of {@link UploadData.DeleteInfo.DeleteData }
     * 
     */
    public UploadData.DeleteInfo.DeleteData createUploadDataDeleteInfoDeleteData() {
        return new UploadData.DeleteInfo.DeleteData();
    }

    /**
     * Create an instance of {@link UploadData.DeleteInfo.DeleteData.User }
     * 
     */
    public UploadData.DeleteInfo.DeleteData.User createUploadDataDeleteInfoDeleteDataUser() {
        return new UploadData.DeleteInfo.DeleteData.User();
    }

    /**
     * Create an instance of {@link UploadData.UpdateInfo }
     * 
     */
    public UploadData.UpdateInfo createUploadDataUpdateInfo() {
        return new UploadData.UpdateInfo();
    }

    /**
     * Create an instance of {@link UploadData.UpdateInfo.UpdateData }
     * 
     */
    public UploadData.UpdateInfo.UpdateData createUploadDataUpdateInfoUpdateData() {
        return new UploadData.UpdateInfo.UpdateData();
    }

    /**
     * Create an instance of {@link UploadData.UpdateInfo.UpdateData.UserInfo }
     * 
     */
    public UploadData.UpdateInfo.UpdateData.UserInfo createUploadDataUpdateInfoUpdateDataUserInfo() {
        return new UploadData.UpdateInfo.UpdateData.UserInfo();
    }

    /**
     * Create an instance of {@link UploadData.UpdateInfo.UpdateData.UserInfo.Info }
     * 
     */
    public UploadData.UpdateInfo.UpdateData.UserInfo.Info createUploadDataUpdateInfoUpdateDataUserInfoInfo() {
        return new UploadData.UpdateInfo.UpdateData.UserInfo.Info();
    }

    /**
     * Create an instance of {@link UploadData.UpdateInfo.UpdateData.UserInfo.Info.HouseHoldInfo }
     * 
     */
    public UploadData.UpdateInfo.UpdateData.UserInfo.Info.HouseHoldInfo createUploadDataUpdateInfoUpdateDataUserInfoInfoHouseHoldInfo() {
        return new UploadData.UpdateInfo.UpdateData.UserInfo.Info.HouseHoldInfo();
    }

    /**
     * Create an instance of {@link UploadData.UpdateInfo.UpdateData.UserInfo.Info.ServiceInfo }
     * 
     */
    public UploadData.UpdateInfo.UpdateData.UserInfo.Info.ServiceInfo createUploadDataUpdateInfoUpdateDataUserInfoInfoServiceInfo() {
        return new UploadData.UpdateInfo.UpdateData.UserInfo.Info.ServiceInfo();
    }

    /**
     * Create an instance of {@link UploadData.UpdateInfo.UpdateData.UserInfo.Info.ServiceInfo.HousesHoldInfo }
     * 
     */
    public UploadData.UpdateInfo.UpdateData.UserInfo.Info.ServiceInfo.HousesHoldInfo createUploadDataUpdateInfoUpdateDataUserInfoInfoServiceInfoHousesHoldInfo() {
        return new UploadData.UpdateInfo.UpdateData.UserInfo.Info.ServiceInfo.HousesHoldInfo();
    }

    /**
     * Create an instance of {@link UploadData.UpdateInfo.UpdateData.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans }
     * 
     */
    public UploadData.UpdateInfo.UpdateData.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans createUploadDataUpdateInfoUpdateDataUserInfoInfoServiceInfoHousesHoldInfoIPtrans() {
        return new UploadData.UpdateInfo.UpdateData.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans();
    }

    /**
     * Create an instance of {@link UploadData.UpdateInfo.UpdateData.HouseInfo }
     * 
     */
    public UploadData.UpdateInfo.UpdateData.HouseInfo createUploadDataUpdateInfoUpdateDataHouseInfo() {
        return new UploadData.UpdateInfo.UpdateData.HouseInfo();
    }

    /**
     * Create an instance of {@link UploadData.NewInfo }
     * 
     */
    public UploadData.NewInfo createUploadDataNewInfo() {
        return new UploadData.NewInfo();
    }

    /**
     * Create an instance of {@link UploadData.NewInfo.UserInfo }
     * 
     */
    public UploadData.NewInfo.UserInfo createUploadDataNewInfoUserInfo() {
        return new UploadData.NewInfo.UserInfo();
    }

    /**
     * Create an instance of {@link UploadData.NewInfo.UserInfo.Info }
     * 
     */
    public UploadData.NewInfo.UserInfo.Info createUploadDataNewInfoUserInfoInfo() {
        return new UploadData.NewInfo.UserInfo.Info();
    }

    /**
     * Create an instance of {@link UploadData.NewInfo.UserInfo.Info.HouseHoldInfo }
     * 
     */
    public UploadData.NewInfo.UserInfo.Info.HouseHoldInfo createUploadDataNewInfoUserInfoInfoHouseHoldInfo() {
        return new UploadData.NewInfo.UserInfo.Info.HouseHoldInfo();
    }

    /**
     * Create an instance of {@link UploadData.NewInfo.UserInfo.Info.ServiceInfo }
     * 
     */
    public UploadData.NewInfo.UserInfo.Info.ServiceInfo createUploadDataNewInfoUserInfoInfoServiceInfo() {
        return new UploadData.NewInfo.UserInfo.Info.ServiceInfo();
    }

    /**
     * Create an instance of {@link UploadData.NewInfo.UserInfo.Info.ServiceInfo.HousesHoldInfo }
     * 
     */
    public UploadData.NewInfo.UserInfo.Info.ServiceInfo.HousesHoldInfo createUploadDataNewInfoUserInfoInfoServiceInfoHousesHoldInfo() {
        return new UploadData.NewInfo.UserInfo.Info.ServiceInfo.HousesHoldInfo();
    }

    /**
     * Create an instance of {@link UploadData.NewInfo.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans }
     * 
     */
    public UploadData.NewInfo.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans createUploadDataNewInfoUserInfoInfoServiceInfoHousesHoldInfoIPtrans() {
        return new UploadData.NewInfo.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans();
    }

    /**
     * Create an instance of {@link UploadData.NewInfo.HouseInfo }
     * 
     */
    public UploadData.NewInfo.HouseInfo createUploadDataNewInfoHouseInfo() {
        return new UploadData.NewInfo.HouseInfo();
    }

    /**
     * Create an instance of {@link IdcCommand.Command }
     * 
     */
    public IdcCommand.Command createIdcCommandCommand() {
        return new IdcCommand.Command();
    }

    /**
     * Create an instance of {@link CodeList.Wfwgqk }
     * 
     */
    public CodeList.Wfwgqk createCodeListWfwgqk() {
        return new CodeList.Wfwgqk();
    }

    /**
     * Create an instance of {@link CodeList.Gzlx }
     * 
     */
    public CodeList.Gzlx createCodeListGzlx() {
        return new CodeList.Gzlx();
    }

    /**
     * Create an instance of {@link CodeList.Fwnr }
     * 
     */
    public CodeList.Fwnr createCodeListFwnr() {
        return new CodeList.Fwnr();
    }

    /**
     * Create an instance of {@link CodeList.Dllx }
     * 
     */
    public CodeList.Dllx createCodeListDllx() {
        return new CodeList.Dllx();
    }

    /**
     * Create an instance of {@link CodeList.Jfxz }
     * 
     */
    public CodeList.Jfxz createCodeListJfxz() {
        return new CodeList.Jfxz();
    }

    /**
     * Create an instance of {@link CodeList.Zjlx }
     * 
     */
    public CodeList.Zjlx createCodeListZjlx() {
        return new CodeList.Zjlx();
    }

    /**
     * Create an instance of {@link CodeList.Dwsx }
     * 
     */
    public CodeList.Dwsx createCodeListDwsx() {
        return new CodeList.Dwsx();
    }

    /**
     * Create an instance of {@link CodeList.Jrfs }
     * 
     */
    public CodeList.Jrfs createCodeListJrfs() {
        return new CodeList.Jrfs();
    }

    /**
     * Create an instance of {@link CodeList.Basx }
     * 
     */
    public CodeList.Basx createCodeListBasx() {
        return new CodeList.Basx();
    }

    /**
     * Create an instance of {@link IdcMonitor.HouseMonitor }
     * 
     */
    public IdcMonitor.HouseMonitor createIdcMonitorHouseMonitor() {
        return new IdcMonitor.HouseMonitor();
    }

    /**
     * Create an instance of {@link MonitorResult.Log }
     * 
     */
    public MonitorResult.Log createMonitorResultLog() {
        return new MonitorResult.Log();
    }

    /**
     * Create an instance of {@link IdcCommandAck.CommandAck }
     * 
     */
    public IdcCommandAck.CommandAck createIdcCommandAckCommandAck() {
        return new IdcCommandAck.CommandAck();
    }

    /**
     * Create an instance of {@link LogQuery.SrcIP }
     * 
     */
    public LogQuery.SrcIP createLogQuerySrcIP() {
        return new LogQuery.SrcIP();
    }

    /**
     * Create an instance of {@link LogQuery.DestIP }
     * 
     */
    public LogQuery.DestIP createLogQueryDestIP() {
        return new LogQuery.DestIP();
    }

    /**
     * Create an instance of {@link ActiveState }
     * 
     */
    public ActiveState createActiveState() {
        return new ActiveState();
    }

    /**
     * Create an instance of {@link CommandQuery }
     * 
     */
    public CommandQuery createCommandQuery() {
        return new CommandQuery();
    }

    /**
     * Create an instance of {@link LogQueryResult.Result }
     * 
     */
    public LogQueryResult.Result createLogQueryResultResult() {
        return new LogQueryResult.Result();
    }

    /**
     * Create an instance of {@link LogQueryResult.Log }
     * 
     */
    public LogQueryResult.Log createLogQueryResultLog() {
        return new LogQueryResult.Log();
    }

    /**
     * Create an instance of {@link IdcInfoManage }
     * 
     */
    public IdcInfoManage createIdcInfoManage() {
        return new IdcInfoManage();
    }

    /**
     * Create an instance of {@link Return }
     * 
     */
    public Return createReturn() {
        return new Return();
    }

    /**
     * Create an instance of {@link FilterResult.Log.Attachment }
     * 
     */
    public FilterResult.Log.Attachment createFilterResultLogAttachment() {
        return new FilterResult.Log.Attachment();
    }

    /**
     * Create an instance of {@link CommandQueryResult.Command.Rule }
     * 
     */
    public CommandQueryResult.Command.Rule createCommandQueryResultCommandRule() {
        return new CommandQueryResult.Command.Rule();
    }

    /**
     * Create an instance of {@link CommandQueryResult.Command.Action }
     * 
     */
    public CommandQueryResult.Command.Action createCommandQueryResultCommandAction() {
        return new CommandQueryResult.Command.Action();
    }

    /**
     * Create an instance of {@link CommandQueryResult.Command.Time }
     * 
     */
    public CommandQueryResult.Command.Time createCommandQueryResultCommandTime() {
        return new CommandQueryResult.Command.Time();
    }

    /**
     * Create an instance of {@link CommandQueryResult.Command.Range }
     * 
     */
    public CommandQueryResult.Command.Range createCommandQueryResultCommandRange() {
        return new CommandQueryResult.Command.Range();
    }

    /**
     * Create an instance of {@link CommandQueryResult.Command.Privilege }
     * 
     */
    public CommandQueryResult.Command.Privilege createCommandQueryResultCommandPrivilege() {
        return new CommandQueryResult.Command.Privilege();
    }

    /**
     * Create an instance of {@link UploadData.QueryResult.IdcOfficer }
     * 
     */
    public UploadData.QueryResult.IdcOfficer createUploadDataQueryResultIdcOfficer() {
        return new UploadData.QueryResult.IdcOfficer();
    }

    /**
     * Create an instance of {@link UploadData.QueryResult.EmergencyContact }
     * 
     */
    public UploadData.QueryResult.EmergencyContact createUploadDataQueryResultEmergencyContact() {
        return new UploadData.QueryResult.EmergencyContact();
    }

    /**
     * Create an instance of {@link UploadData.QueryResult.UserInfo.Info.Officer }
     * 
     */
    public UploadData.QueryResult.UserInfo.Info.Officer createUploadDataQueryResultUserInfoInfoOfficer() {
        return new UploadData.QueryResult.UserInfo.Info.Officer();
    }

    /**
     * Create an instance of {@link UploadData.QueryResult.UserInfo.Info.HouseHoldInfo.IPseg }
     * 
     */
    public UploadData.QueryResult.UserInfo.Info.HouseHoldInfo.IPseg createUploadDataQueryResultUserInfoInfoHouseHoldInfoIPseg() {
        return new UploadData.QueryResult.UserInfo.Info.HouseHoldInfo.IPseg();
    }

    /**
     * Create an instance of {@link UploadData.QueryResult.UserInfo.Info.ServiceInfo.Domain }
     * 
     */
    public UploadData.QueryResult.UserInfo.Info.ServiceInfo.Domain createUploadDataQueryResultUserInfoInfoServiceInfoDomain() {
        return new UploadData.QueryResult.UserInfo.Info.ServiceInfo.Domain();
    }

    /**
     * Create an instance of {@link UploadData.QueryResult.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans.InternetIP }
     * 
     */
    public UploadData.QueryResult.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans.InternetIP createUploadDataQueryResultUserInfoInfoServiceInfoHousesHoldInfoIPtransInternetIP() {
        return new UploadData.QueryResult.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans.InternetIP();
    }

    /**
     * Create an instance of {@link UploadData.QueryResult.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans.NetIP }
     * 
     */
    public UploadData.QueryResult.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans.NetIP createUploadDataQueryResultUserInfoInfoServiceInfoHousesHoldInfoIPtransNetIP() {
        return new UploadData.QueryResult.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans.NetIP();
    }

    /**
     * Create an instance of {@link UploadData.QueryResult.HouseInfo.HouseOfficer }
     * 
     */
    public UploadData.QueryResult.HouseInfo.HouseOfficer createUploadDataQueryResultHouseInfoHouseOfficer() {
        return new UploadData.QueryResult.HouseInfo.HouseOfficer();
    }

    /**
     * Create an instance of {@link UploadData.QueryResult.HouseInfo.GatewayInfo }
     * 
     */
    public UploadData.QueryResult.HouseInfo.GatewayInfo createUploadDataQueryResultHouseInfoGatewayInfo() {
        return new UploadData.QueryResult.HouseInfo.GatewayInfo();
    }

    /**
     * Create an instance of {@link UploadData.QueryResult.HouseInfo.IPsegInfo }
     * 
     */
    public UploadData.QueryResult.HouseInfo.IPsegInfo createUploadDataQueryResultHouseInfoIPsegInfo() {
        return new UploadData.QueryResult.HouseInfo.IPsegInfo();
    }

    /**
     * Create an instance of {@link UploadData.DeleteInfo.DeleteData.House }
     * 
     */
    public UploadData.DeleteInfo.DeleteData.House createUploadDataDeleteInfoDeleteDataHouse() {
        return new UploadData.DeleteInfo.DeleteData.House();
    }

    /**
     * Create an instance of {@link UploadData.DeleteInfo.DeleteData.User.Service }
     * 
     */
    public UploadData.DeleteInfo.DeleteData.User.Service createUploadDataDeleteInfoDeleteDataUserService() {
        return new UploadData.DeleteInfo.DeleteData.User.Service();
    }

    /**
     * Create an instance of {@link UploadData.UpdateInfo.IdcOfficer }
     * 
     */
    public UploadData.UpdateInfo.IdcOfficer createUploadDataUpdateInfoIdcOfficer() {
        return new UploadData.UpdateInfo.IdcOfficer();
    }

    /**
     * Create an instance of {@link UploadData.UpdateInfo.EmergencyContact }
     * 
     */
    public UploadData.UpdateInfo.EmergencyContact createUploadDataUpdateInfoEmergencyContact() {
        return new UploadData.UpdateInfo.EmergencyContact();
    }

    /**
     * Create an instance of {@link UploadData.UpdateInfo.UpdateData.UserInfo.Info.Officer }
     * 
     */
    public UploadData.UpdateInfo.UpdateData.UserInfo.Info.Officer createUploadDataUpdateInfoUpdateDataUserInfoInfoOfficer() {
        return new UploadData.UpdateInfo.UpdateData.UserInfo.Info.Officer();
    }

    /**
     * Create an instance of {@link UploadData.UpdateInfo.UpdateData.UserInfo.Info.HouseHoldInfo.IPseg }
     * 
     */
    public UploadData.UpdateInfo.UpdateData.UserInfo.Info.HouseHoldInfo.IPseg createUploadDataUpdateInfoUpdateDataUserInfoInfoHouseHoldInfoIPseg() {
        return new UploadData.UpdateInfo.UpdateData.UserInfo.Info.HouseHoldInfo.IPseg();
    }

    /**
     * Create an instance of {@link UploadData.UpdateInfo.UpdateData.UserInfo.Info.ServiceInfo.Domain }
     * 
     */
    public UploadData.UpdateInfo.UpdateData.UserInfo.Info.ServiceInfo.Domain createUploadDataUpdateInfoUpdateDataUserInfoInfoServiceInfoDomain() {
        return new UploadData.UpdateInfo.UpdateData.UserInfo.Info.ServiceInfo.Domain();
    }

    /**
     * Create an instance of {@link UploadData.UpdateInfo.UpdateData.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans.InternetIP }
     * 
     */
    public UploadData.UpdateInfo.UpdateData.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans.InternetIP createUploadDataUpdateInfoUpdateDataUserInfoInfoServiceInfoHousesHoldInfoIPtransInternetIP() {
        return new UploadData.UpdateInfo.UpdateData.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans.InternetIP();
    }

    /**
     * Create an instance of {@link UploadData.UpdateInfo.UpdateData.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans.NetIP }
     * 
     */
    public UploadData.UpdateInfo.UpdateData.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans.NetIP createUploadDataUpdateInfoUpdateDataUserInfoInfoServiceInfoHousesHoldInfoIPtransNetIP() {
        return new UploadData.UpdateInfo.UpdateData.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans.NetIP();
    }

    /**
     * Create an instance of {@link UploadData.UpdateInfo.UpdateData.HouseInfo.HouseOfficer }
     * 
     */
    public UploadData.UpdateInfo.UpdateData.HouseInfo.HouseOfficer createUploadDataUpdateInfoUpdateDataHouseInfoHouseOfficer() {
        return new UploadData.UpdateInfo.UpdateData.HouseInfo.HouseOfficer();
    }

    /**
     * Create an instance of {@link UploadData.UpdateInfo.UpdateData.HouseInfo.GatewayInfo }
     * 
     */
    public UploadData.UpdateInfo.UpdateData.HouseInfo.GatewayInfo createUploadDataUpdateInfoUpdateDataHouseInfoGatewayInfo() {
        return new UploadData.UpdateInfo.UpdateData.HouseInfo.GatewayInfo();
    }

    /**
     * Create an instance of {@link UploadData.UpdateInfo.UpdateData.HouseInfo.IPsegInfo }
     * 
     */
    public UploadData.UpdateInfo.UpdateData.HouseInfo.IPsegInfo createUploadDataUpdateInfoUpdateDataHouseInfoIPsegInfo() {
        return new UploadData.UpdateInfo.UpdateData.HouseInfo.IPsegInfo();
    }

    /**
     * Create an instance of {@link UploadData.NewInfo.IdcOfficer }
     * 
     */
    public UploadData.NewInfo.IdcOfficer createUploadDataNewInfoIdcOfficer() {
        return new UploadData.NewInfo.IdcOfficer();
    }

    /**
     * Create an instance of {@link UploadData.NewInfo.EmergencyContact }
     * 
     */
    public UploadData.NewInfo.EmergencyContact createUploadDataNewInfoEmergencyContact() {
        return new UploadData.NewInfo.EmergencyContact();
    }

    /**
     * Create an instance of {@link UploadData.NewInfo.UserInfo.Info.Officer }
     * 
     */
    public UploadData.NewInfo.UserInfo.Info.Officer createUploadDataNewInfoUserInfoInfoOfficer() {
        return new UploadData.NewInfo.UserInfo.Info.Officer();
    }

    /**
     * Create an instance of {@link UploadData.NewInfo.UserInfo.Info.HouseHoldInfo.IPseg }
     * 
     */
    public UploadData.NewInfo.UserInfo.Info.HouseHoldInfo.IPseg createUploadDataNewInfoUserInfoInfoHouseHoldInfoIPseg() {
        return new UploadData.NewInfo.UserInfo.Info.HouseHoldInfo.IPseg();
    }

    /**
     * Create an instance of {@link UploadData.NewInfo.UserInfo.Info.ServiceInfo.Domain }
     * 
     */
    public UploadData.NewInfo.UserInfo.Info.ServiceInfo.Domain createUploadDataNewInfoUserInfoInfoServiceInfoDomain() {
        return new UploadData.NewInfo.UserInfo.Info.ServiceInfo.Domain();
    }

    /**
     * Create an instance of {@link UploadData.NewInfo.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans.InternetIP }
     * 
     */
    public UploadData.NewInfo.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans.InternetIP createUploadDataNewInfoUserInfoInfoServiceInfoHousesHoldInfoIPtransInternetIP() {
        return new UploadData.NewInfo.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans.InternetIP();
    }

    /**
     * Create an instance of {@link UploadData.NewInfo.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans.NetIP }
     * 
     */
    public UploadData.NewInfo.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans.NetIP createUploadDataNewInfoUserInfoInfoServiceInfoHousesHoldInfoIPtransNetIP() {
        return new UploadData.NewInfo.UserInfo.Info.ServiceInfo.HousesHoldInfo.IPtrans.NetIP();
    }

    /**
     * Create an instance of {@link UploadData.NewInfo.HouseInfo.HouseOfficer }
     * 
     */
    public UploadData.NewInfo.HouseInfo.HouseOfficer createUploadDataNewInfoHouseInfoHouseOfficer() {
        return new UploadData.NewInfo.HouseInfo.HouseOfficer();
    }

    /**
     * Create an instance of {@link UploadData.NewInfo.HouseInfo.GatewayInfo }
     * 
     */
    public UploadData.NewInfo.HouseInfo.GatewayInfo createUploadDataNewInfoHouseInfoGatewayInfo() {
        return new UploadData.NewInfo.HouseInfo.GatewayInfo();
    }

    /**
     * Create an instance of {@link UploadData.NewInfo.HouseInfo.IPsegInfo }
     * 
     */
    public UploadData.NewInfo.HouseInfo.IPsegInfo createUploadDataNewInfoHouseInfoIPsegInfo() {
        return new UploadData.NewInfo.HouseInfo.IPsegInfo();
    }

    /**
     * Create an instance of {@link IdcCommand.Command.Rule }
     * 
     */
    public IdcCommand.Command.Rule createIdcCommandCommandRule() {
        return new IdcCommand.Command.Rule();
    }

    /**
     * Create an instance of {@link IdcCommand.Command.Action }
     * 
     */
    public IdcCommand.Command.Action createIdcCommandCommandAction() {
        return new IdcCommand.Command.Action();
    }

    /**
     * Create an instance of {@link IdcCommand.Command.Time }
     * 
     */
    public IdcCommand.Command.Time createIdcCommandCommandTime() {
        return new IdcCommand.Command.Time();
    }

    /**
     * Create an instance of {@link IdcCommand.Command.Range }
     * 
     */
    public IdcCommand.Command.Range createIdcCommandCommandRange() {
        return new IdcCommand.Command.Range();
    }

    /**
     * Create an instance of {@link IdcCommand.Command.Privilege }
     * 
     */
    public IdcCommand.Command.Privilege createIdcCommandCommandPrivilege() {
        return new IdcCommand.Command.Privilege();
    }

    /**
     * Create an instance of {@link CodeList.Wfwgqk.WfwgqkXx }
     * 
     */
    public CodeList.Wfwgqk.WfwgqkXx createCodeListWfwgqkWfwgqkXx() {
        return new CodeList.Wfwgqk.WfwgqkXx();
    }

    /**
     * Create an instance of {@link CodeList.Gzlx.GzlxXx }
     * 
     */
    public CodeList.Gzlx.GzlxXx createCodeListGzlxGzlxXx() {
        return new CodeList.Gzlx.GzlxXx();
    }

    /**
     * Create an instance of {@link CodeList.Fwnr.FwnrXx }
     * 
     */
    public CodeList.Fwnr.FwnrXx createCodeListFwnrFwnrXx() {
        return new CodeList.Fwnr.FwnrXx();
    }

    /**
     * Create an instance of {@link CodeList.Dllx.DllxXx }
     * 
     */
    public CodeList.Dllx.DllxXx createCodeListDllxDllxXx() {
        return new CodeList.Dllx.DllxXx();
    }

    /**
     * Create an instance of {@link CodeList.Jfxz.JfxzXx }
     * 
     */
    public CodeList.Jfxz.JfxzXx createCodeListJfxzJfxzXx() {
        return new CodeList.Jfxz.JfxzXx();
    }

    /**
     * Create an instance of {@link CodeList.Zjlx.ZjlxXx }
     * 
     */
    public CodeList.Zjlx.ZjlxXx createCodeListZjlxZjlxXx() {
        return new CodeList.Zjlx.ZjlxXx();
    }

    /**
     * Create an instance of {@link CodeList.Dwsx.DwsxXx }
     * 
     */
    public CodeList.Dwsx.DwsxXx createCodeListDwsxDwsxXx() {
        return new CodeList.Dwsx.DwsxXx();
    }

    /**
     * Create an instance of {@link CodeList.Jrfs.JrfsXx }
     * 
     */
    public CodeList.Jrfs.JrfsXx createCodeListJrfsJrfsXx() {
        return new CodeList.Jrfs.JrfsXx();
    }

    /**
     * Create an instance of {@link CodeList.Basx.BasxXx }
     * 
     */
    public CodeList.Basx.BasxXx createCodeListBasxBasxXx() {
        return new CodeList.Basx.BasxXx();
    }

    /**
     * Create an instance of {@link IdcMonitor.HouseMonitor.IllegalInfo }
     * 
     */
    public IdcMonitor.HouseMonitor.IllegalInfo createIdcMonitorHouseMonitorIllegalInfo() {
        return new IdcMonitor.HouseMonitor.IllegalInfo();
    }

    /**
     * Create an instance of {@link IdcMonitor.HouseMonitor.IPinfo }
     * 
     */
    public IdcMonitor.HouseMonitor.IPinfo createIdcMonitorHouseMonitorIPinfo() {
        return new IdcMonitor.HouseMonitor.IPinfo();
    }

    /**
     * Create an instance of {@link MonitorResult.Log.Attachment }
     * 
     */
    public MonitorResult.Log.Attachment createMonitorResultLogAttachment() {
        return new MonitorResult.Log.Attachment();
    }

}
