//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.2.8-b130911.1802 生成的
// 请访问 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2021.08.18 时间 11:04:46 AM CST 
//


package com.surfilter.ismi.xmlbeans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idcCommandID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="idcID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="commandAck">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="commandID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="resultCode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="msgInfo" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="timeStamp" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idcCommandID",
    "idcID",
    "commandAck",
    "timeStamp"
})
@XmlRootElement(name = "idcCommandAck")
public class IdcCommandAck {

    @XmlElement(required = true)
    protected Object idcCommandID;
    @XmlElement(required = true)
    protected Object idcID;
    @XmlElement(required = true)
    protected CommandAck commandAck;
    @XmlElement(required = true)
    protected Object timeStamp;

    /**
     * 获取idcCommandID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getIdcCommandID() {
        return idcCommandID;
    }

    /**
     * 设置idcCommandID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setIdcCommandID(Object value) {
        this.idcCommandID = value;
    }

    /**
     * 获取idcID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getIdcID() {
        return idcID;
    }

    /**
     * 设置idcID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setIdcID(Object value) {
        this.idcID = value;
    }

    /**
     * 获取commandAck属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CommandAck }
     *     
     */
    public CommandAck getCommandAck() {
        return commandAck;
    }

    /**
     * 设置commandAck属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CommandAck }
     *     
     */
    public void setCommandAck(CommandAck value) {
        this.commandAck = value;
    }

    /**
     * 获取timeStamp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getTimeStamp() {
        return timeStamp;
    }

    /**
     * 设置timeStamp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setTimeStamp(Object value) {
        this.timeStamp = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="houseID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="commandID" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="resultCode" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="msgInfo" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "houseID",
        "commandID",
        "type",
        "resultCode",
        "msgInfo"
    })
    public static class CommandAck {

        @XmlElement(required = true)
        protected Object houseID;
        @XmlElement(required = true)
        protected Object commandID;
        @XmlElement(required = true)
        protected Object type;
        @XmlElement(required = true)
        protected Object resultCode;
        protected Object msgInfo;

        /**
         * 获取houseID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getHouseID() {
            return houseID;
        }

        /**
         * 设置houseID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setHouseID(Object value) {
            this.houseID = value;
        }

        /**
         * 获取commandID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getCommandID() {
            return commandID;
        }

        /**
         * 设置commandID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setCommandID(Object value) {
            this.commandID = value;
        }

        /**
         * 获取type属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getType() {
            return type;
        }

        /**
         * 设置type属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setType(Object value) {
            this.type = value;
        }

        /**
         * 获取resultCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getResultCode() {
            return resultCode;
        }

        /**
         * 设置resultCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setResultCode(Object value) {
            this.resultCode = value;
        }

        /**
         * 获取msgInfo属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getMsgInfo() {
            return msgInfo;
        }

        /**
         * 设置msgInfo属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setMsgInfo(Object value) {
            this.msgInfo = value;
        }

    }

}
