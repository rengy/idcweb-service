//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.2.8-b130911.1802 生成的
// 请访问 <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2021.08.18 时间 11:04:46 AM CST 
//


package com.surfilter.ismi.xmlbeans;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="commandID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="basx" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="basx_xx" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="jrfs" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="jrfs_xx" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="dwsx" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="dwsx_xx" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="zjlx" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="zjlx_xx" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="jfxz" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="jfxz_xx" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="dllx" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="dllx_xx" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="fwnr" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="fwnr_xx" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="fl" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="gzlx" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="gzlx_xx" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="wfwgqk" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="wfwgqk_xx" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="timeStamp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "commandID",
    "basx",
    "jrfs",
    "dwsx",
    "zjlx",
    "jfxz",
    "dllx",
    "fwnr",
    "gzlx",
    "wfwgqk",
    "timeStamp"
})
@XmlRootElement(name = "codeList")
public class CodeList {

    protected long commandID;
    protected Basx basx;
    protected Jrfs jrfs;
    protected Dwsx dwsx;
    protected Zjlx zjlx;
    protected Jfxz jfxz;
    protected Dllx dllx;
    protected Fwnr fwnr;
    protected Gzlx gzlx;
    protected Wfwgqk wfwgqk;
    @XmlElement(required = true)
    protected String timeStamp;

    /**
     * 获取commandID属性的值。
     * 
     */
    public long getCommandID() {
        return commandID;
    }

    /**
     * 设置commandID属性的值。
     * 
     */
    public void setCommandID(long value) {
        this.commandID = value;
    }

    /**
     * 获取basx属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Basx }
     *     
     */
    public Basx getBasx() {
        return basx;
    }

    /**
     * 设置basx属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Basx }
     *     
     */
    public void setBasx(Basx value) {
        this.basx = value;
    }

    /**
     * 获取jrfs属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Jrfs }
     *     
     */
    public Jrfs getJrfs() {
        return jrfs;
    }

    /**
     * 设置jrfs属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Jrfs }
     *     
     */
    public void setJrfs(Jrfs value) {
        this.jrfs = value;
    }

    /**
     * 获取dwsx属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Dwsx }
     *     
     */
    public Dwsx getDwsx() {
        return dwsx;
    }

    /**
     * 设置dwsx属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Dwsx }
     *     
     */
    public void setDwsx(Dwsx value) {
        this.dwsx = value;
    }

    /**
     * 获取zjlx属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Zjlx }
     *     
     */
    public Zjlx getZjlx() {
        return zjlx;
    }

    /**
     * 设置zjlx属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Zjlx }
     *     
     */
    public void setZjlx(Zjlx value) {
        this.zjlx = value;
    }

    /**
     * 获取jfxz属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Jfxz }
     *     
     */
    public Jfxz getJfxz() {
        return jfxz;
    }

    /**
     * 设置jfxz属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Jfxz }
     *     
     */
    public void setJfxz(Jfxz value) {
        this.jfxz = value;
    }

    /**
     * 获取dllx属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Dllx }
     *     
     */
    public Dllx getDllx() {
        return dllx;
    }

    /**
     * 设置dllx属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Dllx }
     *     
     */
    public void setDllx(Dllx value) {
        this.dllx = value;
    }

    /**
     * 获取fwnr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Fwnr }
     *     
     */
    public Fwnr getFwnr() {
        return fwnr;
    }

    /**
     * 设置fwnr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Fwnr }
     *     
     */
    public void setFwnr(Fwnr value) {
        this.fwnr = value;
    }

    /**
     * 获取gzlx属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Gzlx }
     *     
     */
    public Gzlx getGzlx() {
        return gzlx;
    }

    /**
     * 设置gzlx属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Gzlx }
     *     
     */
    public void setGzlx(Gzlx value) {
        this.gzlx = value;
    }

    /**
     * 获取wfwgqk属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Wfwgqk }
     *     
     */
    public Wfwgqk getWfwgqk() {
        return wfwgqk;
    }

    /**
     * 设置wfwgqk属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Wfwgqk }
     *     
     */
    public void setWfwgqk(Wfwgqk value) {
        this.wfwgqk = value;
    }

    /**
     * 获取timeStamp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     * 设置timeStamp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeStamp(String value) {
        this.timeStamp = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="basx_xx" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "basxXx"
    })
    public static class Basx {

        @XmlElement(name = "basx_xx", required = true)
        protected List<BasxXx> basxXx;

        /**
         * Gets the value of the basxXx property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the basxXx property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBasxXx().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BasxXx }
         * 
         * 
         */
        public List<BasxXx> getBasxXx() {
            if (basxXx == null) {
                basxXx = new ArrayList<BasxXx>();
            }
            return this.basxXx;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "id",
            "mc",
            "bz",
            "sfyx"
        })
        public static class BasxXx {

            @XmlElement(name = "ID")
            protected long id;
            @XmlElement(required = true)
            protected String mc;
            @XmlElement(required = true)
            protected String bz;
            protected long sfyx;

            /**
             * 获取id属性的值。
             * 
             */
            public long getID() {
                return id;
            }

            /**
             * 设置id属性的值。
             * 
             */
            public void setID(long value) {
                this.id = value;
            }

            /**
             * 获取mc属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMc() {
                return mc;
            }

            /**
             * 设置mc属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMc(String value) {
                this.mc = value;
            }

            /**
             * 获取bz属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBz() {
                return bz;
            }

            /**
             * 设置bz属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBz(String value) {
                this.bz = value;
            }

            /**
             * 获取sfyx属性的值。
             * 
             */
            public long getSfyx() {
                return sfyx;
            }

            /**
             * 设置sfyx属性的值。
             * 
             */
            public void setSfyx(long value) {
                this.sfyx = value;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="dllx_xx" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dllxXx"
    })
    public static class Dllx {

        @XmlElement(name = "dllx_xx", required = true)
        protected List<DllxXx> dllxXx;

        /**
         * Gets the value of the dllxXx property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the dllxXx property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDllxXx().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DllxXx }
         * 
         * 
         */
        public List<DllxXx> getDllxXx() {
            if (dllxXx == null) {
                dllxXx = new ArrayList<DllxXx>();
            }
            return this.dllxXx;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "id",
            "mc",
            "bz",
            "sfyx"
        })
        public static class DllxXx {

            @XmlElement(name = "ID")
            protected long id;
            @XmlElement(required = true)
            protected String mc;
            @XmlElement(required = true)
            protected String bz;
            protected long sfyx;

            /**
             * 获取id属性的值。
             * 
             */
            public long getID() {
                return id;
            }

            /**
             * 设置id属性的值。
             * 
             */
            public void setID(long value) {
                this.id = value;
            }

            /**
             * 获取mc属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMc() {
                return mc;
            }

            /**
             * 设置mc属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMc(String value) {
                this.mc = value;
            }

            /**
             * 获取bz属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBz() {
                return bz;
            }

            /**
             * 设置bz属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBz(String value) {
                this.bz = value;
            }

            /**
             * 获取sfyx属性的值。
             * 
             */
            public long getSfyx() {
                return sfyx;
            }

            /**
             * 设置sfyx属性的值。
             * 
             */
            public void setSfyx(long value) {
                this.sfyx = value;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="dwsx_xx" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dwsxXx"
    })
    public static class Dwsx {

        @XmlElement(name = "dwsx_xx", required = true)
        protected List<DwsxXx> dwsxXx;

        /**
         * Gets the value of the dwsxXx property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the dwsxXx property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDwsxXx().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DwsxXx }
         * 
         * 
         */
        public List<DwsxXx> getDwsxXx() {
            if (dwsxXx == null) {
                dwsxXx = new ArrayList<DwsxXx>();
            }
            return this.dwsxXx;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "id",
            "mc",
            "bz",
            "sfyx"
        })
        public static class DwsxXx {

            @XmlElement(name = "ID")
            protected long id;
            @XmlElement(required = true)
            protected String mc;
            @XmlElement(required = true)
            protected String bz;
            protected long sfyx;

            /**
             * 获取id属性的值。
             * 
             */
            public long getID() {
                return id;
            }

            /**
             * 设置id属性的值。
             * 
             */
            public void setID(long value) {
                this.id = value;
            }

            /**
             * 获取mc属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMc() {
                return mc;
            }

            /**
             * 设置mc属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMc(String value) {
                this.mc = value;
            }

            /**
             * 获取bz属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBz() {
                return bz;
            }

            /**
             * 设置bz属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBz(String value) {
                this.bz = value;
            }

            /**
             * 获取sfyx属性的值。
             * 
             */
            public long getSfyx() {
                return sfyx;
            }

            /**
             * 设置sfyx属性的值。
             * 
             */
            public void setSfyx(long value) {
                this.sfyx = value;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="fwnr_xx" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="fl" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fwnrXx"
    })
    public static class Fwnr {

        @XmlElement(name = "fwnr_xx", required = true)
        protected List<FwnrXx> fwnrXx;

        /**
         * Gets the value of the fwnrXx property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the fwnrXx property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFwnrXx().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link FwnrXx }
         * 
         * 
         */
        public List<FwnrXx> getFwnrXx() {
            if (fwnrXx == null) {
                fwnrXx = new ArrayList<FwnrXx>();
            }
            return this.fwnrXx;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="fl" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "id",
            "mc",
            "fl",
            "bz",
            "sfyx"
        })
        public static class FwnrXx {

            @XmlElement(name = "ID")
            protected long id;
            @XmlElement(required = true)
            protected String mc;
            protected long fl;
            @XmlElement(required = true)
            protected String bz;
            protected long sfyx;

            /**
             * 获取id属性的值。
             * 
             */
            public long getID() {
                return id;
            }

            /**
             * 设置id属性的值。
             * 
             */
            public void setID(long value) {
                this.id = value;
            }

            /**
             * 获取mc属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMc() {
                return mc;
            }

            /**
             * 设置mc属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMc(String value) {
                this.mc = value;
            }

            /**
             * 获取fl属性的值。
             * 
             */
            public long getFl() {
                return fl;
            }

            /**
             * 设置fl属性的值。
             * 
             */
            public void setFl(long value) {
                this.fl = value;
            }

            /**
             * 获取bz属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBz() {
                return bz;
            }

            /**
             * 设置bz属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBz(String value) {
                this.bz = value;
            }

            /**
             * 获取sfyx属性的值。
             * 
             */
            public long getSfyx() {
                return sfyx;
            }

            /**
             * 设置sfyx属性的值。
             * 
             */
            public void setSfyx(long value) {
                this.sfyx = value;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="gzlx_xx" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "gzlxXx"
    })
    public static class Gzlx {

        @XmlElement(name = "gzlx_xx", required = true)
        protected List<GzlxXx> gzlxXx;

        /**
         * Gets the value of the gzlxXx property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the gzlxXx property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGzlxXx().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GzlxXx }
         * 
         * 
         */
        public List<GzlxXx> getGzlxXx() {
            if (gzlxXx == null) {
                gzlxXx = new ArrayList<GzlxXx>();
            }
            return this.gzlxXx;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "id",
            "mc",
            "bz",
            "sfyx"
        })
        public static class GzlxXx {

            @XmlElement(name = "ID")
            protected long id;
            @XmlElement(required = true)
            protected String mc;
            @XmlElement(required = true)
            protected String bz;
            protected long sfyx;

            /**
             * 获取id属性的值。
             * 
             */
            public long getID() {
                return id;
            }

            /**
             * 设置id属性的值。
             * 
             */
            public void setID(long value) {
                this.id = value;
            }

            /**
             * 获取mc属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMc() {
                return mc;
            }

            /**
             * 设置mc属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMc(String value) {
                this.mc = value;
            }

            /**
             * 获取bz属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBz() {
                return bz;
            }

            /**
             * 设置bz属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBz(String value) {
                this.bz = value;
            }

            /**
             * 获取sfyx属性的值。
             * 
             */
            public long getSfyx() {
                return sfyx;
            }

            /**
             * 设置sfyx属性的值。
             * 
             */
            public void setSfyx(long value) {
                this.sfyx = value;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="jfxz_xx" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "jfxzXx"
    })
    public static class Jfxz {

        @XmlElement(name = "jfxz_xx", required = true)
        protected List<JfxzXx> jfxzXx;

        /**
         * Gets the value of the jfxzXx property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the jfxzXx property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getJfxzXx().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link JfxzXx }
         * 
         * 
         */
        public List<JfxzXx> getJfxzXx() {
            if (jfxzXx == null) {
                jfxzXx = new ArrayList<JfxzXx>();
            }
            return this.jfxzXx;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "id",
            "mc",
            "bz",
            "sfyx"
        })
        public static class JfxzXx {

            @XmlElement(name = "ID")
            protected long id;
            @XmlElement(required = true)
            protected String mc;
            @XmlElement(required = true)
            protected String bz;
            protected long sfyx;

            /**
             * 获取id属性的值。
             * 
             */
            public long getID() {
                return id;
            }

            /**
             * 设置id属性的值。
             * 
             */
            public void setID(long value) {
                this.id = value;
            }

            /**
             * 获取mc属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMc() {
                return mc;
            }

            /**
             * 设置mc属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMc(String value) {
                this.mc = value;
            }

            /**
             * 获取bz属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBz() {
                return bz;
            }

            /**
             * 设置bz属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBz(String value) {
                this.bz = value;
            }

            /**
             * 获取sfyx属性的值。
             * 
             */
            public long getSfyx() {
                return sfyx;
            }

            /**
             * 设置sfyx属性的值。
             * 
             */
            public void setSfyx(long value) {
                this.sfyx = value;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="jrfs_xx" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "jrfsXx"
    })
    public static class Jrfs {

        @XmlElement(name = "jrfs_xx", required = true)
        protected List<JrfsXx> jrfsXx;

        /**
         * Gets the value of the jrfsXx property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the jrfsXx property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getJrfsXx().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link JrfsXx }
         * 
         * 
         */
        public List<JrfsXx> getJrfsXx() {
            if (jrfsXx == null) {
                jrfsXx = new ArrayList<JrfsXx>();
            }
            return this.jrfsXx;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "id",
            "mc",
            "bz",
            "sfyx"
        })
        public static class JrfsXx {

            @XmlElement(name = "ID")
            protected long id;
            @XmlElement(required = true)
            protected String mc;
            @XmlElement(required = true)
            protected String bz;
            protected long sfyx;

            /**
             * 获取id属性的值。
             * 
             */
            public long getID() {
                return id;
            }

            /**
             * 设置id属性的值。
             * 
             */
            public void setID(long value) {
                this.id = value;
            }

            /**
             * 获取mc属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMc() {
                return mc;
            }

            /**
             * 设置mc属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMc(String value) {
                this.mc = value;
            }

            /**
             * 获取bz属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBz() {
                return bz;
            }

            /**
             * 设置bz属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBz(String value) {
                this.bz = value;
            }

            /**
             * 获取sfyx属性的值。
             * 
             */
            public long getSfyx() {
                return sfyx;
            }

            /**
             * 设置sfyx属性的值。
             * 
             */
            public void setSfyx(long value) {
                this.sfyx = value;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="wfwgqk_xx" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "wfwgqkXx"
    })
    public static class Wfwgqk {

        @XmlElement(name = "wfwgqk_xx", required = true)
        protected List<WfwgqkXx> wfwgqkXx;

        /**
         * Gets the value of the wfwgqkXx property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the wfwgqkXx property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getWfwgqkXx().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link WfwgqkXx }
         * 
         * 
         */
        public List<WfwgqkXx> getWfwgqkXx() {
            if (wfwgqkXx == null) {
                wfwgqkXx = new ArrayList<WfwgqkXx>();
            }
            return this.wfwgqkXx;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "id",
            "mc",
            "bz",
            "sfyx"
        })
        public static class WfwgqkXx {

            @XmlElement(name = "ID")
            protected long id;
            @XmlElement(required = true)
            protected String mc;
            @XmlElement(required = true)
            protected String bz;
            protected long sfyx;

            /**
             * 获取id属性的值。
             * 
             */
            public long getID() {
                return id;
            }

            /**
             * 设置id属性的值。
             * 
             */
            public void setID(long value) {
                this.id = value;
            }

            /**
             * 获取mc属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMc() {
                return mc;
            }

            /**
             * 设置mc属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMc(String value) {
                this.mc = value;
            }

            /**
             * 获取bz属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBz() {
                return bz;
            }

            /**
             * 设置bz属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBz(String value) {
                this.bz = value;
            }

            /**
             * 获取sfyx属性的值。
             * 
             */
            public long getSfyx() {
                return sfyx;
            }

            /**
             * 设置sfyx属性的值。
             * 
             */
            public void setSfyx(long value) {
                this.sfyx = value;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="zjlx_xx" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "zjlxXx"
    })
    public static class Zjlx {

        @XmlElement(name = "zjlx_xx", required = true)
        protected List<ZjlxXx> zjlxXx;

        /**
         * Gets the value of the zjlxXx property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the zjlxXx property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getZjlxXx().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ZjlxXx }
         * 
         * 
         */
        public List<ZjlxXx> getZjlxXx() {
            if (zjlxXx == null) {
                zjlxXx = new ArrayList<ZjlxXx>();
            }
            return this.zjlxXx;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="mc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="bz" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="sfyx" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "id",
            "mc",
            "bz",
            "sfyx"
        })
        public static class ZjlxXx {

            @XmlElement(name = "ID")
            protected long id;
            @XmlElement(required = true)
            protected String mc;
            @XmlElement(required = true)
            protected String bz;
            protected long sfyx;

            /**
             * 获取id属性的值。
             * 
             */
            public long getID() {
                return id;
            }

            /**
             * 设置id属性的值。
             * 
             */
            public void setID(long value) {
                this.id = value;
            }

            /**
             * 获取mc属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMc() {
                return mc;
            }

            /**
             * 设置mc属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMc(String value) {
                this.mc = value;
            }

            /**
             * 获取bz属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBz() {
                return bz;
            }

            /**
             * 设置bz属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBz(String value) {
                this.bz = value;
            }

            /**
             * 获取sfyx属性的值。
             * 
             */
            public long getSfyx() {
                return sfyx;
            }

            /**
             * 设置sfyx属性的值。
             * 
             */
            public void setSfyx(long value) {
                this.sfyx = value;
            }

        }

    }

}
