package com.surfilter.ismi.controller;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.surfilter.ismi.client.WSClientService;
import com.surfilter.ismi.service.BizService;
import com.surfilter.ismi.service.DataUploadService;
import com.surfilter.ismi.util.ExceptionPrintUtils;
import com.surfilter.ismi.util._DateUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(path = "/test")
public class TestController {

    private DataUploadService dataUploadService;

    @Autowired
    private BizService  bizService;
    
    @Autowired
    private WSClientService wSClientService;
    @RequestMapping(path = "/ack")
    @ResponseBody
    public String ack() {
    	try {
			wSClientService.idc_commandack(1L,"a");
		} catch (Exception e) {
			return ExceptionPrintUtils.getTrace(e);
		}
    	return "ok";
    }
    
    @RequestMapping(path = "/upload2")
    @ResponseBody
    public String upload2() {
        //http://101.200.148.31:8080/test/upload?name=9
        
        //http://10.10.10.3:8080/test/upload?name=9
        	
        //UploadData uploadData = new UploadData();
        //UploadData.NewInfo newInfo = new UploadData.NewInfo();
        //newInfo.setIdcID("1111");
        //newInfo.setIdcName("idc名称");
        //uploadData.setNewInfo(newInfo);

		//String type=FilenameUtils.getBaseName(newFile.getName());
        try {
//            dataUploadService.doUpload(xmlMapper.writeValueAsString(uploadData), "1", "2021-08-18");
        	String uploadXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><fileLoad><idcId>11234</idcId><dataUpload>593suaWuc1XQWyCkt+sYc50ydkWUmjm/2yYiSuqp44t2VdzHhL8+41BI2xIny2xHCkjEf2YW2hGmUEJp7fHY9JNaq5SAKP6bRNNaRwxBSVWj8pjQxwpQ6ul/HUeZhdL2BZb+lfLbwjTux3ILm6YTB6PZc10MRg6CF4oD6Xlqd0bKUNrLPBvVOSOTKg95xkPL</dataUpload><encryptAlgorithm>1</encryptAlgorithm><compressionFormat>1</compressionFormat><hashAlgorithm>1</hashAlgorithm><dataHash>ZTExMTExY2Y3NjRmMWQxNzhhYmVkMDk3M2MyNmRjYWU=</dataHash><commandVersion>v2.0</commandVersion></fileLoad>";
        	
        	log.info("上传到ftp服务器的数据：：{}",uploadXml);
        	dataUploadService.doUploadFtp(uploadXml, "9", "2021-08-19");
        } catch (Exception e) {
        	return ExceptionPrintUtils.getTrace(e);
        }
        return null;

    }
    
    @RequestMapping(path = "/upload", method = RequestMethod.GET)
    @ResponseBody
    public String upload(@RequestParam(value="name") String name,@RequestParam(value="idcId") String idcId) {
        log.info("test");
        //http://101.200.148.31:8080/test/upload?name=9
        
        //http://10.10.10.3:8080/test/upload?name=9
        	
        //UploadData uploadData = new UploadData();
        //UploadData.NewInfo newInfo = new UploadData.NewInfo();
        //newInfo.setIdcID("1111");
        //newInfo.setIdcName("idc名称");
        //uploadData.setNewInfo(newInfo);

        ApplicationHome home=new ApplicationHome();
		
		File file=home.getSource().getParentFile();
		log.info(file.getAbsolutePath());
		File p=new File(file,"xml");
		File newFile=new File(p,name+".xml");
		//String type=FilenameUtils.getBaseName(newFile.getName());
        try {
        	String data=FileUtils.readFileToString(newFile, StandardCharsets.UTF_8);
//            dataUploadService.doUpload(xmlMapper.writeValueAsString(uploadData), "1", "2021-08-18");
        	String uploadXml = bizService.genFileUploadXml(idcId,data);
        	log.info("上传到ftp服务器的数据：：{}",uploadXml);
        	String ymd=_DateUtils.formatDate(new Date(), null, "yyyy-MM-dd");
        	dataUploadService.doUploadFtp(uploadXml, name, ymd);
        } catch (Exception e){
        	return ExceptionPrintUtils.getTrace(e);
        }
        return null;

    }

    /* ---------------------------------------  我是美丽的分割线  --------------------------------------- */


    @Autowired
    public void setDataUploadService(DataUploadService dataUploadService) {
        this.dataUploadService = dataUploadService;
    }
}
