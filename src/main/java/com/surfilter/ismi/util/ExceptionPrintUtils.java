package com.surfilter.ismi.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionPrintUtils {
	
	public static String getTrace(Throwable error) {
		StringWriter stackTrace = new StringWriter();
		error.printStackTrace(new PrintWriter(stackTrace));
		stackTrace.flush();
		return stackTrace.toString();
	}
}
