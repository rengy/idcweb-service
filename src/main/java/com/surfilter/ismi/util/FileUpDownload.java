package com.surfilter.ismi.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.FTPClient;

import lombok.extern.slf4j.Slf4j;
/**
 * 
 * FTP文件上传下载实现,调用前请确认文件上传下载的目录结构配置正确(ftpconfig.properties),
 * 目前支持单一、同级目录批量文件上传下载
 */
@Slf4j
public class FileUpDownload {
	
	/**
	 * 
	 * @param client
	 * @return 返回连接状态
	 */
	public static boolean ftpConnection(FTPClient client,String ftpIp,String ftpUser,String ftpPwd){//
		boolean flag = false;
		//String FTPIp = bundle.getString("ftpip");
		//String FTPUser = bundle.getString("ftpuser");
		//String FTPPassword = bundle.getString("ftppassword");
		try {
			client.connect(ftpIp);
			log.info("ftp链接成功");
			} catch (SocketException e) {
				log.info("ftpIP异常");
			} catch (IOException e) {
				e.printStackTrace();
			}
		try {
				client.login(ftpUser, ftpPwd);
				flag = true;
				log.info("ftp用户名密码验证通过");
			} catch (IOException e) {
				e.printStackTrace();
			}
		return flag;
	}
	
	/**
	 * 文件上传
	 * @return 上传结束的状态
	 */
	public static boolean fileUpload(String uploadRemotePath,String uploadLocalPath,String ftpIp,String ftpUser,String ftpPwd){
		boolean flag = false;
		FTPClient ftpClient = new FTPClient(); 
		FileInputStream in = null;
		//String uploadRemotePath = bundle.getString("uploadremotepath");
		//String uploadLocalPath = bundle.getString("uploadlocalpath");
		//String ftpPath = bundle.getString("ftppath");
		if((!"".equals(uploadRemotePath)&&null!=uploadRemotePath)&&(!"".equals(uploadLocalPath)&&null!=uploadLocalPath)){
			if(FileUpDownload.ftpConnection(ftpClient, ftpIp, ftpUser, ftpPwd)){
		        try {
		            File srcFile = new File(uploadLocalPath);
		            String fileName = srcFile.getName();
		            if(srcFile.isFile()){
		            	in = new FileInputStream(srcFile); 
		            	ftpClient.changeWorkingDirectory(uploadRemotePath); 
		            	ftpClient.setBufferSize(1024); 
		            	ftpClient.setControlEncoding("UTF-8");
		            	ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE); 
		            	ftpClient.storeFile(fileName, in);
		            	log.info("文件["+srcFile+File.separator+fileName+"]已上传至["+uploadRemotePath+File.separator+fileName+"]");
		            	flag = true;
		            }else{
		            	String fileNames[] = srcFile.list();
		            	String fileDir = srcFile.getName();
//		            	File file = new File(File.separator+uploadRemotePath+File.separator+fileDir);
//		            	if(!file.exists()){
//		            		file.mkdir();
//		            	}
		            	for(int i = 0; i<fileNames.length;i++){
		            		if(new File(uploadLocalPath+File.separator+fileNames[i]).isFile()){
		            			in = new FileInputStream(uploadLocalPath+File.separator+fileNames[i]); 
		            			ftpClient.changeWorkingDirectory(uploadRemotePath); 
		            			ftpClient.setBufferSize(1024); 
		            			ftpClient.setControlEncoding("UTF-8"); 
		            			ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE); 
		            			ftpClient.storeFile(fileNames[i], in);
		            			log.info("文件["+uploadLocalPath+File.separator+fileNames[i]+"]已上传至["+uploadRemotePath+File.separator+fileNames[i]+"]");
		            		}
		            	}
		            	flag = true;
		            }
		            log.info("上传完毕！");
		        } catch (IOException e) { 
		            e.printStackTrace(); 
		            throw new RuntimeException("FTP客户端出错！", e); 
		        } finally { 
		            IOUtils.closeQuietly(in); 
		            try { 
		                ftpClient.disconnect(); 
		            } catch (IOException e) { 
		                e.printStackTrace(); 
		                throw new RuntimeException("关闭FTP连接发生异常！", e); 
		            } 
		        } 
			}
		}else{
			log.info("请检查配置文件中上传下载的路径是否配置合法！");
		}
		return flag;
	}
	/**
	 * FTP文件下载
	 * @return 文件下载完毕的状态
	 */
	public static boolean fileDownload(String ftpIp,String ftpUser,String ftpPwd,String downloadRemotePath,String downloadLocalPath){
		boolean flag = false;
		FTPClient ftpClient = new FTPClient();
		FileOutputStream out = null;
		//String downloadRemotePath = bundle.getString("downloadremotepath");
		//String downloadLocalPath = bundle.getString("downloadlocalpath");
		//String ftpPath = bundle.getString("ftppath");
		//String realPath = ftpPath+File.separator+downloadRemotePath;
		if((!"".equals(downloadRemotePath)&&null!=downloadRemotePath)&&(!"".equals(downloadLocalPath)&&null!=downloadLocalPath)){
			if(FileUpDownload.ftpConnection(ftpClient, ftpIp, ftpUser, ftpPwd)){
				 try {
					 	File scrFile = new File(downloadRemotePath);
					 	if(scrFile.isFile()){
					 		out = new FileOutputStream(downloadLocalPath+File.separator+scrFile.getName()); 
				            ftpClient.setBufferSize(1024); 
				            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE); 
				            ftpClient.retrieveFile(downloadRemotePath, out); 
				            flag = true;
				            log.info("文件["+downloadRemotePath+File.separator+scrFile.getName()+"]已下载到["+downloadLocalPath+File.separator+scrFile.getName()+"]");
				           
			           }else{
			        	   String []fileNames = scrFile.list();
			        	   String fileDir = downloadLocalPath+File.separator+scrFile.getName();
			        	   File file = new File(fileDir);
			        	   if(!file.exists()){
			        		   file.mkdir();
			        	   }
			        	   for(int i=0;i<fileNames.length;i++){
			        		   	out = new FileOutputStream(fileDir+File.separator+fileNames[i]); 
					            ftpClient.setBufferSize(1024); 
					            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE); 
					            ftpClient.retrieveFile(downloadRemotePath+File.separator+fileNames[i], out); 
					            log.info("文件["+downloadRemotePath+File.separator+fileNames[i]+"]已下载到["+fileDir+File.separator+fileNames[i]+"]");
			        	   }
			        	   flag = true;
			           }
					 log.info("下载完毕！");
			        } catch (IOException e) {
			        	e.printStackTrace(); 
			            throw new RuntimeException("FTP客户端出错！", e); 
			        } finally {
			        		IOUtils.closeQuietly(out);
			            try {
			            	ftpClient.disconnect(); 
			            } catch (IOException e) {
			            	e.printStackTrace(); 
			                throw new RuntimeException("关闭FTP连接发生异常！", e); 
			            }
			        }
			}
		}else{
			log.info("请检查配置文件中上传下载的路径是否配置合法!");
		}
		 return flag;
	}
	/**
	public static Map<String ,String> getMessage(String key){
		Map<String ,String> map = null;
		if(!"".equals(key)&&null!=key){
			map = new LinkedHashMap<String, String>();
			String resultValue = bundle.getString(key);
			if(!"".equals(resultValue)&&null!=resultValue){
				String[] arr = resultValue.split(",");
				for(String str : arr){
					String []temp = str.split(":");
					map.put(temp[0], temp[1]);
				}
			}
		}
		return map;
	}
	**/
	public static void main(String []args){
//		if( TarUtils.filesToTar()){
//			if(FileUpDownload.fileUpload()){
//				FileUpDownload.fileDownload();
//			}
//		}
		//log.info("上传状态："+FileUpDownload.fileUpload());
		//log.info("下载状态："+FileUpDownload.fileDownload());
		
		boolean b=fileDownload("127.0.0.1","admin","admin","/test1","C:/localSource");
		System.out.println(b);
	}

}
