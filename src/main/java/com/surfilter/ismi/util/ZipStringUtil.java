package com.surfilter.ismi.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import lombok.extern.slf4j.Slf4j;
@Slf4j
public class ZipStringUtil {
	/**
     * 压缩
     *
     * @param paramString
     * @return
     */
    public static final byte[] compress(String paramString) throws Exception {
        if (paramString == null)
            return null;
        ByteArrayOutputStream byteArrayOutputStream = null;
        ZipOutputStream zipOutputStream = null;
        byte[] arrayOfByte;
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            zipOutputStream = new ZipOutputStream(byteArrayOutputStream);
            zipOutputStream.putNextEntry(new ZipEntry("0"));
            zipOutputStream.write(paramString.getBytes(StandardCharsets.UTF_8));//这里采用gbk方式压缩，如果采用编译器默认的utf-8，这里就直接getByte();
            zipOutputStream.closeEntry();
            arrayOfByte = byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            arrayOfByte = null;
            throw new Exception("压缩字符串数据出错", e);
        } finally {
            if (zipOutputStream != null)
                try {
                    zipOutputStream.close();
                } catch (IOException e) {
                    log.error("关闭zipOutputStream出错", e);
                }
            if (byteArrayOutputStream != null)
                try {
                    byteArrayOutputStream.close();
                } catch (IOException e) {
                	log.error("关闭byteArrayOutputStream出错", e);
                }
        }
        return arrayOfByte;
    }
    
    /**
     * 解压缩
     *
     * @param compressed
     * @return
     */
    public static String decompress(byte[] compressed) throws Exception {
        if (compressed == null)
            return null;
        ByteArrayOutputStream out = null;
        ByteArrayInputStream in = null;
        ZipInputStream zin = null;
        String decompressed;
        try {
            out = new ByteArrayOutputStream();
            in = new ByteArrayInputStream(compressed);
            zin = new ZipInputStream(in);
            zin.getNextEntry();
            byte[] buffer = new byte[1024];
            int offset = -1;
            while ((offset = zin.read(buffer)) != -1) {
                out.write(buffer, 0, offset);
            }
            decompressed = out.toString(StandardCharsets.UTF_8.name());//相应的这里也要采用gbk方式解压缩，如果采用编译器默认的utf-8，这里就直接toString()就ok了
        } catch (IOException e) {
            decompressed = null;
            throw new Exception("解压缩字符串数据出错", e);
        } finally {
            if (zin != null) {
                try {
                    zin.close();
                } catch (IOException e) {
                	log.debug("关闭ZipInputStream出错", e);
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    log.error("关闭ByteArrayInputStream出错", e);
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                	log.debug("关闭ByteArrayOutputStream出错", e);
                }
            }
        }
        return decompressed;
    }
}
