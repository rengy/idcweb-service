package com.surfilter.ismi.util;
/**
 * AES加解密工具类
 */

import java.nio.charset.StandardCharsets;
import java.security.Security;
import java.util.Scanner;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class EncryptAndDecryptUtil {
    /**
     * 密钥算法 AES
     */
    private static final String KEY_ALGORITHM = "AES";

    /**
     * 加解密算法/工作模式/填充方式
     * java支持PKCS5Padding、不支持PKCS7Padding
     * Bouncy Castle支持PKCS7Padding填充方式
     */
    private static final String CIPHER_ALGORITHM = "AES/CBC/PKCS7Padding";

    /**
     * 偏移量，CBC模式需要
     * 与其他语言平台使用的需一致才能通用加解密
     */
    private static final String IV = "VZAVUFAE58697989";


    static {
        // 是PKCS7Padding填充方式，则需要添加Bouncy Castle支持
        Security.addProvider(new BouncyCastleProvider());
    }

    /**
     * AES加密方法
     *
     * @param source 原文
     * @param password 加密密钥
     * @return 密文
     */
//    public static String encrypt(String source, String password) throws Exception {
//        byte[] sourceBytes = source.getBytes(StandardCharsets.UTF_8);
//        byte[] passwordBytes = password.getBytes(StandardCharsets.UTF_8);
//        Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM, "BC");
//        IvParameterSpec iv = new IvParameterSpec(IV.getBytes(StandardCharsets.UTF_8));
//        cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(passwordBytes, KEY_ALGORITHM), iv);
//        byte[] encryptBytes = cipher.doFinal(sourceBytes);
//        String encryptText = new String(Base64.encodeBase64(encryptBytes), StandardCharsets.UTF_8);
//
//
//        return encryptText;
//    }

    public static byte[] encrypt(byte[] sourceBytes, String password) throws Exception {
        byte[] passwordBytes = password.getBytes(StandardCharsets.UTF_8);
        Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM, "BC");
        IvParameterSpec iv = new IvParameterSpec(IV.getBytes(StandardCharsets.UTF_8));
        cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(passwordBytes, KEY_ALGORITHM), iv);
        byte[] encryptBytes = cipher.doFinal(sourceBytes);
       // String encryptText = new String(Base64.encodeBase64(encryptBytes), StandardCharsets.UTF_8);


        return encryptBytes;
    }

    /**
     * AES解密方法
     *
     * @param encryptText 密文
     * @param password 密码
     * @return 原文
     */
//    public static byte[] decrypt(String encryptText, String password) throws Exception {
//        byte[] sourceBytes = password.getBytes(encryptText);
//        byte[] passwordBytes = password.getBytes(StandardCharsets.UTF_8);
//        Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM, "BC");
//        IvParameterSpec iv = new IvParameterSpec(IV.getBytes(StandardCharsets.UTF_8));
//        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(passwordBytes, KEY_ALGORITHM), iv);
//        byte[] decryptBytes = cipher.doFinal(sourceBytes);
//        return decryptBytes;
//    }
//    public static String decrypt(byte[] sourceBytes, String password) throws Exception {
//        byte[] passwordBytes = password.getBytes(StandardCharsets.UTF_8);
//        Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM, "BC");
//        IvParameterSpec iv = new IvParameterSpec(IV.getBytes(StandardCharsets.UTF_8));
//        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(passwordBytes, KEY_ALGORITHM), iv);
//        byte[] decryptBytes = cipher.doFinal(sourceBytes);
//        return new String(decryptBytes, StandardCharsets.UTF_8);
//    }
    
    public static byte[] decrypt(byte[] sourceBytes, String password) throws Exception {
        byte[] passwordBytes = password.getBytes(StandardCharsets.UTF_8);
        Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM, "BC");
        IvParameterSpec iv = new IvParameterSpec(IV.getBytes(StandardCharsets.UTF_8));
        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(passwordBytes, KEY_ALGORITHM), iv);
        byte[] decryptBytes = cipher.doFinal(sourceBytes);
        return decryptBytes;
    }
    
    

    public static void main(String[] args) {
        try {
        	String text = "<?xml version=\"1.0\" encoding=\"utf-8\"?><fileLoad><idcId>11234</idcId><dataUpload>QhuCgIP4+2f9xfSJbqp8RdeHH53/CPlR1sTa/mvYt86HnY3HuL7kVhfQjo54bRZPOTM++NxCNvSSze0POyhh/2JHJJ1iWLHJAoT3j7+6xR7on1MnnV0cW73lYJk2usQR1FN5aoRLr6GRGd9G2vJ4LaqxtOs3CW09q7IMQ1XcdLKmgXR8UHWHkc97THGLt53iSlZeKVwSHgHBUCYXZNn+Fw==</dataUpload><encryptAlgorithm>1</encryptAlgorithm><compressionFormat>1</compressionFormat><hashAlgorithm>1</hashAlgorithm><dataHash>1bSvP+jQ+M+QqW/l1SDQHg==</dataHash><commandVersion>v2.0</commandVersion></fileLoad>";
        	byte m []= EncryptAndDecryptUtil.encrypt(text.getBytes(),  _Constant.aeskey);
            
            byte jm[]= EncryptAndDecryptUtil.decrypt(m,  _Constant.aeskey);
            System.out.println(new String(jm,StandardCharsets.UTF_8));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }
}