package com.surfilter.ismi.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;



public class _DateUtils{
	/**
	 * 
	 * @param source
	 * @param fromPattern
	 * @param toPattern
	 * @return
	 */
	public static String formatDate(Object source,String fromPattern,String toPattern){
		String newValue=null;
		if(source!=null){
			SimpleDateFormat formatter = new SimpleDateFormat(toPattern);
			if(source instanceof java.util.Date){
				newValue=formatter.format(source);
			}else if(source.getClass()==String.class){
				try {
					Date date =  DateUtils.parseDate(source.toString(), fromPattern);
					newValue=formatter.format(date);
				} catch (ParseException e) {
				}
			}else if(source.getClass()==Long.class){
				newValue=formatter.format(new Date((Long)source));
			}
		}
        return newValue;
	}
	
	public static Date parseDate(String source,String... parsePatterns){
		Date date=null;
		if(StringUtils.isBlank(source)){
			return null;
		}
		if(parsePatterns==null){
			parsePatterns=new String[1];
			parsePatterns[0] = "yyyy-MM-dd HH:mm:ss";
		}
		try {
			date = DateUtils.parseDate(source, null, parsePatterns);
		} catch (Exception e) {}
		return date;
	}
}
