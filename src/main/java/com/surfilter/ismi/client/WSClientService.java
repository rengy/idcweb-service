package com.surfilter.ismi.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Date;

import javax.xml.namespace.QName;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.surfilter.ismi.dto.ack.CommandAck;
import com.surfilter.ismi.dto.ack.IdcCommandAck;
import com.surfilter.ismi.util.EncryptAndDecryptUtil;
import com.surfilter.ismi.util.ZipStringUtil;
import com.surfilter.ismi.util._Constant;
import com.surfilter.ismi.util._DateUtils;
import com.surfilter.ismi.webservice.IDCWebService_Service;
import com.surfilter.ismi.webservice.IdcWebService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class WSClientService {
	private XmlMapper xmlMapper=new XmlMapper();
	@Value("${smms.url}")
	private String smmsurl;
	
	@Value("${smms.kouling}")
	private String kouling;
	
	public  String idc_commandack (Long commandId,String randVal) throws Exception{
        // 接口地址
        //String address = "http://101.200.148.31:8080/IDCWebService/idcCommand?wsdl";
    	// 代理工厂
        JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
        // 设置代理地址
        jaxWsProxyFactoryBean.setAddress(smmsurl);
        // 设置接口类型
        jaxWsProxyFactoryBean.setServiceClass(IdcWebService.class);
        // 创建一个代理接口实现
        IdcWebService us = (IdcWebService) jaxWsProxyFactoryBean.create();
        // 数据准备
        // 调用代理接口的方法调用并返回结果
        
        
        //yyyy-MM-dd HH:mm:ss
        IdcCommandAck idcCommandAck=new IdcCommandAck();
        CommandAck commandAck=new CommandAck();
        commandAck.setCommandId(commandId);
        commandAck.setFilterCount(1L);
        commandAck.setHouseId(1L);
        commandAck.setMonitorCount(1L);
        commandAck.setMsgInfo("ok");
        commandAck.setResultCode(0);
        commandAck.setType(1);
        commandAck.setView(1L);
        idcCommandAck.setCommandAck(commandAck);
        idcCommandAck.setIdcId("11234");
        idcCommandAck.setTimeStamp(_DateUtils.formatDate(new Date(), null, "yyyy-MM-dd HH:mm:ss"));
        String data=xmlMapper.writeValueAsString(idcCommandAck);
        
        String idcId="11234";
        //String randVal=RandomStringUtils.randomNumeric(20);
        /**
         * 将用户口令和随机字符串连接后使用hashAlgorithm指定的哈希算法进行哈希运算，
         * 然后进行base64编码得到的结果。
         */
        byte hashdata[]= DigestUtils.md5(kouling+randVal);
        String hashHex=Hex.encodeHexString(hashdata);
        String pwdHash=new String (Base64.encodeBase64(hashHex.getBytes(StandardCharsets.UTF_8)),StandardCharsets.UTF_8);
        log.info("idc_commandack接口请求参数pwdHash：{}",pwdHash);
        /**
         * 对指令执行结果（见11.11节）使用compressionFormat指定的压缩算法进行压缩，
         * 再对压缩后的信息按照encryptAlgorithm参数的要求进行加密，然后进行base64编码运算得到的结果，
         * 包括基础数据管理指令、信息安全管理指令和代码表发布指令的执行结果
         */
        
        String result = new String(Base64.encodeBase64(EncryptAndDecryptUtil.encrypt(
				ZipStringUtil.compress(data)
				, _Constant.aeskey)), StandardCharsets.UTF_8);
        
        
        
        log.info("idc_commandack接口请求参数result：{}",result);
        /**
         * 对指令指令结果使用compressionFormat指定的压缩算法进行压缩，
         * 压缩后串接消息认证密钥，
         * 然后使用hashAlgorithm指定的哈希算法进行哈希运算得到哈希值，
         * 并对哈希值进行base64编码运算形成commandHash，用于验证完整性。
		        消息认证密钥由SMMS与ISMS事先配置确定，长度至少为20位，最多32位
         */
        MessageDigest md5=DigestUtils.getMd5Digest();
        md5.update(ZipStringUtil.compress(data));
        md5.update(_Constant.authkey.getBytes(StandardCharsets.UTF_8));
        
        String md5Hex=Hex.encodeHexString(md5.digest());
        String resultHash= new String(
        		Base64.encodeBase64(md5Hex.getBytes(StandardCharsets.UTF_8)) ,StandardCharsets.UTF_8);
        log.info("idc_commandack接口请求参数resultHash：{}",resultHash);		
        Integer encryptAlgorithm=1;
        Integer hashAlgorithm=1;
        Integer compressionFormat=1;
        String t = ack( idcId,  randVal,  pwdHash ,  result,  resultHash,  encryptAlgorithm,  hashAlgorithm,compressionFormat,  "v2.0");
        log.info("idc_commandack接口返回：{}",t);
        return t;
    }
	private static final QName SERVICE_NAME = new QName("http://webservice.ismi.surfilter.com/", "IDCWebService");

	private String ack(String idcId ,
    String randVal ,
    String pwdHash ,
    String result ,
    String resultHash ,
    int encryptAlgorithm ,
    int hashAlgorithm ,
    int compressionFormat ,
    String commandVersion ) throws MalformedURLException {

        URL wsdlURL = new URL(smmsurl);
        IDCWebService_Service ss = new IDCWebService_Service(wsdlURL, SERVICE_NAME);
        IdcWebService port = ss.getIdcWebServicePort();

        
    
    
    
        String _idcCommandack__return = port.idcCommandack(idcId,  randVal,  pwdHash,  result,  resultHash,
   			 encryptAlgorithm,  hashAlgorithm,  compressionFormat,  commandVersion);

        return _idcCommandack__return;

    
	}
	
    public static void idcCommand(){
        // 接口地址
        //String address = "http://101.200.148.31:8080/IDCWebService/idcCommand?wsdl";
    	String address = "http://localhost:8080/IDCWebService/idcCommand?wsdl";
    	// 代理工厂
        JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
        // 设置代理地址
        jaxWsProxyFactoryBean.setAddress(address);
        // 设置接口类型
        jaxWsProxyFactoryBean.setServiceClass(IdcWebService.class);
        // 创建一个代理接口实现
        IdcWebService us = (IdcWebService) jaxWsProxyFactoryBean.create();
        // 数据准备
        String data = "hello world";
        // 调用代理接口的方法调用并返回结果
        String result = us.idcCommand("","","", "", "", 3, 1L, 1, 1, 1, "");
        System.out.println("返回结果:" + result);
    }


    public static void main(String[] args) {
    	idcCommand();
    }
}
