package com.surfilter.ismi.test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.time.Duration;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;

import com.surfilter.ismi.util.EncryptAndDecryptUtil;
import com.surfilter.ismi.util.ZipStringUtil;
import com.surfilter.ismi.util._Constant;

public class MyTest {

	public static void main(String[] args) {
		//File file=new File("D:\\工作\\ws\\b64.xml");
		try {
			//String text=FileUtils.readFileToString(file, StandardCharsets.UTF_8);
			
			//电信报文中dataUpload节点值
			String text="Q5EzsMO8XNr1lVlmFHz0SUopVPDvxNaBB5dyJbVOf3m1GEDvVZc2aLKriP9bUqxhoU+JKCc14QxDjW41gtBseHZBgRBw0lcQHfUUKkLxeqUfFmLYxfwoVJzj28umAOssvRWpymopNtvmMExEEb4ihFTH0e13iyqp+1ZVsmoOqO7RXF2LR2klRopw7USo0XhQ15XmZ5Lx6PB6M12retFT1dmQ/sx6Hy4BM7dyRXVK3OEo07NrikUF1vjXcfWZ0lubP1EL+MvjjWucR7ssq/hyUOrvLbmGMwrMjGcqXDrXuxEJQdpLtKKYnkza0+ZrOldcYtxD/rp0067VHMoPKLCRTGSx4Rd6G68kVUN0UJ35C3iX5PIn8aIC/WYKudjYGwMpgc+QTMA0SbC8H/rRXxeMcGs7fkb1HZQY/jZdY0l7+sDHCIKw9UkIKiOUExKUEry5F5GLk+3E3uWtsXprA/ujx+Kw+2ZJroNGfkJm4DaETKX70QHvapGhV/Qlj6gI7lp2T2Kwx/qIaOt8C8hq5cGo1RlzMoikOQrETaK+crTDn/9hCjjuDFCqmZRs8bDvmGCERAOZlVhVxhovZGGx3xdWigu4vU41a9QWOBRqqGtGtHtGJB1rOpb6GvmMMA8c36mP9TEh8bDijjb+no7xvk6N/eLTmRiX4to8+Dutpmnt8/tROww5Hc7JiAPPpJFne1BvhufxxbE2XTCAyPKWrtV92DwF3St5LDauKWC/B6Ir5ckFPHt4pnpi/VvV8+8inT8aysQgAb+I6ygokTMYcQYZOaZrAtYKjaQR0H0S4lHWtVQlxQQjqaTO+NpuLwpETx8x4quc0guYDXDaSymQPDWdGzqhThkA2cTsvpSZOhy0fj4IwedkzwE3BLmPIT742aUrstIsc4uPlEwmqWfqP6lJwhq4thBkCdF/aLlsXGCtdtLUUW2rwJ+Awf4qMafI+Pl52OBqgCDe4zBGrwBoJt8AoTL5mRNllReIFrk9J4Ib22qi21X+toixj44/lt57h8TIqlH71Salu8CqNJA7ObIgSEQvfrMV+y5Or2YMWeCw5N0mX+W8ksBNrZzChMG3xdE98jcqAR9Znf730Wh6In6sY08cLPYABdzkay8Tmm7LYsD7VNqJWjiIY5cylb8UIkkpfUGeBTFaDkFyefSjO4qklao4G4Z6bANrK6mtyK1JPCkx8zm/rKCAbisSUNAYwV34tKDhfvAlb3DKW0qLt2UDzydVPDQ0+nX7gtI90UW2UmmaS9PUG74+WIQK3vVh+JVTYQBdaG15VDQl/TYoSiUkeuaoRQMSabE1DRUgfiTOIzztFgR0SjfLZ2noNv38LFYY9xStLWmgJJo68jqFRHTg3BH1G0BSHInpYJ9EQv0rCApV711TgsGL+nnaLH0u83cLg6JESUkkRxiSgQRCwFhG/x8HN52wKNny+EGhG7PXhMKOZe6MbvnSUiNplDVhj4iPVUGhxmJIgJJ6a9RNO0qpmfN47JTecyRDxxG1tc/ebhyFJ09T9k9BrYtcSL3QeU/61WQgsvnT+MbFOiM/n1tkEiSzPQBuwu1jxaOwlZ1b24J/i0b329x8v+Z7EfmMiyz3z9TFlFoCaLtC7GPsaX05UkVwLv3lK181uCeofHJcPYgO7xviKbdNstZkQHOPsYgJXak4yUI2nBwKnn5/SciBkDpx+zLP0kqBXDoDs3e08nYTtl8foUy8lJqpI9b/dVbedmeHt2FQYddvJRyytfW2M8Zl5ooYgW/QFWts+jrnAbM=";
			//解码
			byte base64data[]=Base64.decodeBase64(text);
			//解密
			byte aesdata[]=EncryptAndDecryptUtil.decrypt (base64data,  _Constant.aeskey);
			//解压后的字符串
			System.out.println(ZipStringUtil.decompress(aesdata));
			
			//hash
			MessageDigest md5=DigestUtils.getMd5Digest();
			md5.update(aesdata);
            md5.update(_Constant.authkey.getBytes(StandardCharsets.UTF_8));
            byte md5data[]=md5.digest();
            //byte md5data[]=DigestUtils.md5(aesdata+_Constant.authkey);
            //String datahash= new String(Base64.encodeBase64(md5data), StandardCharsets.UTF_8);
			System.out.println(Hex.encodeHexString(md5data));
			
			//电信报文中dataHash节点值
            String dataHash="MjljNTY1NDcwNGU4MTgzNzYxMWU3OTcxNmNiNzNkOTI=";
            byte newh[]=Base64.decodeBase64(dataHash);
            System.out.println(new String(newh,StandardCharsets.UTF_8));
			
            
			//System.out.println(mw);
			//System.out.println(Duration.ofMillis(System.currentTimeMillis()).getSeconds());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
