package com.surfilter.ismi.model;


import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ResultModel {

    String code;
    String msg;
    Object data;


    public static ResultModel success(Object data) {
        return ResultModel.builder()
                .code("0")
                .data(data)
                .build();
    }

    public static ResultModel fail(String code, String msg) {
        return ResultModel.builder()
                .code(code)
                .data(msg)
                .build();
    }

    public static ResultModel fail(String msg) {
        return ResultModel.builder()
                .code("999")
                .data(msg)
                .build();
    }

}
