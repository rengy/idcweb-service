package com.surfilter.ismi.model;

import lombok.*;

/**
 * 上传文件处理结果
 */
@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class FileUploadResult {

    private String dataTypeCode;
    private String fileName;
    private String rltCode;
    private String rleDesc;

}
