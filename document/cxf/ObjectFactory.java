
package com.surfilter.ismi.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.surfilter.ismi.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _IdcCommandack_QNAME = new QName("http://webservice.ismi.surfilter.com/", "idc_commandack");
    private final static QName _IdcCommandackResponse_QNAME = new QName("http://webservice.ismi.surfilter.com/", "idc_commandackResponse");
    private final static QName _SendAck_QNAME = new QName("http://webservice.ismi.surfilter.com/", "sendAck");
    private final static QName _SendAckResponse_QNAME = new QName("http://webservice.ismi.surfilter.com/", "sendAckResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.surfilter.ismi.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IdcCommandack }
     * 
     */
    public IdcCommandack createIdcCommandack() {
        return new IdcCommandack();
    }

    /**
     * Create an instance of {@link IdcCommandackResponse }
     * 
     */
    public IdcCommandackResponse createIdcCommandackResponse() {
        return new IdcCommandackResponse();
    }

    /**
     * Create an instance of {@link SendAck }
     * 
     */
    public SendAck createSendAck() {
        return new SendAck();
    }

    /**
     * Create an instance of {@link SendAckResponse }
     * 
     */
    public SendAckResponse createSendAckResponse() {
        return new SendAckResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IdcCommandack }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.ismi.surfilter.com/", name = "idc_commandack")
    public JAXBElement<IdcCommandack> createIdcCommandack(IdcCommandack value) {
        return new JAXBElement<IdcCommandack>(_IdcCommandack_QNAME, IdcCommandack.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IdcCommandackResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.ismi.surfilter.com/", name = "idc_commandackResponse")
    public JAXBElement<IdcCommandackResponse> createIdcCommandackResponse(IdcCommandackResponse value) {
        return new JAXBElement<IdcCommandackResponse>(_IdcCommandackResponse_QNAME, IdcCommandackResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendAck }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.ismi.surfilter.com/", name = "sendAck")
    public JAXBElement<SendAck> createSendAck(SendAck value) {
        return new JAXBElement<SendAck>(_SendAck_QNAME, SendAck.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendAckResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.ismi.surfilter.com/", name = "sendAckResponse")
    public JAXBElement<SendAckResponse> createSendAckResponse(SendAckResponse value) {
        return new JAXBElement<SendAckResponse>(_SendAckResponse_QNAME, SendAckResponse.class, null, value);
    }

}
