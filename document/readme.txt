chmod +x start
chmod +x stop



--spring.config.location

nohup java -jar -Dspring.profiles.active=test webservice-0.0.1-SNAPSHOT.jar > /dev/null 2>&1 &

我们 ISMS
对方 SMMS


陈经理

——基础数据上报与查询
基础数据记录包括IDC/ISP经营单位、IDC/ISP业务客户有关主体信息记录以及IDC/ISP业务经营单位所辖全部IDC业务机房/ISP业务节点有关的资源信息记录。
ISMS应在本地新增基础数据或更新基础数据后同步将新增数据记录或含修改内容的数据记录上报给SMMS，
上报消息格式见11.3节。同时，ISMS能根据SMMS的上报基础数据信息核验反馈指令，
对存在异常的记录及时进行补正并重新上报，核验结果反馈消息格式见11.14节，上报消息格式见11.3节。
ISMS应支持SMMS针对特定记录的实时查询。SMMS通过ISMI接口向ISMS下发基础数据查询指令的方式实现基础数据查询。
基础数据查询消息格式见11.2节、上报消息格式见11.3节。
——基础数据异常监测
ISMS应对基础数据异常情况进行监测并形成基础数据监测异常记录。
基础数据监测异常记录应定时通过ISMI自动上报并支持SMMS的实时查询。
SMMS通过ISMI接口向ISMS下发基础数据查询指令的方式实现基础数据监测异常记录的查询功能，
有关的基础数据监测异常记录的查询消息格式见11.2节，上报消息格式见11.4节。

6.2
SMMS通过ISMI接口向ISMS下发访问日志查询指令的方式实现访问日志的查询功能，
有关的访问日志查询消息格式见11.5节，上报消息格式见11.6节。



所有timeStamp节点描述的时间均采用yyyy-MM-dd HH:mm:ss格式。

http://172.30.102.6:7080/ism_product/ws/commandack?wsdl

加密密钥：LJRYRPYF27466944
对称加密算法模式：CBC
密钥偏移量：VZAVUFAE58697989
哈希算法：MD5
加密：AES加密




名称:        
山西联通-信安标准接口
唯一标识:        
sxlt
接口分类:        
信息安全接口
状态:        
未知
 url配置
接口地址:        
http://10.10.10.3:8080/IDCWebService/idcCommand?wsdl



http://101.200.148.31:8080/IDCWebService/idcCommand?wsdl
密钥配置
密码:        
KWQ239
消息加密密钥:        
LJRYRPYF27466944
消息认证密码:        
666666
偏移量密码:        
VZAVUFAE58697989
FTP配置
FTP地址:        
172.30.102.6
FTP用户名:        
sxlt
FTP密码:        
Zgqlt@654
FTP物理路径:        
/bigdata/idc_home/sxlt
绑定主机配置
绑定IP/域名:        
10.10.10.3



表1  数据上报类型代码表
代码	上报数据类型
1	基础数据记录
2	基础数据监测异常记录
3	访问日志查询记录
4	违法信息监测记录
5	违法信息过滤记录
6	（保留）
7	ISMS活动状态
8	活跃资源监测记录
9	违法违规网站监测记录
